package com.ourincheon.smartcampus1231.System;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DaeWook Choi 2012.03.21
 */

public class DwAndroidUtils {

	static public Map<String, Object> map;

	static public List<Map<String, Object>> jsonArrayToList(
			JSONArray jsonArray, List<Map<String, Object>> list)
			throws JSONException {
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			Map<String, Object> map = new HashMap<String, Object>();

			for (int j = 0; j < jsonObject.names().length(); j++) {
				String strKey = (String) jsonObject.names().get(j);
				map.put(strKey, jsonObject.get(strKey));
			}

			list.add(map);
		}
		return list;
	}

	static public Map<String, Object> jsonObjectToMap(JSONObject jsonObject)
			throws JSONException {

		map = new HashMap<String, Object>();

		for (int i = 0; i < jsonObject.length(); ++i) {
			String strKey = (String) jsonObject.names().get(i);
			String strValue = jsonObject.get(strKey).toString();
			map.put(strKey, strValue);
		}

		return map;

	}

}
