package com.ourincheon.smartcampus1231.System;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Youngdo on 2016-02-20.
 */
public class OnlineCheck {
    private static Context context;

    public OnlineCheck(Context context) {
        this.context = context;
    }

    public static boolean isOnline(){
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo.State wifi = connectivityManager.getNetworkInfo(1).getState();//wifi
            if(wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING){
                return true;
            }
        }catch (NullPointerException e){
            return false;
        }
        return false;
    }
}
