package com.ourincheon.smartcampus1231;

import android.graphics.Bitmap;

/**
 * Created by Youngdo on 2016-03-17.
 */
public class Datastorage {
    private static String version;
    private static Bitmap[] adimg = new Bitmap[5];
    private static String[] aduri = new String[5];

    public static void setVersion(String version) {
        Datastorage.version = version;
    }

    public static void setAdBitmap(Bitmap bitmap[]){
        adimg[0] = bitmap[0];
        adimg[1] = bitmap[1];
        adimg[2] = bitmap[2];
        adimg[3] = bitmap[3];
        adimg[4] = bitmap[4];
    }

    public static void  setAduri(String string[]){
        aduri[0] = string[0];
        aduri[1] = string[1];
        aduri[2] = string[2];
        aduri[3] = string[3];
        aduri[4] = string[4];
    }

    public static String getVersion() {

        return version;
    }

    public static Bitmap[] getAdBitmap(){
        return adimg;
    }

    public static String[] getAduri(){
        return aduri;
    }
}