package com.ourincheon.smartcampus1231;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.ourincheon.smartcampus1231.DB.MySQLHelper;
import com.ourincheon.smartcampus1231.Login.LoginActiviy;
import com.ourincheon.smartcampus1231.Resource.RestClient;
import com.ourincheon.smartcampus1231.timetable.TimetableDBhelper;
import com.ourincheon.smartcampus1231.timetable.Timetabledbset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Youngdo on 2016-02-24.
 */
public class Splash extends AppCompatActivity {
    SharedPreferences setting;
    SharedPreferences.Editor editor;
    ProgressDialog dialog;
    RequestParams params = new RequestParams();

    SharedPreferences pref;
    boolean gettable;
    Context context;
    SQLiteDatabase db;
    SQLiteDatabase db_time;
    TimetableDBhelper helper_time;
    MySQLHelper helper;
    Bitmap[] adbitmap = new Bitmap[5];
    String[] aduri = new String[5];

    public static Context sContext;

    String title = "";
    String message = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

//        final RestClient restClient = new RestClient(this);

//        restClient.get("server_status.json", null, new JsonHttpResponseHandler() {
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                super.onFailure(statusCode, headers, responseString, throwable);
//
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Splash.this);
//                alertDialogBuilder.setMessage("서버에 접속할 수 없습니다.").setCancelable(false).setPositiveButton("종료", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        Splash.this.finish();
//                    }
//                });
//                AlertDialog alertDialog = alertDialogBuilder.create();
//                alertDialog.show();
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                super.onSuccess(statusCode, headers, response);
//
//                try {
//                    int server_ok = response.getInt("server_ok");
//                    message = response.getString("message");
//
//                    if (server_ok == 0) {
//                        AppStart(restClient);
//                    } else if (server_ok == 1){
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Splash.this);
//                        alertDialogBuilder.setMessage(message).setCancelable(false).setPositiveButton("종료", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                Splash.this.finish();
//                            }
//                        });
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//                        alertDialog.show();
//                    }
//
//                } catch (Exception e){
//                    e.printStackTrace();
//                }
//
//            }
//        });

        AppStart();
    }

    void AppStart(){
        DeleteNCreate();
        DeleteNCreate_time();

        final Timetabledbset timetabledbset = new Timetabledbset(this);
        pref = getSharedPreferences("time", context.MODE_PRIVATE);
        gettable = pref.getBoolean("gettable", false);
        if (true) {
            timetabledbset.execute();
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("gettable", true);
            editor.commit();
        }
        final RestClient restClient = new RestClient(this);
        setting = getSharedPreferences("setting", 0);
        editor= setting.edit();
        Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                if (!setting.getString("ID", "").isEmpty()) {
                    dialog = ProgressDialog.show(Splash.this, "",
                            getString(R.string.loading), true);

                    params.put("sno", setting.getString("ID", ""));
                    params.put("pw", setting.getString("PW", ""));

                    restClient.get("server_msg.json", null, new JsonHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            super.onFailure(statusCode, headers, responseString, throwable);

                            dialog.dismiss();
                            ShowMessage("오류", "서버에 접속할 수 없습니다.", 1);
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);

                            try {
                                title = response.getString("title");
                                message = response.getString("message");
                            } catch (Exception e){
                                e.printStackTrace();
                            }

                            restClient.get("ad/ad1.jpg", null, new FileAsyncHttpResponseHandler(Splash.this) {
                                @Override
                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                                    dialog.dismiss();
                                    ShowMessage(title, message, 1);
                                }

                                @Override
                                public void onSuccess(int statusCode, Header[] headers, File file) {
                                    adbitmap[0] = BitmapFactory.decodeFile(file.getAbsolutePath());
                                    restClient.get("ad/ad2.jpg", null, new FileAsyncHttpResponseHandler(Splash.this) {
                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                                            dialog.dismiss();
                                            ShowMessage(title, message, 1);
                                        }

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, File file) {
                                            adbitmap[1] = BitmapFactory.decodeFile(file.getAbsolutePath());
                                            restClient.get("ad/ad3.jpg", null, new FileAsyncHttpResponseHandler(Splash.this) {
                                                @Override
                                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                                                    dialog.dismiss();
                                                    ShowMessage(title, message, 1);
                                                }

                                                @Override
                                                public void onSuccess(int statusCode, Header[] headers, File file) {
                                                    adbitmap[2] = BitmapFactory.decodeFile(file.getAbsolutePath());
                                                    restClient.get("ad/ad4.jpg", null, new FileAsyncHttpResponseHandler(Splash.this) {
                                                        @Override
                                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                                                            dialog.dismiss();
                                                            ShowMessage(title, message, 1);
                                                        }

                                                        @Override
                                                        public void onSuccess(int statusCode, Header[] headers, File file) {
                                                            adbitmap[3] = BitmapFactory.decodeFile(file.getAbsolutePath());
                                                            restClient.get("ad/ad5.jpg", null, new FileAsyncHttpResponseHandler(Splash.this) {
                                                                @Override
                                                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                                                                    dialog.dismiss();
                                                                    ShowMessage(title, message, 1);
                                                                }

                                                                @Override
                                                                public void onSuccess(int statusCode, Header[] headers, File file) {
                                                                    adbitmap[4] = BitmapFactory.decodeFile(file.getAbsolutePath());
                                                                    restClient.get("aduri.json", null, new JsonHttpResponseHandler() {
                                                                        @Override
                                                                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                                                            dialog.dismiss();
                                                                            ShowMessage(title, message, 1);
                                                                        }

                                                                        @Override
                                                                        public void onSuccess(int statusCode, Header[] headers, JSONArray array) {
                                                                            try {
                                                                                JSONObject object = array.getJSONObject(0);
                                                                                aduri[0] = object.getString("ad1");
                                                                                aduri[1] = object.getString("ad2");
                                                                                aduri[2] = object.getString("ad3");
                                                                                aduri[3] = object.getString("ad4");
                                                                                aduri[4] = object.getString("ad5");
                                                                                restClient.post("postlogin", params, new TextHttpResponseHandler() {
                                                                                    @Override
                                                                                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                                                                        dialog.dismiss();
                                                                                        ShowMessage(title, message, 1);
                                                                                    }

                                                                                    @Override
                                                                                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                                                                        Datastorage.setAdBitmap(adbitmap);
                                                                                        Datastorage.setAduri(aduri);
                                                                                        dialog.dismiss();

                                                                                        ShowMessage(title, message, 0);

                                                                                        sContext = getApplicationContext();
//                                                                                splashfirstmenu = new MenuXmlDataModel();
//                                                                                RestaurantAsyncTask_main restaurantAsyncTask_main = new RestaurantAsyncTask_main();
//                                                                                restaurantAsyncTask_main.execute();

//                                                                                        Intent intent = new Intent(Splash.this, MainActivity.class);
////                                                                                intent.putExtra("splashfirstmenu", splashfirstmenu);
//                                                                                        new Timetabledbset(context).execute();
//                                                                                        startActivity(intent);
//                                                                                        finish();
                                                                                    }
                                                                                });
                                                                            } catch (JSONException e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });

                }
                else {
                    Intent intent = new Intent(Splash.this, LoginActiviy.class);
                    startActivity(intent);
                    finish();
                }
            }
        };

        handler.sendEmptyMessageDelayed(0, 500);
    }

    void ShowMessage(String title, String message, final int type){
        if(message.length() != 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Splash.this);
            alertDialogBuilder.setTitle(title).setMessage(message).setCancelable(false).setPositiveButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(type == 1) {
                        //onfailure에서는 앱 종료
                        Splash.this.finish();
                    } else if (type == 0) {
                        Intent intent = new Intent(Splash.this, MainActivity.class);
                        new Timetabledbset(context).execute();
                        startActivity(intent);
                        finish();
                    }
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else {
            if(type==0){
                Intent intent = new Intent(Splash.this, MainActivity.class);
                new Timetabledbset(context).execute();
                startActivity(intent);
                finish();
            }
        }
    }

    public void DeleteNCreate_time(){

        db_time = helper_time.getInstance(this).getWritableDatabase();

        String timeD = "drop table if exists time";
        db_time.execSQL(timeD);
        String timeC = "create table time (num integer primary key,id text, name text, profes text, day text, room text, simpleroom text, start integar, end intager);";
        db_time.execSQL(timeC);


    }
    public void DeleteNCreate() {
        db = helper.getInstance(this).getWritableDatabase();
//        db.delete("g1n1",null,null);
//        db.delete("g1n2",null,null);
//        db.delete("g2n1",null,null);
//        db.delete("g2n2",null,null);
//        db.delete("g3n1",null,null);
//        db.delete("g3n2",null,null);
//        db.delete("g4n1",null,null);
//        db.delete("g4n2",null,null);
//        db.delete("gn3",null,null);
//        db.delete("gn4",null,null);
//        db.delete("credit",null,null);
//        db.delete("TOT_AVG",null,null);
//        db.delete("Year_AVG",null,null);

        String d_g1n1 = "drop table if exists g1n1";
        String d_g1n2 = "drop table if exists g1n2";
        String d_g2n1 = "drop table if exists g2n1";
        String d_g2n2 = "drop table if exists g2n2";
        String d_g3n1 = "drop table if exists g3n1";
        String d_g3n2 = "drop table if exists g3n2";
        String d_g4n1 = "drop table if exists g4n1";
        String d_g4n2 = "drop table if exists g4n2";
        String d_gn3 = "drop table if exists gn3";
        String d_gn4 = "drop table if exists gn4";
        String d_credit = "drop table if exists credit";
        String d_TOT_AVG = "drop table if exists TOT_AVG";
        String d_Year_AVG = "drop table if exists Year_AVG";
        String d_TOT_CREDIT = "drop table if exists TOT_CREDIT";
        db.execSQL(d_g1n1);
        db.execSQL(d_g1n2);
        db.execSQL(d_g2n1);
        db.execSQL(d_g2n2);
        db.execSQL(d_g3n1);
        db.execSQL(d_g3n2);
        db.execSQL(d_g4n1);
        db.execSQL(d_g4n2);
        db.execSQL(d_gn3);
        db.execSQL(d_gn4);
        db.execSQL(d_credit);
        db.execSQL(d_TOT_AVG);
        db.execSQL(d_Year_AVG);
        db.execSQL(d_TOT_CREDIT);
        String g1n1 = "CREATE TABLE g1n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g1n2 = "CREATE TABLE g1n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g2n1 = "CREATE TABLE g2n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g2n2 = "CREATE TABLE g2n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g3n1 = "CREATE TABLE g3n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g3n2 = "CREATE TABLE g3n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g4n1 = "CREATE TABLE g4n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g4n2 = "CREATE TABLE g4n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String gn3 = "CREATE TABLE gn3 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String gn4 = "CREATE TABLE gn4 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String credit = "CREATE TABLE credit ( acquire_Credit integer not null, TotalCredit integer not null);";
        String TOT_AVG = "CREATE TABLE TOT_AVG ( tot_AVG text not null);";
        String year_AVG = "CREATE TABLE Year_AVG ( year_AVG text not null);";
        String TOT_CREDIT = "CREATE TABLE TOT_CREDIT(tot_CREDIT text not null);";
        db.execSQL(g1n1);
        db.execSQL(g1n2);
        db.execSQL(g2n1);
        db.execSQL(g2n2);
        db.execSQL(g3n1);
        db.execSQL(g3n2);
        db.execSQL(g4n1);
        db.execSQL(g4n2);
        db.execSQL(gn3);
        db.execSQL(gn4);
        db.execSQL(credit);
        db.execSQL(TOT_AVG);
        db.execSQL(year_AVG);
        db.execSQL(TOT_CREDIT);
    }
}
