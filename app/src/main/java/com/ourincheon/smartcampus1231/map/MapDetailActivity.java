package com.ourincheon.smartcampus1231.map;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.System.DwAndroidUtils;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapDetailActivity extends Activity {
	private static List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
	private Context mContext;
	private ListView mapDetailListview;
	private static int position;
	private String mapName;
	private Bitmap bitmap = null;
	private ImageView detailImageview;
	private TextView MapInfoID;
	private TextView MapInfoName;
	private static String[] mapCodeArray = {"SB", "SA", "SZ", "SC", "SF", "SG", "SH", "SI", "SJ", " ", "SL", "SM", "SN",
			"SO", "SP", "SQ", "SR", " ", "ST", "SU", "SV", "SW", "SX", " ", "SE", "ZA","SY1","SY2","SY3", "1201",
			"1301", "1401", "1901", " ", " ", " "};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map_detail);
		mContext = this;
		Button MapDetailBackBtn = (Button) findViewById(R.id.map_detail_btn_back);
		MapDetailBackBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				MapDetailActivity.this.onBackPressed();
				finish();
				overridePendingTransition(R.anim.fade, R.anim.fade);
			}
		});
		
		position = getIntent().getIntExtra("map_id", 0);
		mapName = getIntent().getStringExtra("map_name");
		MapInfoID = (TextView) findViewById(R.id.map_detail_textview_left);
		MapInfoName = (TextView) findViewById(R.id.map_detail_textview_right);
		
		String strPosition = String.valueOf(position);
		int idx = Integer.parseInt(strPosition);
		if (idx > 29) {
			idx = idx - 29;
		}
		MapInfoID.setText(String.valueOf(idx) + "호관");
		MapInfoName.setText(mapName);

		View header = null;
		header = View
				.inflate(mContext, R.layout.map_detail_row_imageview, null);
		detailImageview = (ImageView) header
				.findViewById(R.id.map_detail_row_imageview);
		LinearLayout detailBody = (LinearLayout) header
				.findViewById(R.id.map_detail_header_body);
		try {
			getMapImage();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Display display = ((WindowManager) getSystemService(WINDOW_SERVICE))
				.getDefaultDisplay();
		int displayWidth = display.getWidth();
		detailImageview.setImageBitmap(bitmap);
		detailBody.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, (displayWidth / 4) * 3));
		mapDetailListview = (ListView) findViewById(R.id.map_detail_listview);
		mapDetailListview.addHeaderView(header);
		new SmartCampusAsyncTask().doRunExecute(this, new TaskFunction() {

			@Override
			public void doRun() {
				// TODO Auto-generated method stub
				refreshMapInfo();
			}

			@Override
			public void doFinish() {
				// TODO Auto-generated method stub
				MapListViewAdapter Adapter = new MapListViewAdapter(mContext,
						mapList);
				mapDetailListview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
				mapDetailListview.setAdapter(Adapter);
			}
		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finish();
		overridePendingTransition(R.anim.fade, R.anim.fade);
	}

	public static void refreshMapInfo() {

		try {
			Map<String, Object> requestMap = new HashMap<String, Object>();

			requestMap.put("info_id", mapCodeArray[position-1]);

			JSONArray array = Face3Utils.getUrlLongFromList(SmartCampusVariable
							.getServerHttpUrl("/campusmapdata/getMapInfoList.do"),
					requestMap);

			String[] loadTables = { "campus_map" };
			if (array == null) {
				return;
			}

			mapList.clear();
			mapList = DwAndroidUtils.jsonArrayToList(array, mapList);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getMapImage() throws IOException {

		new SmartCampusAsyncTask().doRunExecute(mContext, new TaskFunction() {

			@Override
			public void doRun() {
				// TODO Auto-generated method stub
				try {
					String mapID = String.valueOf(position);
					if (mapID.length() == 1) {
						mapID = "0" + mapID;
					}
					String strUrl = "http://117.16.191.242/SmartCampus_Web/map_photo/"
							+ mapID + ".png";
					Display display = ((WindowManager) getSystemService(WINDOW_SERVICE))
							.getDefaultDisplay();
					int displayWidth;
					displayWidth = display.getWidth();
					detailImageview
							.setLayoutParams(new LinearLayout.LayoutParams(
									LayoutParams.MATCH_PARENT,
									LayoutParams.MATCH_PARENT));
					detailImageview.setImageBitmap(BitmapFactory
							.decodeStream(new URL(strUrl).openConnection()
									.getInputStream()));
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void doFinish() {
				// TODO Auto-generated method stub
			}
		});
	}

}
