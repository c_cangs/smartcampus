package com.ourincheon.smartcampus1231.map;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ourincheon.smartcampus1231.R;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapDaumActivity extends Activity implements
		MapView.OpenAPIKeyAuthenticationResultListener,
		MapView.MapViewEventListener, MapView.CurrentLocationEventListener,
		MapView.POIItemEventListener {
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	private EditText searchText;
	private MapView mapView;
	private String LOG_TAG;
	private Button btnSong, btnJe;
	private static final int MENU_MAP_TYPE = Menu.FIRST + 1;
	private static final int MENU_MAP_MOVE = Menu.FIRST + 2;
	private static final int MENU_LOCATION_TRACKING = Menu.FIRST + 3;
	private List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	private String mapNameArray[] = { "대학본부", "교수회관", "홍보관", "정보전산원/정보기술교육원",
			"자연과학대학", "학산도서관", "정보기술대학", "공과대학", "공동실험실습관", "게스트하우스", "복지회관",
			"컨벤션센터", "사회과학대학/법과대학", "동북아경제통상대학/경영대학/물류대학원", "인문대학/어학원",
			"예체능대학", "학생회관", "기숙사", "국제교류관", "스포츠센터", "체육관", "학군단", "공연장",
			"전망타워", "어린이집", "온실", "제2공동실험실습관", "도시과학대학", "생명공학부", "본관", "성리관", "성지관", "인천시민대학", "창업보육센터",
			"해방광장", "운동장" };
	private int pinArray[] = { R.drawable.pin01, R.drawable.pin02,
			R.drawable.pin03, R.drawable.pin04, R.drawable.pin05,
			R.drawable.pin06, R.drawable.pin07, R.drawable.pin08,
			R.drawable.pin09, R.drawable.pin10, R.drawable.pin11,
			R.drawable.pin12, R.drawable.pin13, R.drawable.pin14,
			R.drawable.pin15, R.drawable.pin16, R.drawable.pin17,
			R.drawable.pin18, R.drawable.pin19, R.drawable.pin20,
			R.drawable.pin21, R.drawable.pin22, R.drawable.pin23,
			R.drawable.pin24, R.drawable.pin25, R.drawable.pin26,
			R.drawable.pin27,
			R.drawable.pin28,
			R.drawable.pin29,
			R.drawable.pin_je_01, R.drawable.pin_je_02, R.drawable.pin_je_03,
			R.drawable.pin_je_04, R.drawable.pin_je_05, R.drawable.pin_je_06,
			R.drawable.pin_je_07 };
	private Double[] longitudeArray = { 126.634705, 126.633782, 126.634114,
			126.63564920425415, 126.63487672805786, 126.6342544555664,
			126.63372874259949, 126.63293480873108, 126.6335141658783,
			126.6317868232727, 126.63179755210876, 126.632559299469,
			126.6333532333374, 126.63285970687866, 126.6320013999939,
			126.63122892379761, 126.63087487220764, 126.62978053092956,
			126.63023114204407, 126.6298234462738, 126.63025259971618,
			126.63078904151916, 126.6325056552887, 126.63571357727051,
			126.6360354423523, 126.6355311870575,
			126.633324, 126.632857, 126.632546,
			126.65605, 126.658148,
			126.659116, 126.658724, 126.658177, 126.655801, 126.656004,  };
	private Double[] latitudeArray = { 37.376790, 37.377506, 37.377140,
			37.376440978772585, 37.37546902865869, 37.37494041892313,
			37.374360649123176, 37.37345688138004, 37.372646891602855,
			37.37277478530706, 37.37444590966893, 37.375289983842904,
			37.37608289335386, 37.376517711139876, 37.37559691755115,
			37.374795476893276, 37.374156023417974, 37.373823505456464,
			37.37463348252845, 37.3749063149413, 37.37558839163177,
			37.37585269468192, 37.377796572371174, 37.37587827234703,
			37.375170620394925, 37.375162094427104,
			37.371934, 37.371862, 37.372190,
			37.473432, 37.47278,
			37.471301, 37.470145, 37.470645, 37.472778, 37.471957,  };
	private static final int REQUEST_CODE_LOCATION = 2;
	@TargetApi(Build.VERSION_CODES.M)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_map_daum);
			//ACCESS_FINE_LOCATION"

			//permission marshmallow
//		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
//				!= PackageManager.PERMISSION_GRANTED) {
//			requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);

			searchText = (EditText) findViewById(R.id.search_edittext);
//			btnSong = (Button) findViewById(R.id.map_songdo);
//			btnJe = (Button) findViewById(R.id.map_je);

			mapView = new MapView(this);
			// if (SmartCampusVariable.noMember) {
			// setNomemberList();
			// } else {
			// setMapList();
			// }
//			btnSong.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					setSongdo();
//				}
//			});
//
//			btnJe.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					setJemulpo();
//				}
//			});
			Button mapBackBtn = (Button) findViewById(R.id.map_btn_back);
			mapBackBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});

			Button mapLocationTrackingBtn = (Button) findViewById(R.id.map_btn_compus);
			mapLocationTrackingBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOnWithHeading);
				}
			});

			LinearLayout mapBody = (LinearLayout) findViewById(R.id.map_linearlayout);
			// getMapData();

			mapView.setDaumMapApiKey("2bfe61362063039e6d33265ca5365f915bbbb911");
			mapView.setOpenAPIKeyAuthenticationResultListener(this);
			mapView.setMapViewEventListener(this);
			mapView.setPOIItemEventListener(this);
			mapView.setMapType(MapView.MapType.Standard);

			whilePinMap();

			mapBody.addView(mapView);

			// Move and Zoom to
			mapView.setMapCenterPointAndZoomLevel(
			MapPoint.mapPointWithGeoCoord(37.3752729, 126.633031), 1, true);
//			setSongdo();

			mapView.setCurrentLocationEventListener(this);
			mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOnWithoutHeading); // TrackingModeOnWithoutHeading,
			// TrackingModeOff
		}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		menu.add(0, MENU_MAP_TYPE, Menu.NONE, "MapType");
		menu.add(0, MENU_MAP_MOVE, Menu.NONE, "Move");
		menu.add(0, MENU_LOCATION_TRACKING, Menu.NONE, "Location");
		return true;
	}

	@Override
	public void onCalloutBalloonOfPOIItemTouched(MapView mapView,
			MapPOIItem poiItem) {
		// TODO Auto-generated method stub
		// POI Item의 말풍선이 터치되었을 때
		Intent intent = new Intent(this, MapDetailActivity.class);
		intent.putExtra("map_id", poiItem.getTag());
		intent.putExtra("map_name", poiItem.getItemName());
		startActivity(intent);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}

	@Override
	public void onDraggablePOIItemMoved(MapView arg0, MapPOIItem arg1,
			MapPoint arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPOIItemSelected(MapView arg0, MapPOIItem arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCurrentLocationDeviceHeadingUpdate(MapView mapView,
			float headingAngle) {
		// TODO Auto-generated method stub
		Log.i(LOG_TAG,
				String.format(
						"MapView onCurrentLocationDeviceHeadingUpdate: device heading = %f degrees",
						headingAngle));
	}

	@Override
	public void onCurrentLocationUpdate(MapView mapView,
			MapPoint currentLocation, float accuracyInMeters) {
		// TODO Auto-generated method stub
		MapPoint.GeoCoordinate mapPointGeo = currentLocation
				.getMapPointGeoCoord();
		Log.i(LOG_TAG, String.format(
				"MapView onCurrentLocationUpdate (%f,%f) accuracy (%f)",
				mapPointGeo.latitude, mapPointGeo.longitude, accuracyInMeters));
	}

	@Override
	public void onCurrentLocationUpdateCancelled(MapView arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCurrentLocationUpdateFailed(MapView arg0) {
		// TODO Auto-generated method stub
		// 현위치 갱신 실패 시
	}

	@Override
	public void onMapViewCenterPointMoved(MapView arg0, MapPoint arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapViewDoubleTapped(MapView arg0, MapPoint arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapViewInitialized(MapView arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapViewLongPressed(MapView arg0, MapPoint arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapViewSingleTapped(MapView arg0, MapPoint arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapViewZoomLevelChanged(MapView arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDaumMapOpenAPIKeyAuthenticationResult(MapView arg0, int arg1,
			String arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		final int itemId = item.getItemId();

		switch (itemId) {
		case MENU_MAP_TYPE: {
			String hdMapTile = mapView.isHDMapTileEnabled() ? "HD Map Tile Off"
					: "HD Map Tile On";
			String[] mapTypeMenuItems = { "Standard", "Satellite", "Hybrid",
					hdMapTile, "Clear Map Tile Cache" };

			Builder dialog = new Builder(this);
			dialog.setTitle("Map Type");
			dialog.setItems(mapTypeMenuItems, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case 0: // Standard
					{
						mapView.setMapType(MapView.MapType.Standard);
					}
						break;
					case 1: // Satellite
					{
						mapView.setMapType(MapView.MapType.Satellite);
					}
						break;
					case 2: // Hybrid
					{
						mapView.setMapType(MapView.MapType.Hybrid);
					}
						break;
					case 3: // HD Map Tile On/Off
					{
						if (mapView.isHDMapTileEnabled()) {
							mapView.setHDMapTileEnabled(false);
						} else {
							mapView.setHDMapTileEnabled(true);
						}
					}
						break;
					case 4: // Clear Map Tile Cache
					{
						MapView.clearMapTilePersistentCache();
					}
						break;
					}
				}
			});
			dialog.show();
		}
			return true;
		case MENU_LOCATION_TRACKING: {
			String[] mapMoveMenuItems = { "User Location On",
					"User Location+Heading On" };
			Builder dialog = new Builder(this);
			dialog.setTitle("Location Tracking");
			dialog.setItems(mapMoveMenuItems, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case 0: // User Location On
					{
						mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOnWithoutHeading);
					}
						break;
					case 1: // User Location+Heading On
					{
						mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOnWithHeading);
					}
						break;
					}
				}
			});
			dialog.show();
		}
			return true;
		case MENU_MAP_MOVE: {
			String rotateMapMenu = mapView.getMapRotationAngle() == 0.0f ? "Rotate Map 60"
					: "Unrotate Map";
			String[] mapMoveMenuItems = { "Move to", "Zoom In", "Zoom Out",
					rotateMapMenu };
			Builder dialog = new Builder(this);
			dialog.setTitle("Map Type");
			dialog.setItems(mapMoveMenuItems, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case 0: // Move to
					{
						mapView.setMapCenterPoint(MapPoint
								.mapPointWithGeoCoord(37.3752729, 126.633031),
								true);
					}
						break;
					case 1: // Zoom In
					{
						mapView.zoomIn(true);
					}
						break;
					case 2: // Zoom Out
					{
						mapView.zoomOut(true);
					}
						break;
					case 3: // Rotate Map 60, Unrotate Map
					{
						if (mapView.getMapRotationAngle() == 0.0f) {
							mapView.setMapRotationAngle(60.0f, true);
						} else {
							mapView.setMapRotationAngle(0.0f, true);
						}
					}
						break;
					}
				}

			});
			dialog.show();
		}
			return true;

		}
		return false;
	}

	// public static void refreshMapInfo() {
	// try {
	//
	// JSONArray array = Face3Utils.getUrlLongFromList(SmartCampusVariable
	// .getServerHttpUrl("/campusmapdata/getMapPinList.do"), null);
	//
	// String[] loadTables = { "campus_map" };
	// if (array == null) {
	// return;
	// }
	// for (String maptable : loadTables) {
	// SmartCampusSingleton.getInstance().getDbResource()
	// .getAbatisService().execute("delete_" + maptable);
	// GtAndroidUtils.jsonToSqlite(array, "insert_" + maptable);
	// }
	// } catch (ClientProtocolException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	public void createPoiItem(int mapId, String mapName, Double latitude,
			Double longitude, int position) {
		MapPOIItem poiItem = new MapPOIItem();
		poiItem.setItemName(mapName);
		poiItem.setMapPoint(MapPoint.mapPointWithGeoCoord(latitude, longitude));
		// poiItem.setMarkerType(MapPOIItem.MarkerType.BluePin);
		poiItem.setMarkerType(MapPOIItem.MarkerType.CustomImage);
		poiItem.setCustomImageResourceId(pinArray[position]);
		poiItem.setShowAnimationType(MapPOIItem.ShowAnimationType.DropFromHeaven);
		poiItem.setShowCalloutBalloonOnTouch(true);
		// poiItem2.setDraggable(true);
		poiItem.setTag(mapId);
		mapView.addPOIItem(poiItem);

	}

	public void whilePinMap() {
		for (int i = 0; i < mapNameArray.length; i++) {
			int idx = i + 1;

			createPoiItem(idx, mapNameArray[i], latitudeArray[i],
					longitudeArray[i], i);
		}
	}

//	public void setSongdo() {
//		btnSong.setBackgroundResource(R.drawable.map_song2);
//		btnJe.setBackgroundResource(R.drawable.map_je);
//		mapView.setMapCenterPointAndZoomLevel(
//				MapPoint.mapPointWithGeoCoord(37.3752729, 126.633031), 1, true);
//	}
//
//	public void setJemulpo() {
//		btnSong.setBackgroundResource(R.drawable.map_song);
//		btnJe.setBackgroundResource(R.drawable.map_je2);
//		mapView.setMapCenterPointAndZoomLevel(
//				MapPoint.mapPointWithGeoCoord(37.471903, 126.657375), 1, true);
//	}

	public void search(View v){
		Intent intent = new Intent(this, MapSearchActivity.class);
		intent.putExtra("searchStr", searchText.getText().toString());
		startActivity(intent);
	}
}
