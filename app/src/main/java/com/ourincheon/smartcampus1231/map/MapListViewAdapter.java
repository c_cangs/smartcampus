package com.ourincheon.smartcampus1231.map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ourincheon.smartcampus1231.R;

import java.util.List;
import java.util.Map;

public class MapListViewAdapter extends BaseAdapter {

	private Context mContext;
	private List<Map<String, Object>> mData;
	private LayoutInflater mInflater;

	public MapListViewAdapter(Context context, List<Map<String, Object>> data) {
		this.mContext = context;
		this.mData = data;
		this.mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		return mData.size(); // To change body of implemented methods use
								// File |
								// Settings | File Templates.
	}

	@Override
	public Object getItem(int i) {

		return mData.get(i); // To change body of implemented methods
								// use File |
								// Settings | File Templates.
	}

	@Override
	public long getItemId(int i) {
		return i; // To change body of implemented methods use File | Settings |
					// File Templates.
	}

	@Override
	public View getView(int position, View view, ViewGroup viewGroup) {

		if (view == null) {
			view = mInflater.inflate(R.layout.map_detail_row_textview, null);
		}

		TextView mapLeftTextView = (TextView) view
				.findViewById(R.id.map_detail_row_textview_left);
		TextView mapRightTextView = (TextView) view
				.findViewById(R.id.map_detail_row_textview_right);
		mapLeftTextView.setText(mData.get(position).get("LOCATION").toString());
		mapRightTextView.setText(mData.get(position).get("INFO_NAME")
				.toString());

		return view; // To change body of implemented methods use File |
						// Settings | File Templates.
	}
}
