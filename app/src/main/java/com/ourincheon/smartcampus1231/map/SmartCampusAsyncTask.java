package com.ourincheon.smartcampus1231.map;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;

interface TaskFunction {
	void doRun();
	void doFinish();
}

public class SmartCampusAsyncTask extends AsyncTask<String, Integer, String>{


	TaskFunction taskFunction;
	
	private Context curContext;
	private ProgressDialog dialog;
	private Handler barHandler = new Handler();
	private String loadingTitle;
	
	
	public void doRunExecute(Context context,TaskFunction taskRun) {
		this.taskFunction = taskRun;
		this.curContext = context;
		this.loadingTitle = "잠시만 기다려주세요";
		execute("");
	}
	
	public void doRunExecute(Context context,TaskFunction taskRun, String loadingTitle) {
		this.taskFunction = taskRun;
		this.curContext = context;
		this.loadingTitle = loadingTitle;
		execute("");
	}
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		barHandler.post(new Runnable() {
			public void run() {
				dialog = ProgressDialog.show(curContext,"스마트캠퍼스",
						loadingTitle, true);
				dialog.setCanceledOnTouchOutside(false);
			}
		});
		taskFunction.doRun();
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		// super.onPostExecute(result);
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
		taskFunction.doFinish();
	}

}
