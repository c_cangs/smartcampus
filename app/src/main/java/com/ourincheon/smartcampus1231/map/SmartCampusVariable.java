package com.ourincheon.smartcampus1231.map;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.widget.LinearLayout;

/**
 * Created by IntelliJ IDEA. User: Administrator Date: 11. 9. 19 Time: 오후 6:22
 * To change this template use File | Settings | File Templates.
 */
public class SmartCampusVariable {

	public static final String SERVER_SUCCESS_STRING = "SUCCESS";
	public static final String WEB_SERVER_URL = "117.16.191.242";
	// public static final String WEB_SERVER_URL = "appcenter.hustay.co.kr";
	// public static final String WEB_SERVER_URL = "10.80.41.218";
	public static final String WEB_SERVER_CONTEXT_PATH = "SmartCampus_Web";
	public static final String WEB_SERVER_PORT = "80";
	// public static final String WEB_SERVER_PORT = "8081";
	// public static final String WEB_SERVER_PORT = "8080";
	public static final String DEV_MODE = "vm_";
	public static final String ID_PROPERTY = "student_id";
	public static final String IS_LOGIN = "is_login";
	public static final String IS_LOGOUT = "is_logout";
	public static final String GET_REVIEW = "get_review";
	public static final String YEAR_PROPERTY = "cur_year";
	public static final String TERM_PROPERTY = "cur_term";
	public static final String LOCK_PROPERTY = "config_lock";
	public static final String LOCK_PASSWORD_PROPERTY = "config_lock_password";
	public static final String PASSWORD_PROPERTY = "student_password";
	public static final int DISPLAY_HEIGHT = 800;
	public static int DISPLAY_WIDTH = 480;
	public static final int DISPLAY_HEADER_HEIGHT = 96;
	public static final int SOCK_TIMEOUT = 10000;
	public static final int LONG_SOCK_TIMEOUT = 50000;
	public static final String strColorBlue = "#5CA6DB";
	public static final String strColorGreen = "#A1CE63";
	public static final String strColorOrange = "#F27C4B";
	public static final String IS_COMMENT_PUSH = "is_comment_push";
	public static final String IS_NOTICE_PUSH = "is_notice_push";
	public static LinearLayout behindView = null;
	public static Boolean noMember = false;
	public static Boolean gradeGuide = true;

	public static final String getServerImageUrl() {
		return "http://" + WEB_SERVER_URL + ":" + WEB_SERVER_PORT + "/"
				+ "SmartCampus_Web/";
	}

	public static final String getServerHttpUrl(String strProgUrl) {
		if (strProgUrl.charAt(0) == '/') {
			return getServerHttpUrl() + strProgUrl;
		} else {
			return getServerHttpUrl() + "/" + strProgUrl;
		}
	}

	public static final String getServerHttpUrl() {
		return "http://" + WEB_SERVER_URL + ":" + WEB_SERVER_PORT + "/"
				+ WEB_SERVER_CONTEXT_PATH;

	}

	public static final String getVersionName(Context context) {
		try {
			PackageInfo pi = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			return pi.versionName;
		} catch (NameNotFoundException e) {
			return null;
		}
	}

}
