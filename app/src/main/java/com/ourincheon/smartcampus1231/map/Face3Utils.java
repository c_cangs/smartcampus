/*
 *
 * <P>
 * Copyright 2008 짤 General Motors   All Rights Reserved.
 * </P>
 *
 * @author <Gitae Kim>
 * @version 2.0
 * @dateCreated 2010. 9. 27
 *
 * Change Log
 * ===================================================================
 * Date             By         Version    /methods        Description
 * ----------   -----------    --------   ------------   ------------
 * 2010. 9. 27      < Gitae Kim >       1.0                    <Create Face3Utils>
 * ===================================================================
 */
package com.ourincheon.smartcampus1231.map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Gitae Kim
 */
public class Face3Utils {

    static public JSONArray getListFromJson(String strJson, String rootStr)
            throws JSONException {
        // JSON json = JSONSerializer.toJSON(strJson);

        JSONArray jsonArray = null;
        try {
            JSONObject jsonObj = new JSONObject(strJson);
            jsonArray = (JSONArray) jsonObj.get(rootStr);
        } catch (Exception e) {
        }


        return jsonArray;

    }

    static public JSONObject getObjectFromJson(String strJson)
            throws JSONException {
        // JSON json = JSONSerializer.toJSON(strJson);
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(strJson);
        } catch (Exception e) {
        }


        return jsonObj;

    }

    static public JSONObject getUrlToJsonObject(String url,
            Map<String, Object> param) throws ClientProtocolException,
            IOException, JSONException {


        String line = Face3Utils.getUrlToString(url, param, SmartCampusVariable.SOCK_TIMEOUT);

        return Face3Utils.getObjectFromJson(line);

    }

    static public JSONObject getUrlLongToJsonObject(String url,
            Map<String, Object> param) throws ClientProtocolException,
            IOException, JSONException {


        String line = Face3Utils.getUrlToString(url, param, SmartCampusVariable.LONG_SOCK_TIMEOUT);

        return Face3Utils.getObjectFromJson(line);

    }
    static public JSONArray getUrlLongFromList(String url, Map<String, Object> param)
            throws ClientProtocolException, IOException, JSONException {

        String line = Face3Utils.getUrlToString(url, param, SmartCampusVariable.SOCK_TIMEOUT);

        return Face3Utils.getListFromJson(line, "list");

    }

    static public JSONArray getUrlFromList(String url, Map<String, Object> param)
            throws ClientProtocolException, IOException, JSONException {

        String line = Face3Utils.getUrlToString(url, param, SmartCampusVariable.SOCK_TIMEOUT);

        return Face3Utils.getListFromJson(line, "list");

    }

    static public String getUrlToString(String url, Map<String, Object> param) {
        return getUrlToString(url, param, SmartCampusVariable.SOCK_TIMEOUT);
    }

    static public String getUrlToString(String url, Map<String, Object> param, int timeout) {


        String line = "";

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpParams params = httpClient.getParams();

        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpConnectionParams.setSoTimeout(params, timeout);
        HttpPost httpPost = new HttpPost(url);

        if (param == null) {
            param = new HashMap<String, Object>();
        }

//        
//        String stduentId = SmartCampusSingleton.getInstance().getSharedPreference().getString(SmartCampusVariable.ID_PROPERTY, null);
//        String strPassword = SmartCampusSingleton.getInstance().getSharedPreference().getString(SmartCampusVariable.PASSWORD_PROPERTY, null);
//
//        if (!(stduentId == null || strPassword == null)) {
//            param.put("sec_student_no", stduentId);
//            param.put("sec_password", strPassword);
//        }

        List<NameValuePair> nameValuePairs;
        nameValuePairs = new ArrayList<NameValuePair>(2);

        for (Object obj : param.keySet().toArray()) {
            String key = obj.toString();

            String paramValue = String.valueOf(param.get(key));
            nameValuePairs.add(new BasicNameValuePair(key, paramValue));
        }

        UrlEncodedFormEntity entity = null;
        try {
            entity = new UrlEncodedFormEntity(
                    nameValuePairs, HTTP.UTF_8);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Face3Utils.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }

        httpPost.setEntity(entity);



        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(httpPost);
        } catch (IOException ex) {
            Logger.getLogger(Face3Utils.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }

        try {
            HttpEntity httpEntity = httpResponse.getEntity();
            line = EntityUtils.toString(httpEntity);
        } catch (IOException ex) {
            Logger.getLogger(Face3Utils.class.getName()).log(Level.SEVERE, null, ex);
            //Toast.makeText(SmartCampusSingleton.getInstance().getCurrentActivity(), "통신 접속 Error", Toast.LENGTH_SHORT).show();
            return "";

        } catch (ParseException ex) {
            Logger.getLogger(Face3Utils.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
        return line;

    }
}
