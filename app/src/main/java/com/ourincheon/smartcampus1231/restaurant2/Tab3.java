package com.ourincheon.smartcampus1231.restaurant2;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.RestClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

/**
 * Created by seonilkim on 2017. 1. 24..
 */

public class Tab3 extends Fragment {
    View view;
    RequestParams params = new RequestParams();
    private RestClient restClient;

    TextView teach_10, teach_11, teach_12, teach_20, tv_time_teach;
    View teach_line_11, teach_line_12;

    private ProgressDialog mProgressDialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = (View) inflater.inflate(R.layout.restaurant2_tab3, container, false);

        teach_10 = (TextView) view.findViewById(R.id.teach_10);
        teach_11 = (TextView) view.findViewById(R.id.teach_11);
        teach_12 = (TextView) view.findViewById(R.id.teach_12);
        teach_20 = (TextView) view.findViewById(R.id.teach_20);
        teach_line_11 = (View) view.findViewById(R.id.teach_line_11);
        teach_line_12 = (View) view.findViewById(R.id.teach_line_12);
        tv_time_teach = (TextView) view.findViewById(R.id.tv_time_teach);

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(new Date());
        params.put("date", today);
        params.put("page", 3);

//        mProgressDialog = new ProgressDialog(getActivity());
//        mProgressDialog.show();

        try {
            Date today2 = new Date();
            Date date1 = sdf.parse("2017-03-02");
            Date date2 = sdf.parse("2017-06-21");
            Date date3 = sdf.parse("2017-09-04");
            Date date4 = sdf.parse("2017-12-22");
            if((today2.after(date1) && today2.before(date2)) || (today2.after(date3) && today2.before(date4))){
                //학기중엔 학기중 시간표
                tv_time_teach.setText("평일 :	중식 11:30 ~ 13:30\n          석식 17:00 ~ 19:00\n주말 :	휴점");
            } else {
                tv_time_teach.setText("평일 :	중식 11:30 ~ 13:30\n          석식 17:00 ~ 18:00\n주말 :	휴점");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        restClient.get("restaurant", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                ArrayList<String> teach1 = new ArrayList<String>();
                ArrayList<String> teach2 = new ArrayList<String>();

                String text="";
                for(int i=0;i<response.length();i++){
                    try {
                        JSONObject object = response.getJSONObject(i);
                        text=object.get("menu").toString();
                        if(text.length()!=0 && !text.isEmpty() && !text.equals("null")) {
                            if (object.get("corner").equals("중식")) teach1.add(text);
                            else if (object.get("corner").equals("석식")) teach2.add(text);
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }

                switch (teach1.size()){
                    case 0:
                        teach_10.setVisibility(View.VISIBLE);
                        teach_10.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        teach_10.setText(R.string.no_today);
                        break;
                    case 1:
                        teach_10.setVisibility(View.VISIBLE);
                        teach_10.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        teach_10.setText(teach1.get(0));
                        break;
                    case 2:
                        teach_10.setVisibility(View.VISIBLE);
                        teach_11.setVisibility(View.VISIBLE);
                        teach_line_11.setVisibility(View.VISIBLE);
                        teach_10.setText(teach1.get(0));
                        teach_11.setText(teach1.get(1));
                        break;
                    case 3:
                        teach_10.setVisibility(View.VISIBLE);
                        teach_11.setVisibility(View.VISIBLE);
                        teach_12.setVisibility(View.VISIBLE);
                        teach_line_11.setVisibility(View.VISIBLE);
                        teach_line_12.setVisibility(View.VISIBLE);
                        teach_10.setText(teach1.get(0));
                        teach_11.setText(teach1.get(1));
                        teach_12.setText(teach1.get(2));
                        break;
                }

                switch (teach2.size()){
                    case 0:
                        teach_20.setVisibility(View.VISIBLE);
                        teach_20.setText(R.string.no_today);
                        break;
                    case 1:
                        teach_20.setVisibility(View.VISIBLE);
                        teach_20.setText(teach2.get(0));
                        break;
                }
//                mProgressDialog.dismiss();
            }
        });

        return view;
    }
}
