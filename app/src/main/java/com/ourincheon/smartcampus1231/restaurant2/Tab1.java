package com.ourincheon.smartcampus1231.restaurant2;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.RestClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

/**
 * Created by seonilkim on 2017. 1. 24..
 */

public class Tab1 extends Fragment {
    View view;
    RequestParams params = new RequestParams();
    private RestClient restClient;

    TextView svc_10, svc_11, svc_12, svc_13, svc_20, svc_21, svc_30, svc_40, svc_50, cafet_10, cafet_20, cafet_21, cafet_30, cafet_31, tv_time_svc, tv_time_cafeteria;
    View svc_line_11, svc_line_12, svc_line_13, svc_line_21, cafet_line_21, cafet_line_31;

    private ProgressDialog mProgressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = (View) inflater.inflate(R.layout.restaurant2_tab1, container, false);
        restClient = new RestClient(getActivity());

        svc_10 = (TextView) view.findViewById(R.id.svc_10);
        svc_11 = (TextView) view.findViewById(R.id.svc_11);
        svc_12 = (TextView) view.findViewById(R.id.svc_12);
        svc_13 = (TextView) view.findViewById(R.id.svc_13);
        svc_20 = (TextView) view.findViewById(R.id.svc_20);
        svc_21 = (TextView) view.findViewById(R.id.svc_21);
        svc_30 = (TextView) view.findViewById(R.id.svc_30);
        svc_40 = (TextView) view.findViewById(R.id.svc_40);
        svc_50 = (TextView) view.findViewById(R.id.svc_50);
        cafet_10 = (TextView) view.findViewById(R.id.cafet_10);
        cafet_20 = (TextView) view.findViewById(R.id.cafet_20);
        cafet_21 = (TextView) view.findViewById(R.id.cafet_21);
        cafet_30 = (TextView) view.findViewById(R.id.cafet_30);
        cafet_31 = (TextView) view.findViewById(R.id.cafet_31);
        svc_line_11 = (View) view.findViewById(R.id.svc_line_11);
        svc_line_12 = (View) view.findViewById(R.id.svc_line_12);
        svc_line_13 = (View) view.findViewById(R.id.svc_line_13);
        svc_line_21 = (View) view.findViewById(R.id.svc_line_21);
        cafet_line_21 = (View) view.findViewById(R.id.cafet_line_21);
        cafet_line_31 = (View) view.findViewById(R.id.cafet_line_31);

        tv_time_svc = (TextView) view.findViewById(R.id.tv_time_svc);
        tv_time_cafeteria = (TextView) view.findViewById(R.id.tv_time_cafeteria);

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(new Date());
        params.put("date", today);
        params.put("page", 1);
//        params.put("name", "복지회관");
//        params.put("corner", "1코너");
//        params.put("menu_num", 0);


//        mProgressDialog = new ProgressDialog(getActivity());
//        mProgressDialog.show();

        try {
            Date today2 = new Date();
            Date date1 = sdf.parse("2017-03-02");
            Date date2 = sdf.parse("2017-06-21");
            Date date3 = sdf.parse("2017-09-04");
            Date date4 = sdf.parse("2017-12-22");
            if((today2.after(date1) && today2.before(date2)) || (today2.after(date3) && today2.before(date4))){
                //학기중엔 학기중 시간표
                tv_time_svc.setText("평일 : 10:00 ~ 19:00\n주말 : 휴점");
                tv_time_cafeteria.setText("평일 : 중식 11:00 ~ 14:00\n          석식 17:00 ~ 19:00\n주말 : 휴점");
            } else {
                tv_time_svc.setText("평일 : 08:00 ~ 18:30\n주말 : 08:30 ~ 18:30\n(14:00 ~ 15:00 휴식시간)");
                tv_time_cafeteria.setText("휴점 (방학)");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        restClient.get("restaurant", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                ArrayList<String> svc1 = new ArrayList<>();
                ArrayList<String> svc2 = new ArrayList<>();
                ArrayList<String> svc3 = new ArrayList<>();
                ArrayList<String> svc4 = new ArrayList<>();
                ArrayList<String> svc5 = new ArrayList<>();
                ArrayList<String> cafet1 = new ArrayList<>();
                ArrayList<String> cafet2 = new ArrayList<>();
                ArrayList<String> cafet3 = new ArrayList<>();
//                ArrayList<String> cafe
                String text="";
                for(int i=0;i<response.length();i++){
                    try {
                        JSONObject object = response.getJSONObject(i);
                        text=object.get("menu").toString().trim();
                        if(text.length()!=0 && !text.isEmpty() && !text.equals("null")) {
                            if(object.get("name").equals("복지회관")) {
                                if (object.get("corner").equals("1코너")) svc1.add(text);
                                else if (object.get("corner").equals("2코너")) svc2.add(text);
                                else if (object.get("corner").equals("3코너")) svc3.add(text);
                                else if (object.get("corner").equals("4코너")) svc4.add(text);
                                else if (object.get("corner").equals("5코너")) svc5.add(text);
                            }
                            else if(object.get("name").equals("카페테리아")){
//                                Log.d("MENU", object.toString());
                                if (object.get("corner").equals("A코너_중식")) cafet1.add(text);
                                else if (object.get("corner").equals("A코너_석식")) cafet2.add(text);
                                else if (object.get("corner").equals("B코너")) {
                                    cafet3.add(text);
                                    Log.d("TEXT", text);
                                }
                            }
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }

                switch (svc1.size()){
                    case 0:
                        svc_10.setVisibility(View.VISIBLE);
                        svc_10.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        svc_10.setText(R.string.no_today);
                        break;
                    case 1:
                        svc_10.setVisibility(View.VISIBLE);
                        svc_10.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        svc_10.setText(svc1.get(0));
                        break;
                    case 2:
                        svc_10.setVisibility(View.VISIBLE);
                        svc_11.setVisibility(View.VISIBLE);
                        svc_line_11.setVisibility(View.VISIBLE);
                        svc_10.setText(svc1.get(0));
                        svc_11.setText(svc1.get(1));
                        break;
                    case 3:
                        svc_10.setVisibility(View.VISIBLE);
                        svc_11.setVisibility(View.VISIBLE);
                        svc_12.setVisibility(View.VISIBLE);
                        svc_line_11.setVisibility(View.VISIBLE);
                        svc_line_12.setVisibility(View.VISIBLE);
                        svc_10.setText(svc1.get(0));
                        svc_11.setText(svc1.get(1));
                        svc_12.setText(svc1.get(2));
                        break;
                    case 4:
                        svc_10.setVisibility(View.VISIBLE);
                        svc_11.setVisibility(View.VISIBLE);
                        svc_12.setVisibility(View.VISIBLE);
                        svc_13.setVisibility(View.VISIBLE);
                        svc_line_11.setVisibility(View.VISIBLE);
                        svc_line_12.setVisibility(View.VISIBLE);
                        svc_line_13.setVisibility(View.VISIBLE);
                        svc_10.setText(svc1.get(0));
                        svc_11.setText(svc1.get(1));
                        svc_12.setText(svc1.get(2));
                        svc_13.setText(svc1.get(3));
                        break;
                }

                switch (svc2.size()){
                    case 0:
                        svc_20.setVisibility(View.VISIBLE);
                        svc_20.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        svc_20.setText(R.string.no_today);
                        break;
                    case 1:
                        svc_20.setVisibility(View.VISIBLE);
                        svc_20.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        svc_20.setText(svc2.get(0));
                        if(svc2.get(0).equals(""))
                        break;
                    case 2:
                        svc_20.setVisibility(View.VISIBLE);
                        svc_21.setVisibility(View.VISIBLE);
                        svc_line_21.setVisibility(View.VISIBLE);
                        svc_20.setText(svc2.get(0));
                        svc_21.setText(svc2.get(1));
                        break;
                }

                switch (svc3.size()){
                    case 0:
                        svc_30.setVisibility(View.VISIBLE);
                        svc_30.setText(R.string.no_today);
                        break;
                    case 1:
                        svc_30.setVisibility(View.VISIBLE);
                        svc_30.setText(svc3.get(0));
                        break;
                }

                switch (svc4.size()){
                    case 0:
                        svc_40.setVisibility(View.VISIBLE);
                        svc_40.setText(R.string.no_today);
                        break;
                    case 1:
                        svc_40.setVisibility(View.VISIBLE);
                        svc_40.setText(svc4.get(0));
                        break;
                }

                switch (svc5.size()){
                    case 0:
                        svc_50.setVisibility(View.VISIBLE);
                        svc_50.setText(R.string.no_today);
                        break;
                    case 1:
                        svc_50.setVisibility(View.VISIBLE);
                        svc_50.setText(svc5.get(0));
                        break;
                }

                switch (cafet1.size()){
                    case 0:
                        cafet_10.setVisibility(View.VISIBLE);
                        cafet_10.setText(R.string.no_today);
                        break;
                    case 1:
                        cafet_10.setVisibility(View.VISIBLE);
                        cafet_10.setText(cafet1.get(0));
                        break;
                }

                switch (cafet2.size()){
                    case 0:
                        cafet_20.setVisibility(View.VISIBLE);
                        cafet_20.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        cafet_20.setText(R.string.no_today);
                        break;
                    case 1:
                        cafet_20.setVisibility(View.VISIBLE);
                        cafet_20.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        cafet_20.setText(cafet2.get(0));
                        break;
                    case 2:
                        cafet_20.setVisibility(View.VISIBLE);
                        cafet_21.setVisibility(View.VISIBLE);
                        cafet_line_21.setVisibility(View.VISIBLE);
                        cafet_20.setText(cafet2.get(0));
                        cafet_21.setText(cafet2.get(1));
                        break;
                }

                switch (cafet3.size()){
                    case 0:
                        cafet_30.setVisibility(View.VISIBLE);
                        cafet_30.setText(R.string.no_today);
                        break;
                    case 1:
                        cafet_30.setVisibility(View.VISIBLE);
                        cafet_30.setText(cafet3.get(0));
                        break;
                    case 2:
                        cafet_30.setVisibility(View.VISIBLE);
                        cafet_30.setText(cafet3.get(0));
                        cafet_31.setVisibility(View.VISIBLE);
                        cafet_31.setText(cafet3.get(1));
                        cafet_line_31.setVisibility(View.VISIBLE);
                        break;
                }



//                mProgressDialog.dismiss();
            }
        });
        return view;
    }

}
