package com.ourincheon.smartcampus1231.restaurant2;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.ourincheon.smartcampus1231.R;

/**
 * Created by seonilkim on 2017. 1. 24..
 */

public class Restaurant2 extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    public static Context r2Context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        r2Context = getApplicationContext();

        Button mapBackBtn = (Button) findViewById(R.id.restaurant_back);
        mapBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tab_restaurnat);
        viewPager = (ViewPager) findViewById(R.id.view_restaurant);

        tabLayout.addTab(tabLayout.newTab().setText("복지회관/카페테리아"));
        tabLayout.addTab(tabLayout.newTab().setText("사범대식당/생활관"));
        tabLayout.addTab(tabLayout.newTab().setText("교직원식당"));

        TabPagerAdapter pagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
