package com.ourincheon.smartcampus1231.restaurant2;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.RestClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

/**
 * Created by seonilkim on 2017. 1. 24..
 */

public class Tab2 extends Fragment {
    View view;
    RequestParams params = new RequestParams();
    private RestClient restClient;

    TextView mch_10, mch_11, mch_12, mch_20, dorm_10, dorm_20, dorm_30, tv_time_mch, tv_time_dorm1, tv_time_dorm2;
    View mch_line_11, mch_line_12;

    private ProgressDialog mProgressDialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = (View) inflater.inflate(R.layout.restaurant2_tab2, container, false);

        mch_10 = (TextView) view.findViewById(R.id.mch_10);
        mch_11 = (TextView) view.findViewById(R.id.mch_11);
        mch_12 = (TextView) view.findViewById(R.id.mch_12);
        mch_20 = (TextView) view.findViewById(R.id.mch_20);
        dorm_10 = (TextView) view.findViewById(R.id.dorm_10);
        dorm_20 = (TextView) view.findViewById(R.id.dorm_20);
        dorm_30 = (TextView) view.findViewById(R.id.dorm_30);
        mch_line_11 = (View) view.findViewById(R.id.mch_line_11);
        mch_line_12 = (View) view.findViewById(R.id.mch_line_12);

        tv_time_mch = (TextView) view.findViewById(R.id.tv_time_mch);
        tv_time_dorm1 = (TextView) view.findViewById(R.id.tv_time_dorm1);
        tv_time_dorm2 = (TextView) view.findViewById(R.id.tv_time_dorm2);

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(new Date());
        params.put("date", today);
        params.put("page", 2);

//        mProgressDialog = new ProgressDialog(getActivity());
//        mProgressDialog.show();

        try {
            Date today2 = new Date();
            Date date1 = sdf.parse("2017-03-02");
            Date date2 = sdf.parse("2017-06-21");
            Date date3 = sdf.parse("2017-09-04");
            Date date4 = sdf.parse("2017-12-22");
            if((today2.after(date1) && today2.before(date2)) || (today2.after(date3) && today2.before(date4))){
                //학기중엔 학기중 시간표
                tv_time_mch.setText("평일 : 중식 11:00 ~ 14:00\n          석식 17:00 ~ 18:30\n주말 : 휴점");
                tv_time_dorm1.setText("평일 : 조식 07:30 ~ 09:30\n          중식 11:30 ~ 13:30\n          석식 17:00 ~ 19:00");
                tv_time_dorm2.setText("주말 : 조식 08:00 ~ 10:00\n          중식 12:00 ~ 13:30\n          석식 17:00 ~ 19:00");
            } else {
                tv_time_mch.setText("평일 : 11:00 ~ 14:00\n주말 : 휴점");
                tv_time_dorm1.setText("");
                tv_time_dorm2.setText("휴점 (방학)");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        restClient.get("restaurant", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                ArrayList<String> mch1 = new ArrayList<String>();
                ArrayList<String> mch2 = new ArrayList<String>();
                ArrayList<String> dorm1 = new ArrayList<String>();
                ArrayList<String> dorm2 = new ArrayList<String>();
                ArrayList<String> dorm3 = new ArrayList<String>();

                String text="";
                for(int i=0;i<response.length();i++){
                    try {
                        JSONObject object = response.getJSONObject(i);
                        text=object.get("menu").toString().trim();
                        if(text.length()!=0 && !text.isEmpty() && !text.equals("null")) {
                            if(object.get("name").equals("사범대식당")) {
                                if (object.get("corner").equals("중식")) mch1.add(text);
                                else if (object.get("corner").equals("석식")) mch2.add(text);
                            }
                            else if(object.get("name").equals("생활관")){
                                if (object.get("corner").equals("조식")) dorm1.add(text);
                                else if (object.get("corner").equals("중식")) dorm2.add(text);
                                else if (object.get("corner").equals("석식")) dorm3.add(text);
                            }
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }

                switch (mch1.size()){
                    case 0:
                        mch_10.setVisibility(View.VISIBLE);
                        mch_10.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        mch_10.setText(R.string.no_today);
                        break;
                    case 1:
                        mch_10.setVisibility(View.VISIBLE);
                        mch_10.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        mch_10.setText(mch1.get(0));
                        break;
                    case 2:
                        mch_10.setVisibility(View.VISIBLE);
                        mch_11.setVisibility(View.VISIBLE);
                        mch_line_11.setVisibility(View.VISIBLE);
                        mch_10.setText(mch1.get(0));
                        mch_11.setText(mch1.get(1));
                        break;
                    case 3:
                        mch_10.setVisibility(View.VISIBLE);
                        mch_11.setVisibility(View.VISIBLE);
                        mch_12.setVisibility(View.VISIBLE);
                        mch_line_11.setVisibility(View.VISIBLE);
                        mch_line_12.setVisibility(View.VISIBLE);
                        mch_10.setText(mch1.get(0));
                        mch_11.setText(mch1.get(1));
                        mch_12.setText(mch1.get(2));
                        break;
                }

                switch (mch2.size()){
                    case 0:
                        mch_20.setVisibility(View.VISIBLE);
                        mch_20.setText(R.string.no_today);
                        break;
                    case 1:
                        mch_20.setVisibility(View.VISIBLE);
                        mch_20.setText(mch2.get(0));
                        break;
                }

                switch (dorm1.size()){
                    case 0:
                        dorm_10.setVisibility(View.VISIBLE);
                        dorm_10.setText(R.string.no_today);
                        break;
                    case 1:
                        dorm_10.setVisibility(View.VISIBLE);
                        dorm_10.setText(dorm1.get(0));
                        break;
                }

                switch (dorm2.size()){
                    case 0:
                        dorm_20.setVisibility(View.VISIBLE);
                        dorm_20.setText(R.string.no_today);
                        break;
                    case 1:
                        dorm_20.setVisibility(View.VISIBLE);
                        dorm_20.setText(dorm2.get(0));
                        break;
                }

                switch (dorm3.size()){
                    case 0:
                        dorm_30.setVisibility(View.VISIBLE);
                        dorm_30.setText(R.string.no_today);
                        break;
                    case 1:
                        dorm_30.setVisibility(View.VISIBLE);
                        dorm_30.setText(dorm3.get(0));
                        break;
                }
//                mProgressDialog.dismiss();
            }
        });

        return view;
    }
}
