package com.ourincheon.smartcampus1231.timetable;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.ourincheon.smartcampus1231.Login.LoginActiviy;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.RestClient;
import com.ourincheon.smartcampus1231.System.DwAndroidUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

/**
 * Created by kyun on 2016-02-24.
 */
public class ClassinfoActivity extends AppCompatActivity {

    List<Map<String, Object>> classinfo = new ArrayList<Map<String, Object>>();
    RequestParams params = new RequestParams();
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.classes_info);
        RestClient restClient = new RestClient(this);
        final TextView class_name, class_prof, class_room, class_time, class_book, class_in;
        Intent intent = getIntent();
        final String id = intent.getStringExtra("id");
        final String croom = intent.getStringExtra("room");
        final Button stulist,back;

        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int term;
        if (month < 6) term = 1;
        else term = 2;
        final String y = Integer.toString(year);
        final String t = Integer.toString(term);
        params.put("year", y);
        params.put("term", t);
        params.put("sno", id);

        class_name = (TextView) findViewById(R.id.class_name);
        class_prof = (TextView) findViewById(R.id.class_prof);
        class_room = (TextView) findViewById(R.id.class_room);
        class_time = (TextView) findViewById(R.id.class_time);
        class_book = (TextView) findViewById(R.id.class_book);
        class_in = (TextView) findViewById(R.id.class_in);

        stulist = (Button) findViewById(R.id.class_btn_student);

        back=(Button)findViewById(R.id.classinfo_btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        restClient.get("clsinfo", params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e("failure", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray array) {
                try {
                    classinfo = DwAndroidUtils.jsonArrayToList(array, classinfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (final Map<String, Object> map : classinfo) {
                    class_name.setText("  " + map.get("COURSE_NAME").toString());
                    class_prof.setText(map.get("PROF_NM").toString()+" 교수님  ");
                    if(croom == null) {
                        class_room.setText("  " + map.get("ROOM_NO").toString().substring(0,2) + "호관" + map.get("ROOM_NO").toString().substring(2));
                    }else class_room.setText("  " + croom.substring(0,2) + "호관" + croom.substring(2));
                    class_time.setText("  "+map.get("DAY").toString());
                    if(map.get("M_MASTER_NM").toString() == "null"){
                        class_book.setText("  책 정보가 존재하지 않습니다");
                    }else class_book.setText("  "+map.get("M_MASTER_NM").toString());
                    class_in.setText("\n"+map.get("SUBJECT_GOAL").toString());

                    stulist.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ClassinfoActivity.this, classstudentinfo.class);
                            intent.putExtra("id", id);
                            intent.putExtra("name", map.get("COURSE_NAME").toString());
                            intent.putExtra("pro", map.get("PROF_NM").toString());
                            intent.putExtra("year", y);
                            intent.putExtra("term", t);
                            startActivity(intent);
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        RestClient restClient = new RestClient(this);
        pref = getSharedPreferences("setting", 0);
        if (!pref.getString("ID", "").isEmpty()) {
            params.put("sno", pref.getString("ID", ""));
            params.put("pw", pref.getString("PW", ""));
            restClient.post("postlogin", params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {

                }
            });

        } else {
            finish();
            Intent intent = new Intent(ClassinfoActivity.this, LoginActiviy.class);
            startActivity(intent);
        }
    }
}
