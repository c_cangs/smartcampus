package com.ourincheon.smartcampus1231.timetable;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.RestClient;
import com.ourincheon.smartcampus1231.System.DwAndroidUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

/**
 * Created by kyun on 2016-03-03.
 */
public class classstudentinfo extends AppCompatActivity {
    List<Map<String, Object>> studentlist = new ArrayList<Map<String, Object>>();
    RequestParams params = new RequestParams();
    RequestParams params2 = new RequestParams();
    SharedPreferences pref;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_class_list);

        final RestClient restClient = new RestClient(this);
        final LinearLayout v = (LinearLayout) findViewById(R.id.student);
        final LayoutInflater inflater = getLayoutInflater();
        Intent intent = getIntent();
        final String id = intent.getStringExtra("id");
        final String name = intent.getStringExtra("name");
        final String pro = intent.getStringExtra("pro");
        final String year = intent.getStringExtra("year");
        final String term = intent.getStringExtra("term");
        pref = classstudentinfo.this.getSharedPreferences("photocheck", classstudentinfo.this.MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();
        TextView cname, cpro;
        Button back;

        params.put("year", year);
        params.put("term", term);
        params.put("sno", id);

        back = (Button) findViewById(R.id.classlist_btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        cname = (TextView) findViewById(R.id.class_list_name);
        cpro = (TextView) findViewById(R.id.class_list_pro);
        cname.setText("  " + name);
        cpro.setText(pro + " 교수님  ");

        restClient.get("clsmate", params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e("failure", responseString);
            }

            @Override
            public void onSuccess(int statusCode, final Header[] headers, JSONArray array) {
                try {
                    studentlist = DwAndroidUtils.jsonArrayToList(array, studentlist);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                int t = 1;
                boolean c = true;
                String department = "",name = "",stunum = "";
                String department2,name2,stunum2;
                Color color = new Color();
                for (Map<String, Object> map : studentlist) {
                    if(c){
                        department = map.get("DEPARTMENT").toString();
                        name = map.get("NAME").toString();
                        stunum = map.get("STU_ID").toString();
                        c = false;
                    }
                    else if(!c){

                        department2 = map.get("DEPARTMENT").toString();
                        name2 = map.get("NAME").toString();
                        stunum2 = map.get("STU_ID").toString();
                        final String finalStunum = stunum;
                        final String finalStunum2 = stunum2;
                        LinearLayout v2 = (LinearLayout) inflater.inflate(R.layout.student_list_row, null);
                        RelativeLayout v3 = (RelativeLayout) v2.findViewById(R.id.classrelative);
                        RelativeLayout v4 = (RelativeLayout) v2.findViewById(R.id.classrelative2);

                        if (t % 2 == 0) {
                            v3.setBackgroundColor(color.rgb(243, 247, 243));
                            v4.setBackgroundColor(color.rgb(243, 247, 243));
                        }

                        TextView stusub = (TextView) v2.findViewById(R.id.stusub);
                        TextView stuname = (TextView) v2.findViewById(R.id.stuname);
                        TextView stusub2 = (TextView) v2.findViewById(R.id.stusub2);
                        TextView stuname2 = (TextView) v2.findViewById(R.id.stuname2);


//                        v3.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                final boolean photo = pref.getBoolean(finalStunum, false);
//                                final Dialog dialog = new Dialog(classstudentinfo.this);
//                                dialog.setOwnerActivity(classstudentinfo.this);
//                                dialog.setContentView(R.layout.student_info_image);
//                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                                final ImageView imageView = (ImageView)dialog.findViewById(R.id.stuent_image);
//                                if (!photo) {
//                                    params2.put("sno", finalStunum);
//                                    restClient.get("Ustuphoto2", params2, new AsyncHttpResponseHandler() {
//                                        @Override
//                                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                                            restClient.get("photo/" + finalStunum + ".jpg", null, new FileAsyncHttpResponseHandler(classstudentinfo.this) {
//
//
//                                                @Override
//                                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
//
//                                                }
//
//                                                @Override
//                                                public void onSuccess(int statusCode, Header[] headers, File file) {
//                                                    Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                                                    imageView.setImageBitmap(myBitmap);
//                                                    imageView.setOnClickListener(new View.OnClickListener() {
//                                                        @Override
//                                                        public void onClick(View v) {
//                                                            dialog.dismiss();
//                                                        }
//                                                    });
//                                                    dialog.show();
//                                                }
//                                            });
//                                        }
//
//                                        @Override
//                                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//
//                                        }
//                                    });
//                                    editor.putBoolean(finalStunum, true);
//                                    editor.commit();
//                                }
//                                else{
//                                    restClient.get("photo/" + finalStunum + ".jpg", null, new FileAsyncHttpResponseHandler(classstudentinfo.this) {
//
//
//                                        @Override
//                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
//
//                                        }
//
//                                        @Override
//                                        public void onSuccess(int statusCode, Header[] headers, File file) {
//                                            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                                            imageView.setImageBitmap(myBitmap);
//                                            imageView.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                            dialog.show();
//                                        }
//                                    });
//                                }
//                            }
//                        });
//
//                        v4.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                final Dialog dialog = new Dialog(classstudentinfo.this);
//                                final boolean photo2 = pref.getBoolean(finalStunum2, false);
//                                dialog.setOwnerActivity(classstudentinfo.this);
//                                dialog.setContentView(R.layout.student_info_image);
//                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                                final ImageView imageView = (ImageView)dialog.findViewById(R.id.stuent_image);
//
//                                if(!photo2){
//                                    params2.put("sno", finalStunum2);
//                                    restClient.get("Ustuphoto2", params2, new AsyncHttpResponseHandler() {
//                                        @Override
//                                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                                            restClient.get("photo/" + finalStunum2 + ".jpg", null, new FileAsyncHttpResponseHandler(classstudentinfo.this) {
//
//
//                                                @Override
//                                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
//
//                                                }
//
//                                                @Override
//                                                public void onSuccess(int statusCode, Header[] headers, File file) {
//                                                    Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                                                    imageView.setImageBitmap(myBitmap);
//                                                    imageView.setOnClickListener(new View.OnClickListener() {
//                                                        @Override
//                                                        public void onClick(View v) {
//                                                            dialog.dismiss();
//                                                        }
//                                                    });
//                                                    dialog.show();
//                                                }
//                                            });
//
//                                        }
//
//                                        @Override
//                                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//
//                                        }
//                                    });
//                                    editor.putBoolean(finalStunum2, true);
//                                    editor.commit();
//                                }
//                                else {
//                                    restClient.get("photo/" + finalStunum2 + ".jpg", null, new FileAsyncHttpResponseHandler(classstudentinfo.this) {
//
//
//                                        @Override
//                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
//
//                                        }
//
//                                        @Override
//                                        public void onSuccess(int statusCode, Header[] headers, File file) {
//                                            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                                            imageView.setImageBitmap(myBitmap);
//                                            imageView.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//                                                    dialog.dismiss();
//                                                }
//                                            });
//                                            dialog.show();
//                                        }
//                                    });
//                                }
//                            }
//                        });
                        stusub.setText(department);
                        stuname.setText(name);
                        stusub2.setText(department2);
                        stuname2.setText(name2);
                        v.addView(v2);
                        t = t + 1;
                        c = true;
                    }
                }
            }
        });
    }
}
