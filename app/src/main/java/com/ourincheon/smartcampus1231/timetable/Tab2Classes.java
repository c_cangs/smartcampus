package com.ourincheon.smartcampus1231.timetable;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import cz.msebera.android.httpclient.Header;

@SuppressLint("ValidFragment")
public class Tab2Classes extends Fragment {

	RequestParams params = new RequestParams();
	static Context mContext;

    public Tab2Classes() {}


	public Tab2Classes(Context context) {
		mContext = context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_classes, null);

		final RestClient restClient = new RestClient(mContext);
		final LinearLayout vv = (LinearLayout) v.findViewById(R.id.classes_list);
		final LayoutInflater minflater = getLayoutInflater(savedInstanceState);

		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH);
		int term;
		if (month < 6) term = 1;
		else term = 2;
		params.put("year", Integer.toString(year));
		params.put("term", Integer.toString(term));


		restClient.get("clslist", params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e("failure", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray array) {
                final String id[] = new String[array.length()];
                int j = 0;
                try {
                    for (int i = 0; i < array.length(); i++) {
                        final int k = j;
                        JSONObject object = array.getJSONObject(i);
                        LinearLayout v1 = (LinearLayout) minflater.inflate(R.layout.content_list, null);
                        vv.addView(v1);
                        TextView class_name = (TextView) v1.findViewById(R.id.content_row);
                        class_name.setText(object.get("SUBJECT_NM").toString());
                        id[k] = object.get("SUBJECT_NO").toString();
                        v1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Tab2Classes.mContext, ClassinfoActivity.class);
                                intent.putExtra("id", id[k]);
                                startActivity(intent);
                            }
                        });
                        j = j + 1;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
		return v;
	}
}