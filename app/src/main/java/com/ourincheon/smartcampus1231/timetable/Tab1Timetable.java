package com.ourincheon.smartcampus1231.timetable;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.ourincheon.smartcampus1231.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;


@SuppressLint("ValidFragment")
public class Tab1Timetable extends Fragment {

	static ScrollView scroll;
	View v;
	Context mContext;

		public Tab1Timetable() {}
		
		public Tab1Timetable(Context context) {
			mContext=context;
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, 
				ViewGroup container, Bundle savedInstanceState) {

			v = inflater.inflate(R.layout.activity_timetable, null);
			final LinearLayout mon=(LinearLayout) v.findViewById(R.id.timetable_mon);
			final LinearLayout tus=(LinearLayout) v.findViewById(R.id.timetable_tus);
			final LinearLayout wed=(LinearLayout) v.findViewById(R.id.timetable_wed);
			final LinearLayout thu=(LinearLayout) v.findViewById(R.id.timetable_thu);
			final LinearLayout fri=(LinearLayout) v.findViewById(R.id.timetable_fri);
			scroll = (ScrollView) v.findViewById(R.id.timetable_scrollview);

			Timetablelayout tablelayoutmon = new Timetablelayout(mContext, "월");
			mon.addView(tablelayoutmon);
			Timetablelayout tablelayouttus = new Timetablelayout(mContext, "화");
			tus.addView(tablelayouttus);
			Timetablelayout tablelayoutwed = new Timetablelayout(mContext, "수");
			wed.addView(tablelayoutwed);
			Timetablelayout tablelayoutthu = new Timetablelayout(mContext, "목");
			thu.addView(tablelayoutthu);
			Timetablelayout tablelayoutfri = new Timetablelayout(mContext, "금");
			fri.addView(tablelayoutfri);

			return v;
		}

	public static Uri capture(){
		scroll.setDrawingCacheEnabled(true);
		Bitmap captureView = Bitmap.createBitmap(
				scroll.getChildAt(0).getWidth(),
				scroll.getChildAt(0).getHeight() - 7,
				Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(captureView);
		scroll.draw(c);
		scroll.setDrawingCacheEnabled(false);
		FileOutputStream fos;
		String adress = Environment.getExternalStorageDirectory().toString()+"/timetablecapture.jpeg";
		try {
			fos = new FileOutputStream(adress);
			captureView.compress(Bitmap.CompressFormat.JPEG, 100, fos);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Uri uri = Uri.fromFile(new File(adress));
		return uri;
	}
}