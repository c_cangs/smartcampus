package com.ourincheon.smartcampus1231.timetable;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by kyun on 2016-03-02.
 */
public class ScheduleAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public ScheduleAdapter(FragmentManager fm,Context context) {
        super(fm);
        mContext=context;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position)    {
        switch (position){
            case 0:
                return new Tab1Timetable(mContext);
            case 1:
                return new Tab2Classes(mContext);
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String a="시간표",b="수강목록";
        switch (position) {
            case 0:
                return a;
            case 1:
                return b;
        }
        return null;
    }
}
