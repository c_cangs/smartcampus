package com.ourincheon.smartcampus1231.timetable;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by kyun on 2016-03-06.
 */
    public class TimetableDBhelper extends SQLiteOpenHelper {
    private static TimetableDBhelper INSTANCE;
    private Context context;

        public static TimetableDBhelper getInstance(Context context){
            if (INSTANCE == null){
                INSTANCE = new TimetableDBhelper(context.getApplicationContext(),"",null,1);
            }
            return INSTANCE;
        }


        public TimetableDBhelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, "timetable.db", null, 1);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table time (num integer primary key,id text, name text, profes text, day text, room text, simpleroom text, start integar, end intager);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }

        public void insert(int i, String id, String name, String profes, String day, String room, String sroom, int start, int end) {
            SQLiteDatabase db = getWritableDatabase();

            db.execSQL("insert into time values ('" + i + "','" + id + "','" + name + "','" + profes + "','" + day + "','" + room + "','" + sroom + "','" + start + "','" + end + "');");
            db.close();
        }

        public void delete() {
            SQLiteDatabase db = getWritableDatabase();

            db.execSQL("delete from time;");
            db.close();
        }

        public Cursor getResult(String day) {
            SQLiteDatabase db = getReadableDatabase();

            Cursor cursor = db.rawQuery("select * from time where day = '"+ day + "' ", null);

            return cursor;


        }
    }

