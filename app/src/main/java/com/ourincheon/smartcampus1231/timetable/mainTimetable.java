package com.ourincheon.smartcampus1231.timetable;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.ourincheon.smartcampus1231.Login.LoginActiviy;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.ApplicationController;
import com.ourincheon.smartcampus1231.Resource.RestClient;

import java.util.Calendar;

import cz.msebera.android.httpclient.Header;


public class mainTimetable extends FragmentActivity {
    SharedPreferences pref;
    SharedPreferences pref2;
    Context context;
    boolean check;
    SQLiteDatabase db_time;
    TimetableDBhelper helper_time;

    RequestParams params = new RequestParams();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintimetable);
        context = this;
        final Button reset, back, capture;

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager2);
        final ScheduleAdapter adapter = new ScheduleAdapter(getSupportFragmentManager(), context);
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                final ProgressDialog dialog = ProgressDialog.show(context, "",
                        "잠시 기다려주세요", true);
                Handler handler1= new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        tabLayout.addTab(tabLayout.newTab());
                        tabLayout.addTab(tabLayout.newTab());
                        viewPager.setAdapter(adapter);
                        tabLayout.setupWithViewPager(viewPager);
                        dialog.dismiss();
                    }
                };
                handler1.sendEmptyMessageDelayed(0,500);
            }
        };

//        Calendar now = Calendar.getInstance();
//        pref = context.getSharedPreferences("timecheck", context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = pref.edit();
//        int month = now.get(Calendar.MONTH);
//        int day = now.get(Calendar.DATE);
//        check = pref.getBoolean("check", true);
//        if ((month == 2 && day == 1) || (month == 7 && day == 29)) {
//            if (check) {
//                pref2 = context.getSharedPreferences("timecolor", context.MODE_PRIVATE);
//                SharedPreferences.Editor editor2 = pref2.edit();
//                editor2.clear();
//                editor2.commit();
//                for(int i = 0;i < 15;i++) notsamecolor.check[i] = i;
//                notsamecolor.count = 0;
//                TimetableDBhelper dbHelper = new TimetableDBhelper(context, "timetable.db", null, 1);
//                dbHelper.delete();
//                Timetabledbset timetabledbset = new Timetabledbset(context);
//                timetabledbset.execute();
//                editor.putBoolean("check", false);
//                editor.commit();
//                handler.sendEmptyMessage(0);
//            }
//        } else if ((month == 2 && day == 2) || (month == 7 && day == 30)) {
//            editor.putBoolean("check", true);
//            editor.commit();
//        }

        reset = (Button) findViewById(R.id.class_get);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    DeleteNCreate_time();
                    pref2 = context.getSharedPreferences("timecolor", context.MODE_PRIVATE);
                    for (int i = 0; i < 15; i++) notsamecolor.check[i] = i;
                    notsamecolor.count = 0;
                    SharedPreferences.Editor editor2 = pref2.edit();
                    editor2.clear();
                    editor2.commit();
                    TimetableDBhelper dbHelper = new TimetableDBhelper(context, "timetable.db", null, 1);
                    dbHelper.delete();
                    Timetabledbset timetabledbset = new Timetabledbset(context);
                    timetabledbset.execute();
                    handler.sendEmptyMessage(0);
                }catch (SecurityException e){
                    Log.e("reset",e.toString());
                } catch (Exception e){
                    Log.e("reset1",e.toString());
                }
            }
        });

        capture = (Button) findViewById(R.id.class_capture);
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Tab1Timetable.capture();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_STREAM,uri);
                intent.setType("image/jpge");
                startActivity(Intent.createChooser(intent, "공유 하기"));
            }
        });

        back = (Button) findViewById(R.id.classes_btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Tracker t = ((ApplicationController) getApplication()).getTracker(ApplicationController.TrackerName.APP_TRACKER);
        t.setScreenName("TimetableActivity");
        t.send(new HitBuilders.AppViewBuilder().build());
    }


    public void DeleteNCreate_time(){

        db_time = helper_time.getInstance(this).getWritableDatabase();
        String timeD = "drop table if exists time";
        db_time.execSQL(timeD);
        String timeC = "create table time (num integer primary key,id text, name text, profes text, day text, room text, simpleroom text, start integar, end intager);";
        db_time.execSQL(timeC);
        new Timetabledbset(context).execute();


    }

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        RestClient restClient = new RestClient(this);
        pref = getSharedPreferences("setting", 0);
        if (!pref.getString("ID", "").isEmpty()) {
            params.put("sno", pref.getString("ID", ""));
            params.put("pw", pref.getString("PW", ""));
            restClient.post("postlogin", params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {

                }
            });

        } else {
            Intent intent = new Intent(context, LoginActiviy.class);
            startActivity(intent);
            finish();
        }
    }
}
