package com.ourincheon.smartcampus1231.timetable;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ourincheon.smartcampus1231.R;

import java.util.Random;


/**
 * Created by kyun on 2016-03-03.
 */
public class Timetablelayout extends LinearLayout {

    String yoil;
    boolean isnotnull = false;
    LayoutInflater inflater;
    SharedPreferences pref;
    TimetableDBhelper dbHelper;
    Cursor cursor;

    public Timetablelayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        // TODO Auto-generated constructor stub
    }

    public Timetablelayout(Context context) {
        super(context);
        init(context);
        // TODO Auto-generated constructor stub
    }

    public Timetablelayout(Context context, String yoil) {
        super(context);
        this.yoil = yoil;
        init(context);
    }

    public void init(final Context context) {

        try {

            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout v = (LinearLayout) inflater.inflate(R.layout.timetable_item, null);
            dbHelper = TimetableDBhelper.getInstance(context);
            cursor = dbHelper.getResult(yoil);

            while (cursor.moveToNext()) {
                int i = cursor.getInt(0);
                final String id = cursor.getString(1);
                if (!id.equals(null)) isnotnull = true;
                final String classname = cursor.getString(2);
                String profes = cursor.getString(3);
                String day = cursor.getString(4);
                final String room = cursor.getString(5);
                String sroom = cursor.getString(6);
                int start = cursor.getInt(7);
                int end = cursor.getInt(8);
                if (day.equals(yoil)) {
                    TextView na, pr, st, ed;
                    int time = getResources().getIdentifier(
                            "id/c" + Integer.toString(start), "id",
                            "com.appcenter.smartcampus");
                    int name = getResources().getIdentifier(
                            "id/t" + Integer.toString(start), "id",
                            "com.appcenter.smartcampus");
                    int pro = getResources().getIdentifier(
                            "id/p" + Integer.toString(start), "id",
                            "com.appcenter.smartcampus");
                    int stt = getResources().getIdentifier(
                            "id/s" + Integer.toString(start), "id",
                            "com.appcenter.smartcampus");
                    int edd = getResources().getIdentifier(
                            "id/e" + Integer.toString(end), "id",
                            "com.appcenter.smartcampus");
                    LinearLayout t = (LinearLayout) v.findViewById(time);
                    t.setBackgroundColor(makecolor(classname, context));
                    na = (TextView) v.findViewById(name);
                    pr = (TextView) v.findViewById(pro);
                    st = (TextView) v.findViewById(stt);
                    st.setText(settime(start, 0));
                    na.setText(classname + "                                       ");
                    pr.setText("  " + profes + " 교수님");// sroom 으로 바꾸기
                    t.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, ClassinfoActivity.class);
                            intent.putExtra("id", id);
                            intent.putExtra("room", room);
                            context.startActivity(intent);
                        }
                    });
                    if (start == end) {
                        ed = (TextView) v.findViewById(edd);
                        ed.setText(settime(end, 1));
                    } else if (start != end) {
                        for (int j = start + 1; j < end + 1; j++) {
                            int endt = getResources().getIdentifier(
                                    "id/c" + Integer.toString(j), "id",
                                    "com.appcenter.smartcampus");
                            LinearLayout et = (LinearLayout) v.findViewById(endt);
                            ed = (TextView) v.findViewById(edd);
                            ed.setText(settime(end, 1));
                            et.setBackgroundColor(makecolor(classname, context));
                            et.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, ClassinfoActivity.class);
                                    intent.putExtra("id", id);
                                    intent.putExtra("room", room);
                                    context.startActivity(intent);
                                }
                            });
                        }
                    }
                }
            }
            if(!isnotnull) {
                TextView t = (TextView) v.findViewById(R.id.t0);
                t.setText("                                                     ");
            }
            this.addView(v);
        }catch (IllegalStateException e){

        }


    }


    public String settime(int i,int j){
        String string = null;
        if(j==0) {
            switch (i) {
                case 0:
                    string = "오전 8:00";
                    break;
                case 1:
                    string= "오전 9:00";
                    break;
                case 2:
                    string= "오전 10:00";
                    break;
                case 3:
                    string= "오전 11:00";
                    break;
                case 4:
                    string= "오후 12:00";
                    break;
                case 5:
                    string= "오후 1:00";
                    break;
                case 6:
                    string= "오후 2:00";
                    break;
                case 7:
                    string= "오후 3:00";
                    break;
                case 8:
                    string= "오후 4:00";
                    break;
                case 9:
                    string= "오후 5:00";
                    break;
                case 10:
                    string= "오후 6:00";
                    break;
                case 11:
                    string= "오후 6:55";
                    break;
                case 12:
                    string= "오후 7:50";
                    break;
                case 13:
                    string= "오후 8:45";
                    break;
                case 14:
                    string= "오후 9:40";
                    break;
            }
        }
        else if(j==1){
            switch (i) {
                case 0:
                    string = "오전 8:50";
                    break;
                case 1:
                    string= "오전 9:50";
                    break;
                case 2:
                    string= "오전 10:50";
                    break;
                case 3:
                    string= "오전 11:50";
                    break;
                case 4:
                    string= "오후 12:50";
                    break;
                case 5:
                    string= "오후 1:50";
                    break;
                case 6:
                    string= "오후 2:50";
                    break;
                case 7:
                    string= "오후 3:50";
                    break;
                case 8:
                    string= "오후 4:50";
                    break;
                case 9:
                    string= "오후 5:50";
                    break;
                case 10:
                    string= "오후 6:50";
                    break;
                case 11:
                    string= "오후 7:45";
                    break;
                case 12:
                    string= "오후 8:40";
                    break;
                case 13:
                    string= "오후 9:35";
                    break;
                case 14:
                    string= "오후 10:30";
                    break;
            }
        }
        return string;
    }

    public int makecolor(String name,Context context) {
        int rcolor, tcolor, ncolor;
        boolean t = false, p = true;
        pref = context.getSharedPreferences("timecolor", context.MODE_PRIVATE);
        tcolor = pref.getInt(name, 0);
        if (tcolor == 0) {
            if(notsamecolor.count == 14){
                notsamecolor.count = 0;
                for(int i = 0;i < 15;i++){
                    notsamecolor.check[i] = i;
                }
            }
            if(notsamecolor.count == 0){
                notsamecolor.count = 1;
                notsamecolor.check[0] = random();
                ncolor = notsamecolor.check[0];
            }else {
                notsamecolor.count = notsamecolor.count + 1;
                notsamecolor.check[notsamecolor.count] = random();
                while (!t) {
                    for (int i = 0; i <= notsamecolor.count; i++) {
                        for (int j = i + 1; j <= notsamecolor.count; j++) {
                            if (notsamecolor.check[i] == notsamecolor.check[j]) {
                                notsamecolor.check[j] = random();
                                p = false;
                            }
                        }
                    }
                    if (p) {
                        t = true;
                    }else{
                        p = true;
                    }
                }
                ncolor = notsamecolor.check[notsamecolor.count];
            }
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(name, ncolor);
            editor.commit();
            tcolor = pref.getInt(name, 0);
        }
        rcolor = tcolor;
        return rcolor;
    }

    public int random(){
        int i = 0;
        Color color = null;
        Random random = new Random();
        switch (random.nextInt(14)) {
            case 0:
                i = color.rgb(245, 153, 142);
                break;
            case 1:
                i = color.rgb(92, 173, 130);
                break;
            case 2:
                i = color.rgb(255, 207, 87);
                break;
            case 3:
                i = color.rgb(115, 162, 201);
                break;
            case 4:
                i = color.rgb(219, 164, 221);
                break;
            case 5:
                i = color.rgb(134, 208, 193);
                break;
            case 6:
                i = color.rgb(255, 153, 102);
                break;
            case 7:
                i = color.rgb(132, 193, 118);
                break;
            case 8:
                i = color.rgb(244, 112, 110);
                break;
            case 9:
                i = color.rgb(171, 201, 113);
                break;
            case 10:
                i = color.rgb(174, 168, 211);
                break;
            case 11:
                i = color.rgb(103, 128, 159);
                break;
            case 12:
                i = color.rgb(255, 193, 102);
                break;
            case 13:
                i = color.rgb(144, 198, 149);
                break;
            case 14:
                i = color.rgb(237, 106, 88);
                break;
        }
        return i;
    }
}