package com.ourincheon.smartcampus1231.timetable;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ourincheon.smartcampus1231.Resource.RestClient;
import com.ourincheon.smartcampus1231.System.DwAndroidUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

/**
 * Created by kyun on 2016-03-08.
 */
public class Timetabledbset extends AsyncTask<String, String, String> {
    Context context;
    List<Map<String, Object>> timetable = new ArrayList<Map<String, Object>>();
    RequestParams param = new RequestParams();

    public Timetabledbset(Context context){
        this.context=context;
    }

    @Override
    protected String doInBackground(String... params) {
        return null;
    }

    @Override
    protected void onPostExecute(String s){
        super.onPostExecute(s);
        final TimetableDBhelper dbHelper = TimetableDBhelper.getInstance(context);
        final RestClient restClient = new RestClient(context);

        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int term;
        if (month < 6) term = 1;
        else term = 2;
        param.put("year", Integer.toString(year));
        param.put("term", Integer.toString(term));

        restClient.get("time", param, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e("failure", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray array) {
                try {
                    timetable = DwAndroidUtils.jsonArrayToList(array, timetable);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                int i = 0;
                dbHelper.getInstance(context).delete();
                for (Map<String, Object> map : timetable) {
                    String id = map.get("COURSE_ID").toString();
                    String name = map.get("COURSE_NAME").toString();
                    name = name.replaceAll("'","''");
                    String profes = map.get("COURSE_PROFESSOR").toString();
                    String day = map.get("COURSE_DAY").toString();
                    String room = map.get("COURSE_ROOM").toString();
                    String simpleroom = "";
                    int start = Integer.parseInt(map.get("COURSE_DATETIME_START").toString());
                    int end = Integer.parseInt(map.get("COURSE_DATETIME_END").toString());
                    dbHelper.getInstance(context).insert(i,id, name, profes, day, room, simpleroom, start, end);
                    i = i + 1;
                }
            }
        });
    }
}
