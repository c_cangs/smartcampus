package com.ourincheon.smartcampus1231;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.ourincheon.smartcampus1231.Board.Board;
import com.ourincheon.smartcampus1231.DB.MySQLHelper;
import com.ourincheon.smartcampus1231.Grade.GradeActivity;
import com.ourincheon.smartcampus1231.Grade.GradeDataStorage;
import com.ourincheon.smartcampus1231.Grade.GradeItem;
import com.ourincheon.smartcampus1231.Grade.beforeGradeItem;
import com.ourincheon.smartcampus1231.Information.InformationActivity;
import com.ourincheon.smartcampus1231.Library.LibraryActivity;
import com.ourincheon.smartcampus1231.Login.LoginActiviy;
import com.ourincheon.smartcampus1231.Resource.ApplicationController;
import com.ourincheon.smartcampus1231.Resource.BackPressCloseHandler;
import com.ourincheon.smartcampus1231.Resource.RestClient;
import com.ourincheon.smartcampus1231.haksa.HaksaActivity;
import com.ourincheon.smartcampus1231.map.MapDaumActivity;
import com.ourincheon.smartcampus1231.restaurant2.Restaurant2;
import com.ourincheon.smartcampus1231.timetable.mainTimetable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;

//import com.ourincheon.smartcampus1231.restaurant.restaurantData;

/*
 * 12/31 - viewPager test완료 / To Do - viewPager를 식당과 연동해서 식당 데이터 불러오고 이걸 viewPager에 뿌려주는 거 까지, bitBucket에서 pull해서 받아보기
 * 1/2 - viewPager 넣기 완료 / To Do - viewPager에 Datastorage만들어서 create 했을 때 데이터 넣어주고 이걸 뿌려주는 것까지(smartcampus소스, databaseProject소스 참고)
 * 1/5 - viewPager + php파싱 식당메뉴 불러오기 성공 / TO DO - 학식 말고 다른 페이지 메뉴 넣는거랑 / 에 따라서 메뉴가 여러가지 생길때를 대비하여 TextView를 띄워쓰기로 일단 존재하게 한후 있으면 보이고 없으면 안보이게
 * 1/13 - map add , 학식메뉴 다듬기 / TO DO - map - asyncTask 나만의 버전? 만들거나 그때 그때 넣기, face3Util 이거 파싱하는거 알아보기, 학식 말고 다른거 파싱, 도서관 ㅠ
 * 1/16 - TO DO - 도서관 좌석현황, 공지사항, 캠퍼스 맵, log, progress util
 * 1/31 -  issue - InformationDataStorage에서 getinstance안쓰면 왜 안되는지!, DataStorage 안먹힘ㄷㄷ list로 구현했는데, / To DO Viewpager에러 있는거 고치기
 * 3/8 - 계절학기, 학점 눌렀을때 신청, 취득 포지션에 맞게 변하는거, 갱신 - 갱신버튼 눌렀을때 setAdapter, 확정전, 졸업요건, 코드정리(*), 로그인 shared, progressbar
 * 3/10 - TODO - 갱신버튼 눌렀을때 setAdapter, 코드정리 , 자동로그인
 * 3/11
 * 3/14 - todo - image, design, restaurant
 * 3/14 - todo - permission marshmallow , restaurant, grade, ad, board, nav_info, setting, image(sqllite에 byte 저장 후 받아오기), lg
 * 3/18 - todo - 이미지 sqlite저장 - 사진 변경, 사용자 정보 앱내 저장
 * 3/19 - todo - fab Button size
 * 3/20 - todo - 게시판 내용, position내용 확인, 댓글 여러개 받아오기 , reple Effect http://stackoverflow.com/questions/30931889/adding-ripple-effect-to-recyclerview-item
 * 3/21 - todo - loginbutton - delay
 * http://romannurik.github.io/AndroidAssetStudio/icons-generic.html#source.type=image&source.space.trim=1&source.space.pad=0&size=15&padding=8&color=33b5e5%2C100&name=ic_example(floating action button)
 * UA-75598352-1
 * 4/12 first service - Todo 댓글창 크기 조절, edittext 리스트 다 보이게
 * */

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    static boolean adcheck = false;
    public GradeDataStorage gradeDataStorage = new GradeDataStorage();
    Button restaurant, library, map, information, grade, time_schedule, academic_calender, board, inu;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    SQLiteDatabase db;
    MySQLHelper helper;
    SharedPreferences setting;
    SharedPreferences.Editor editor;
    private String YearPlusTerm[] = {
            "g1n1",
            "g1n2",
            "g2n1",
            "g2n2",
            "g3n1",
            "g3n2",
            "g4n1",
            "g4n2",
            "gn3",
            "gn4"
    };
    private RestClient restClient = new RestClient(this);
    private String result = "";
    private ArrayList<ArrayList<GradeItem>> total = new ArrayList<ArrayList<GradeItem>>();
    private ArrayList<GradeItem> g1n1 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g1n2 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g2n1 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g2n2 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g3n1 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g3n2 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g4n1 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g4n2 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> gn3 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> gn4 = new ArrayList<GradeItem>();
    private ArrayList<String> indicator = new ArrayList<String>();
    private ArrayList<String> yearAVG = new ArrayList<String>();//new ArrayList<String> -> string?
    private ArrayList<beforeGradeItem> bGI = new ArrayList<beforeGradeItem>();
    private int TotalCredit = 0;
    private int acquire_Credit = 0;
    private BackPressCloseHandler backPressCloseHandler;
    private String sno = null, name = null, major = null;
    private TextView nav_name, nav_major;
    ProgressDialog dialog;

    public static Context mContext;

    Drawable redrawable;
    /*채팅, 푸쉬, 학점(교양,전공), 이러닝, 자동업데이트?*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Tracker t = ((ApplicationController) getApplication()).getTracker(ApplicationController.TrackerName.APP_TRACKER);
        t.setScreenName("MainActivity");
        t.send(new HitBuilders.AppViewBuilder().build());
/*
        Intent intent = new Intent(this, Recruit_Activity.class);
        startActivity(intent);
*/
//신입회원 모집
        /*
        int random = (int) (Math.random() * 3);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView1 = inflater.inflate(R.layout.activity_ad, null);
        android.support.v7.app.AlertDialog.Builder buider1 = new android.support.v7.app.AlertDialog.Builder(this);
        buider1.setView(dialogView1);
        final android.support.v7.app.AlertDialog dialog1 = buider1.create();
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog1.setCanceledOnTouchOutside(false);
        ImageView recImageview = (ImageView) dialogView1.findViewById(R.id.recruitImage);
        if(random == 0){
            redrawable = getResources().getDrawable(R.drawable.recruit1);
            recImageview.setImageDrawable(redrawable);
        } else if(random == 1){
            redrawable = getResources().getDrawable(R.drawable.recruit2);
            recImageview.setImageDrawable(redrawable);
        } else if(random == 2){
            redrawable = getResources().getDrawable(R.drawable.recruit3);
            recImageview.setImageDrawable(redrawable);
        }
        recImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
*/
        new getVersion().execute();
        final ViewPager viewPager = (ViewPager) findViewById(R.id.adviewpager);
        final AdAdapter adAdapter = new AdAdapter(this);
        viewPager.setAdapter(adAdapter);
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                adcheck = true;
                return false;
            }
        });
        viewPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adcheck = true;
            }
        });
        final Handler handler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (viewPager.getCurrentItem() == 0 && !adcheck) {
                    viewPager.setCurrentItem(1);
                } else if (viewPager.getCurrentItem() == 1 && !adcheck) {
                    viewPager.setCurrentItem(2);
                } else if (viewPager.getCurrentItem() == 2 && !adcheck) {
                    viewPager.setCurrentItem(3);
                }
                else if (viewPager.getCurrentItem() == 3 && !adcheck) {
                    viewPager.setCurrentItem(4);
                }
                else if (viewPager.getCurrentItem() == 4 && !adcheck) {
                    viewPager.setCurrentItem(0);
                }
            }
        };
        Thread adthread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (adcheck) {
                        try {
                            Thread.sleep(5000);
                            adcheck = false;
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    while (!adcheck) {
                        try {
                            Thread.sleep(5000);
                            handler.sendEmptyMessage(0);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        adthread.start();

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(R.drawable.drawer_nav);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.inflateHeaderView(R.layout.nav_header_nevigation);

        final ImageView imageView = (ImageView) header.findViewById(R.id.imageView_nav);
        setting = getSharedPreferences("setting", 0);
        nav_name = (TextView) header.findViewById(R.id.nav_name);
        nav_major = (TextView) header.findViewById(R.id.nav_major);
        restClient.get("stuinfo", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    JSONObject object = response.getJSONObject(0);
                    sno = object.getString("STUDENT_NO");
                    name = object.getString("NAME");
                    major = object.getString("MAJOR");
                    restClient.get("photo/" + sno + ".jpg", null, new FileAsyncHttpResponseHandler(MainActivity.this) {


                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, File file) {
                            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                            //imageView.setImageBitmap(myBitmap);
                        }
                    });
                    nav_name.setText(name);
                    nav_major.setText(major);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        //이미지
//        restClient.get("stuphoto", null, new FileAsyncHttpResponseHandler(this) {
//            @Override
//            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
//
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, File file) {
//                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                imageView.setImageBitmap(myBitmap);
//                Glide.with(MainActivity.this).load(file.getAbsolutePath()).into(imageView);
//            }
//        });
        helper = new MySQLHelper(MainActivity.this).getInstance(this);

        mContext = getApplicationContext();
        Intent intent = getIntent();


        editor = setting.edit();
        backPressCloseHandler = new BackPressCloseHandler(this);

        if (select()) {
            dialog = ProgressDialog.show(MainActivity.this, "",
                    "성적을 받아오는 중입니다. 잠시 기다려주세요", true);

            Grade_work();//db check
            beforeGrade_work();
            /*
            LayoutInflater inflater = getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.popup_info, null);
            AlertDialog.Builder buider = new AlertDialog.Builder(MainActivity.this);
            buider.setView(dialogView);
            final AlertDialog dialog = buider.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            ImageButton check = (ImageButton) dialog.findViewById(R.id.join_popup_check);
            check.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            */
            dialog.dismiss();
        } else {
            try {
                beforeGrade_work();
                total.clear();
                total.add(g1n1);
                total.add(g1n2);
                total.add(g2n1);
                total.add(g2n2);
                total.add(g3n1);
                total.add(g3n2);
                total.add(g4n1);
                total.add(g4n2);
                total.add(gn3);
                total.add(gn4);
                GradeDataStorage.setGrade(total);
                for (int i = 0; i < total.size(); i++) {
                    Array_insert(YearPlusTerm[i], total.get(i));
                }
                db = helper.getReadableDatabase();
                Cursor c1 = db.query("credit", null, null, null, null, null, null);
                while (c1.moveToNext()) {
                    GradeDataStorage.setTotalCredit(Integer.parseInt(c1.getString(c1.getColumnIndex("TotalCredit"))));
                    GradeDataStorage.setAcquire_Credit(Integer.parseInt(c1.getString(c1.getColumnIndex("acquire_Credit"))));
                }
                Cursor c = db.query("TOT_AVG", null, null, null, null, null, null);
                Cursor c4 = db.query("TOT_CREDIT", null, null, null, null, null, null);
                while (c.moveToNext()) {
                    GradeDataStorage.setTotal_avg(c.getString(c.getColumnIndex("tot_AVG")));
                    GradeDataStorage.setTotal_credit(c4.getString(c4.getColumnIndex("tot_CREDIT")));

                }
                Cursor c2 = db.query("Year_AVG", null, null, null, null, null, null);
                yearAVG.clear();
                while (c2.moveToNext()) {
                    yearAVG.add(c2.getString(c2.getColumnIndex("year_AVG")));
                }
                GradeDataStorage.setYearAVG(yearAVG);
            }catch(Exception e) {

            }
        }

        // Set up the drawer.
        restaurant = (Button) findViewById(R.id.restaurant);
        restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, RestaurantActivity.class);
                Intent intent = new Intent(MainActivity.this, Restaurant2.class);
                startActivity(intent);
            }
        });
        library = (Button) findViewById(R.id.Library);
        library.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LibraryActivity.class);
                startActivity(intent);
            }
        });
        map = (Button) findViewById(R.id.map);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MapDaumActivity.class);
                startActivity(intent);
            }
        });
        information = (Button) findViewById(R.id.information);
        information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, InformationActivity.class);
                startActivity(intent);
            }
        });

        grade = (Button) findViewById(R.id.grade);
        grade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GradeActivity.class);
                startActivity(intent);
            }
        });

        time_schedule = (Button) findViewById(R.id.time_Schedule);
        time_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, mainTimetable.class);
                startActivity(intent);
            }
        });

        academic_calender = (Button) findViewById(R.id.academic_Calender);
        academic_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HaksaActivity.class);
                startActivity(intent);
            }
        });
        board = (Button) findViewById(R.id.board);
        board.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Board.class);
                startActivity(intent);
            }
        });

        inu = (Button) findViewById(R.id.inu_button);
        inu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Inuapps.class);
                startActivity(intent);
            }
        });

        ImageButton nav_logout = (ImageButton) findViewById(R.id.nav_logout);
        nav_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.popup, null);
                AlertDialog.Builder buider = new AlertDialog.Builder(MainActivity.this);
                buider.setView(dialogView);
                final AlertDialog dialog = buider.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                Button ok = (Button) dialog.findViewById(R.id.popup_ok);
                Button cancel = (Button) dialog.findViewById(R.id.popup_cancel);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editor.remove("ID");
                        editor.remove("PW");
                        editor.commit();
                        Intent intent = new Intent(MainActivity.this, LoginActiviy.class);
                        startActivity(intent);
                        finish();
                    }
                });


                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

            }
        });
        ImageButton nav_liv = (ImageButton) findViewById(R.id.nav_liv);
        nav_liv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://lib.inu.ac.kr/"));
                startActivity(intent);
            }
        });
        ImageButton nav_inu = (ImageButton) findViewById(R.id.nav_inu);
        nav_inu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.inu.ac.kr/mbshome/mbs/inu/index.do"));
                startActivity(intent);
            }
        });
        ImageButton nav_gisuk = (ImageButton) findViewById(R.id.nav_gisuk);
        nav_gisuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.dorm.co.kr/"));
                startActivity(intent);
            }
        });
        ImageButton nav_sugang = (ImageButton) findViewById(R.id.nav_sugang);
        nav_sugang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://haksa.inu.ac.kr:7790/s_kang/sukang_login.html"));
                startActivity(intent);
            }
        });

        ImageButton nav_info = (ImageButton) findViewById(R.id.nav_info);
        nav_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ApplicationInfo.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        backPressCloseHandler.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.cart) {//not work

        } else if (id == R.id.require) {

        } else if (id == R.id.gonggu) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void insert(ArrayList<GradeItem> item, String table) {
        db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (int i = 0; i < item.size(); i++) {
            values.put("year", item.get(i).getYear());
            values.put("term", item.get(i).getTerm());
            values.put("name", item.get(i).getname());
            values.put("type", item.get(i).getType());
            values.put("credit", item.get(i).getCredit());
            values.put("rank", item.get(i).getRank());
            values.put("score", item.get(i).getScore());
            values.put("GIVE_UP_YN", item.get(i).getGIVE_UP_YN());
            db.insert(table, null, values);
        }

    }

    public void insert_credit(int acquire_Credit, int totalCredit) {
        db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("acquire_Credit", acquire_Credit);
        values.put("TotalCredit", totalCredit);
        db.insert("credit", null, values);
    }

    public void insert_TOT_AVG(String TOT_AVG, String TOT_CREDIT) {
        db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        ContentValues values_credit = new ContentValues();
        values.put("tot_AVG", TOT_AVG);
        values_credit.put("tot_CREDIT", TOT_CREDIT);
        db.insert("TOT_AVG", null, values);
        db.insert("TOT_CREDIT", null, values_credit);
    }

    public void insert_yearAVG(String yearAVG) {
        db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("year_AVG", yearAVG);
        db.insert("Year_AVG", null, values);
    }

    //select 각 테이블 마다 받아오는 것 확인 할 때 여기 있는 테이블에 내용이 있는지 확인해서 있으면 넘어가는 것으로
    public boolean select() {
        db = helper.getReadableDatabase();
        Cursor c = db.query("TOT_CREDIT", null, null, null, null, null, null);
        String year = null;
        while (c.moveToNext()) {
            year = c.getString(c.getColumnIndex("tot_CREDIT"));
            //Log.e("db", year + " " + term + " " + name + " " + type + " " + credit + " " + rank + " " + score);
        }

        if (year == null) {
            return true;
        } else {
            return false;
        }
    }

    public void Array_insert(String array, ArrayList<GradeItem> arrayList) {
        arrayList.clear();
        db = helper.getReadableDatabase();
        Cursor c = db.query(array, null, null, null, null, null, null);
        while (c.moveToNext()) {
            GradeItem gradeItem = new GradeItem(c.getString(c.getColumnIndex("year")), c.getString(c.getColumnIndex("term")), c.getString(c.getColumnIndex("name")),
                    c.getString(c.getColumnIndex("type")), c.getString(c.getColumnIndex("credit")), c.getString(c.getColumnIndex("rank")), c.getString(c.getColumnIndex("score")),
                    c.getString(c.getColumnIndex("GIVE_UP_YN")));
            arrayList.add(gradeItem);
        }
    }

    //table 여러개로
    public void delete() {
        db = helper.getWritableDatabase();
        db.delete("g1n1", null, null);
    }

    public void beforeGrade_work() {

        restClient.get("stubefs", null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                beforeGradeItem beforeGradeItem = new beforeGradeItem(object.getString("SUBJECT_NM"), object.getString("ISU_DIV"), object.getString("CREDIT"), object.getString("GET_RANK"),
                                        object.getString("BIGO"));
                                bGI.add(beforeGradeItem);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        GradeDataStorage.setBeforeGrade(bGI);
                    }
                }

        );
    }

    public void Grade_work() {
        restClient.get("stutots", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    JSONObject object = response.getJSONObject(0);
                    GradeDataStorage.setTotal_avg(object.getString("TOT_AVG"));
                    GradeDataStorage.setTotal_credit(object.getString("TOT_CREDIT"));
                    insert_TOT_AVG(object.getString("TOT_AVG"), object.getString("TOT_CREDIT"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });//years에서 학기별 학점
        restClient.get("stuyears", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject object = response.getJSONObject(i);
                        if (object.getString("TERM").equals("3") || object.getString("TERM").equals("4")) {

                        } else {
                            indicator.add(object.getString("YEAR") + object.getString("TERM"));
                            Collections.sort(indicator, new NameAscCompare());
                            insert_yearAVG(object.getString("AVR_SCORE"));
                            yearAVG.add(object.getString("AVR_SCORE"));
                        }
                    }
                    GradeDataStorage.setYearAVG(yearAVG);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        restClient.get("stualls", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject object = response.getJSONObject(i);
                        String temp = object.getString("YEAR") + object.getString("TERM");
                        GradeItem gradeItem = new GradeItem(object.getString("YEAR"), object.getString("TERM"), object.getString("SUBJECT_NAME"),
                                object.getString("TYPE"), object.getString("CREDIT"), object.getString("GET_RANK"), object.getString("SCORE"), object.getString("GIVE_UP_YN"));
                        TotalCredit += Integer.parseInt(object.getString("CREDIT"));
                        if (object.getString("GIVE_UP_YN").equals("N") && !(object.getString("SCORE").equals("0"))) {
                            acquire_Credit += Integer.parseInt(object.getString("CREDIT"));
                        }
                        try {
                            if (object.getString("TERM").equals("3")) {
                                gn3.add(gradeItem);
                            } else if (object.getString("TERM").equals("4")) {
                                gn4.add(gradeItem);
                            } else if (temp.equals(indicator.get(0).toString())) {
                                g1n1.add(gradeItem);
                            } else if (temp.equals(indicator.get(1).toString())) {
                                g1n2.add(gradeItem);
                            } else if (temp.equals(indicator.get(2).toString())) {
                                g2n1.add(gradeItem);
                            } else if (temp.equals(indicator.get(3).toString())) {
                                g2n2.add(gradeItem);
                            } else if (temp.equals(indicator.get(4).toString())) {
                                g3n1.add(gradeItem);
                            } else if (temp.equals(indicator.get(5).toString())) {
                                g3n2.add(gradeItem);
                            } else if (temp.equals(indicator.get(6).toString())) {
                                g4n1.add(gradeItem);
                            } else if (temp.equals(indicator.get(7).toString())) {
                                g4n2.add(gradeItem);
                            }
                        } catch (IndexOutOfBoundsException e) {

                        }
                    }
                    total.add(g1n1);
                    insert(g1n1, "g1n1");
                    total.add(g1n2);
                    insert(g1n2, "g1n2");
                    total.add(g2n1);
                    insert(g2n1, "g2n1");
                    total.add(g2n2);
                    insert(g2n2, "g2n2");
                    total.add(g3n1);
                    insert(g3n1, "g3n1");
                    total.add(g3n2);
                    insert(g3n2, "g3n2");
                    total.add(g4n1);
                    insert(g4n1, "g4n1");
                    total.add(g4n2);
                    insert(g4n2, "g4n2");
                    total.add(gn3);
                    insert(gn3, "gn3");
                    total.add(gn4);
                    insert(gn4, "gn4");
                    GradeDataStorage.setGrade(total);
                    insert_credit(acquire_Credit, TotalCredit);
                    GradeDataStorage.setTotalCredit(TotalCredit);
                    GradeDataStorage.setAcquire_Credit(acquire_Credit);
                } catch (JSONException e) {

                } catch (SQLiteException e) {

                }
            }
        });
    }




    public static class NameAscCompare implements Comparator<String> {

        /**
         * 오름차순(ASC)
         */
        @Override
        public int compare(String arg0, String arg1) {
            // TODO Auto-generated method stub
            return arg0.toString().compareTo(arg1.toString());
        }

    }

    private class getVersion extends AsyncTask<Void, Void, Void> {
        String store_version;
        String device_version;

        @Override
        protected Void doInBackground(Void... params) {
            store_version = MarketVersionChecker.getMarketVersion(getPackageName());
            try {
                device_version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try {
                if (store_version.compareTo(device_version) > 0) {
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(
                            MainActivity.this);
                    alt_bld.setMessage("새 버전이 나왔습니다. 업데이트 하시겠습니까?")
                            .setCancelable(false)
                            .setPositiveButton("업데이트 하러 가기",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            Intent intent = new Intent(
                                                    Intent.ACTION_VIEW,
                                                    Uri.parse("http://market.android.com/search?q=pname:com.appcenter.smartcampus"));
                                            startActivity(intent);

                                        }
                                    })
                            .setNegativeButton("나중에 하기",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {

                                        }
                                    });
                    AlertDialog alert = alt_bld.create();
                    // Title for AlertDialog
                    alert.setTitle("새 버전");
                    // Icon for AlertDialog
                    alert.show();

                } else {
                    // 업데이트 불필요

                }
            } catch (NullPointerException e) {

            }
        }
    }


    class AdAdapter extends PagerAdapter {

        private Bitmap[] adbitmap;
        private String[] aduri;
        private boolean bcheck1 = false, bcheck2 = false, bcheck3 = false,bcheck4 = false, bcheck5 = false;
        private Context mcontext;
        private LayoutInflater mInflater;
        private RestClient restClient;

        public AdAdapter(Context context) {
            super();
            mcontext = context;
            restClient = new RestClient(context);
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            adbitmap = Datastorage.getAdBitmap();
            aduri = Datastorage.getAduri();
        }

        @Override
        public int getCount() {
            return 5;
        }

        public Object instantiateItem(View pager, int position) {
            View v = null;
            switch (position) {
                case 0:
                    v = mInflater.inflate(R.layout.ad_layout, null);
                    final ImageView imageView1 = (ImageView) v.findViewById(R.id.adimage);
                    Handler b1 = new Handler() {
                        public void handleMessage(android.os.Message msg) {
                            if (adbitmap[0] == null) { //광고 없을시
                                imageView1.setBackgroundResource(R.drawable.ad);
                                imageView1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/UIAppCenter/"));
                                        mcontext.startActivity(intent);
                                    }
                                });
                            } else if (adbitmap[0] != null) {
                                imageView1.setImageBitmap(adbitmap[0]);
                                imageView1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(!aduri[0].equals("")) {
                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(aduri[0]));
                                            mcontext.startActivity(intent);
                                        }
                                    }
                                });
                            }
                        }
                    };
                    if (adbitmap[0] == null && bcheck1 == false) {
                        restClient.get("ad/ad1.jpg", null, new FileAsyncHttpResponseHandler(mcontext) {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, File file) {
                                adbitmap[0] = BitmapFactory.decodeFile(file.getAbsolutePath());
                            }
                        });
                        bcheck1 = true;
                        b1.sendEmptyMessageDelayed(0, 500);
                    } else if (bcheck1 == false) {
                        b1.sendEmptyMessage(0);
                        bcheck1 = true;
                    } else if (bcheck1 == true) {
                        b1.sendEmptyMessage(0);
                    }
                    break;
                case 1:
                    v = mInflater.inflate(R.layout.ad_layout, null);
                    final ImageView imageView2 = (ImageView) v.findViewById(R.id.adimage);
                    Handler b2 = new Handler() {
                        public void handleMessage(android.os.Message msg) {
                            if (adbitmap[1] == null) { //광고 없을시
                                imageView2.setBackgroundResource(R.drawable.ad);
                                imageView2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/UIAppCenter/"));
                                        mcontext.startActivity(intent);
                                    }
                                });
                            } else if (adbitmap[1] != null) {
                                imageView2.setImageBitmap(adbitmap[1]);
                                imageView2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(!aduri[1].equals("")) {
                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(aduri[1]));
                                            mcontext.startActivity(intent);
                                        }
                                    }
                                });
                            }
                        }
                    };
                    if (adbitmap[1] == null && bcheck2 == false) {
                        restClient.get("ad/ad2.jpg", null, new FileAsyncHttpResponseHandler(mcontext) {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, File file) {
                                adbitmap[1] = BitmapFactory.decodeFile(file.getAbsolutePath());
                            }
                        });
                        b2.sendEmptyMessageDelayed(0, 500);
                        bcheck2 = true;
                    } else if (bcheck2 == false) {
                        b2.sendEmptyMessage(0);
                        bcheck2 = true;
                    } else if (bcheck2 == true) {
                        b2.sendEmptyMessage(0);
                    }
                    break;
                case 2:
                    v = mInflater.inflate(R.layout.ad_layout, null);
                    final ImageView imageView3 = (ImageView) v.findViewById(R.id.adimage);
                    Handler b3 = new Handler() {
                        public void handleMessage(android.os.Message msg) {
                            if (adbitmap[2] == null) { //광고 없을시
                                imageView3.setBackgroundResource(R.drawable.ad);
                                imageView3.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/UIAppCenter/"));
                                        mcontext.startActivity(intent);
                                    }
                                });
                            } else if (adbitmap[2] != null) {
                                imageView3.setImageBitmap(adbitmap[2]);
                                imageView3.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(!aduri[2].equals("")) {
                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(aduri[2]));
                                            mcontext.startActivity(intent);
                                        }
                                    }
                                });
                            }
                        }
                    };
                    if (adbitmap[2] == null && bcheck3 == false) {
                        restClient.get("ad/ad3.jpg", null, new FileAsyncHttpResponseHandler(mcontext) {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, File file) {
                                adbitmap[2] = BitmapFactory.decodeFile(file.getAbsolutePath());
                            }
                        });
                        b3.sendEmptyMessageDelayed(0, 500);
                        bcheck3 = true;
                    } else if (bcheck3 == false) {
                        b3.sendEmptyMessage(0);
                        bcheck3 = true;
                    } else if (bcheck3 == true) {
                        b3.sendEmptyMessage(0);
                    }
                    break;
                case 3:
                    v = mInflater.inflate(R.layout.ad_layout, null);
                    final ImageView imageView4 = (ImageView) v.findViewById(R.id.adimage);
                    Handler b4 = new Handler() {
                        public void handleMessage(android.os.Message msg) {
                            if (adbitmap[3] == null) { //광고 없을시
                                imageView4.setBackgroundResource(R.drawable.ad);
                                imageView4.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/UIAppCenter/"));
                                        mcontext.startActivity(intent);
                                    }
                                });
                            } else if (adbitmap[3] != null) {
                                imageView4.setImageBitmap(adbitmap[3]);
                                imageView4.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(!aduri[3].equals("")) {
                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(aduri[3]));
                                            mcontext.startActivity(intent);
                                        }
                                    }
                                });
                            }
                        }
                    };
                    if (adbitmap[3] == null && bcheck4 == false) {
                        restClient.get("ad/ad4.jpg", null, new FileAsyncHttpResponseHandler(mcontext) {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, File file) {
                                adbitmap[3] = BitmapFactory.decodeFile(file.getAbsolutePath());
                            }
                        });
                        b4.sendEmptyMessageDelayed(0, 500);
                        bcheck4 = true;
                    } else if (bcheck4 == false) {
                        b4.sendEmptyMessage(0);
                        bcheck4 = true;
                    } else if (bcheck4 == true) {
                        b4.sendEmptyMessage(0);
                    }
                    break;
                case 4:
                    v = mInflater.inflate(R.layout.ad_layout, null);
                    final ImageView imageView5 = (ImageView) v.findViewById(R.id.adimage);
                    Handler b5 = new Handler() {
                        public void handleMessage(android.os.Message msg) {
                            if (adbitmap[4] == null) { //광고 없을시
                                imageView5.setBackgroundResource(R.drawable.ad);
                                imageView5.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/UIAppCenter/"));
                                        mcontext.startActivity(intent);
                                    }
                                });
                            } else if (adbitmap[4] != null) {
                                imageView5.setImageBitmap(adbitmap[4]);
                                imageView5.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(!aduri[4].equals("")) {
                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(aduri[4]));
                                            mcontext.startActivity(intent);
                                        }
                                    }
                                });
                            }
                        }
                    };
                    if (adbitmap[4] == null && bcheck5 == false) {
                        restClient.get("ad/ad3.jpg", null, new FileAsyncHttpResponseHandler(mcontext) {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, File file) {
                                adbitmap[4] = BitmapFactory.decodeFile(file.getAbsolutePath());
                            }
                        });
                        b5.sendEmptyMessageDelayed(0, 500);
                        bcheck5 = true;
                    } else if (bcheck5 == false) {
                        b5.sendEmptyMessage(0);
                        bcheck5 = true;
                    } else if (bcheck5 == true) {
                        b5.sendEmptyMessage(0);
                    }
                    break;
            }
            ((ViewPager) pager).addView(v, null);
            return v;
        }

        public void destroyItem(View pager, int position, Object view) {
            ((ViewPager) pager).removeView((View) view);
        }

        public boolean isViewFromObject(View v, Object obj) {
            return v == obj;
        }

    }
}
