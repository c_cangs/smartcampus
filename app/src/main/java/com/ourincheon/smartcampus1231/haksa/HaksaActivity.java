package com.ourincheon.smartcampus1231.haksa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.ApplicationController;
import com.ourincheon.smartcampus1231.Resource.RestClient;
import com.ourincheon.smartcampus1231.System.DwAndroidUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;


/**
 * Created by kyun on 2016-01-23.
 */
public class HaksaActivity extends Activity {

    List<Map<String, Object>> yearSchedule = new ArrayList<Map<String, Object>>();
    private Context mcontext;
    private ViewPager pager;
    private monthpageAdapter adapter;
    private TabLayout tabLayout;
    ProgressDialog dialog;
    final RestClient restClient = new RestClient(this);
    RequestParams params = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mcontext = this;
        setContentView(R.layout.layout_haksa_schedule);
        initSchedule();
        Button mapBackBtn = (Button) findViewById(R.id.btn_back);
        mapBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
        Tracker t = ((ApplicationController)getApplication()).getTracker(ApplicationController.TrackerName.APP_TRACKER);
        t.setScreenName("HaksaActivity");
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void initSchedule() {//117.16.191.242:8080/schedule.json{
        dialog = ProgressDialog.show(mcontext, "",
                "학사일정 생성중...", true);
        restClient.get("schedule.json", params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e("failure", responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray array) {
                try {
                    yearSchedule = DwAndroidUtils.jsonArrayToList(array, yearSchedule);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pager = (ViewPager) findViewById(R.id.Haksa_pager);
                adapter = new monthpageAdapter(mcontext, yearSchedule);
                pager.setAdapter(adapter);
                Calendar now = Calendar.getInstance();
                int month = now.get(Calendar.MONTH);
                pager.setCurrentItem(month);
                tabLayout = (TabLayout) findViewById(R.id.tab_Haksa);
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                tabLayout.setupWithViewPager(pager);
                dialog.dismiss();
            }
        });
    }

    /*private String getyearSchedule() { //전체 학사 일정 현재 사용 안함
        List<Map<String, Object>> monthlySchedule = new ArrayList<Map<String, Object>>();
        String schedule = "";
        for (int month = 0; month < 12; month++) {
            for (Map<String, Object> map : yearSchedule) {
                String mMonth = map.get("first_date").toString().substring(5, 7);
                String preMonth = map.get("last_date").toString().substring(5, 7);
                if (Integer.parseInt(mMonth) == month + 1
                        || Integer.parseInt(preMonth) == month + 1) {
                    monthlySchedule.add(map);
                }
            }
        }
        for (Map<String, Object> map : monthlySchedule) {
            schedule += map.get("first_date").toString();
            if (!map.get("first_date").equals(map.get("last_date")))
                schedule += " ~ " + map.get("last_date").toString();
            schedule += "   " + map.get("academic_content") + "\n";
        }
        if (!schedule.equals(""))
            schedule = schedule.substring(0, schedule.length() - 1);

        return schedule;
    }*/
    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
}
