package com.ourincheon.smartcampus1231.haksa;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.ourincheon.smartcampus1231.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;


/**
 * Created by kyun on 2016-01-23.
 */
public class monthpageAdapter extends PagerAdapter{

    private LayoutInflater mInflater;
    private Context mContext;
    private GregorianCalendar today = new GregorianCalendar();
    public List<Map<String, Object>> yearSchedule;

    public monthpageAdapter(Context context, List<Map<String, Object>> _yearSchedule) {
        super();
        mContext = context;
        mInflater = LayoutInflater.from(context);
        yearSchedule=_yearSchedule;
    }

    public int getCount() {
        return 12;
    }

    private List<Map<String, Object>> getMonthlyScheduleList(int month) {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        List<Map<String, Object>> monthlySchedule = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> map : yearSchedule) {
            String mMonth = map.get("first_date").toString().substring(5, 7);
            String preMonth = map.get("last_date").toString().substring(5, 7);
            String neyear = map.get("last_date").toString().substring(0, 4);
            String preyear = map.get("first_date").toString().substring(0, 4);
            if((!(Integer.parseInt(neyear) == year+1 & month == 0)) && (!(Integer.parseInt(preyear) == year-1 & month == 11))) {
                if ((Integer.parseInt(mMonth) == month + 1
                        || Integer.parseInt(preMonth) == month + 1)) {
                    monthlySchedule.add(map);
                }
            }
        }
        return monthlySchedule;
    }

    public Object instantiateItem(View pager, int position){
        View v = null;
        CalendarView calendar;
        switch(position){
            case 0:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 0,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(0));
                ((LinearLayout) v).addView(calendar);
                break;
            case 1:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 1,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(1));
                ((LinearLayout) v).addView(calendar);
                break;
            case 2:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 2,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(2));
                ((LinearLayout) v).addView(calendar);
                break;
            case 3:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 3,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(3));
                ((LinearLayout) v).addView(calendar);
                break;
            case 4:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 4,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(4));
                ((LinearLayout) v).addView(calendar);
                break;
            case 5:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 5,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(5));
                ((LinearLayout) v).addView(calendar);
                break;
            case 6:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 6,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(6));
                ((LinearLayout) v).addView(calendar);
                break;
            case 7:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 7,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(7));
                ((LinearLayout) v).addView(calendar);
                break;
            case 8:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 8,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(8));
                ((LinearLayout) v).addView(calendar);
                break;
            case 9:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 9,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(9));
                ((LinearLayout) v).addView(calendar);
                break;
            case 10:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 10,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(10));
                ((LinearLayout) v).addView(calendar);
                break;
            case 11:
                v = mInflater.inflate(R.layout.mpage, null);
                calendar = new CalendarView(mContext, 11,
                        today.get(GregorianCalendar.YEAR), getMonthlyScheduleList(11));
                ((LinearLayout) v).addView(calendar);
                break;
        }

        ((ViewPager)pager).addView(v, null);
        return v;
    }

    @Override
    public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "1월";
                case 1:
                    return "2월";
                case 2:
                    return "3월";
                case 3:
                    return "4월";
                case 4:
                    return "5월";
                case 5:
                    return "6월";
                case 6:
                    return "7월";
                case 7:
                    return "8월";
                case 8:
                    return "9월";
                case 9:
                    return "10월";
                case 10:
                    return "11월";
                case 11:
                    return "12월";
        }
        return null;
    }

    public void destroyItem(View pager, int position, Object view) {
        ((ViewPager)pager).removeView((View)view);
    }

    public boolean isViewFromObject(View v, Object obj) {
        return v == obj;
    }

}
