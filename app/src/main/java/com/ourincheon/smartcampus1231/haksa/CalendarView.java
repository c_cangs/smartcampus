package com.ourincheon.smartcampus1231.haksa;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ourincheon.smartcampus1231.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class CalendarView extends LinearLayout {

    private List<LinearLayout> rows = new ArrayList<LinearLayout>();
    private LayoutInflater inflater;
    private CalendarAdapter adapter;
    private Context context;
    private List<String> mItems;
    private List<Map<String, Object>> monthlyScheduleList;
    SharedPreferences pref;
    public int month;
    public int year;

    public CalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        // TODO Auto-generated constructor stub
    }

    public CalendarView(Context context) {
        super(context);
        init(context);
        // TODO Auto-generated constructor stub
    }

    public CalendarView(Context context, int month, int year,
                        List<Map<String, Object>> monthlyList) {
        super(context);
        this.month = month;
        this.year = year;
        this.monthlyScheduleList = monthlyList;
        init(context);
    }

    public void init(Context context) {
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (10 * scale + 0.5f);
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        adapter = new CalendarAdapter(month, year);
        mItems = adapter.mItems;
        this.setOrientation(LinearLayout.VERTICAL);
        pref = context.getSharedPreferences("calendarcolor", context.MODE_PRIVATE);
        drawCalendar();
        initBardot();
        this.setPadding(pixels, 0, pixels, pixels);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void drawCalendar() {

        LinearLayout top;
        top = (LinearLayout) inflater.inflate(R.layout.haksa_list_top, null);
        TextView text = (TextView) top
                .findViewById(R.id.monthly_Haksa);
        text.setText(Integer.toString(month + 1) + "월");
        this.addView(top);

        for (int i = 0; i < mItems.size(); i++) {
            int curRowPosition = (int) i / 7;
            int date[] = adapter.getDate(i);

            if (i % 7 == 0) {
                LinearLayout row;
                row = (LinearLayout) inflater.inflate(R.layout.calendar_item,
                        null);
                this.addView(row);
                rows.add(row);
            }
            int dayId = getResources().getIdentifier(
                    "id/day" + Integer.toString(i - curRowPosition * 7), "id",
                    "com.appcenter.smartcampus");
            int barsId = getResources().getIdentifier(
                    "id/bar" + Integer.toString(i - curRowPosition * 7), "id",
                    "com.appcenter.smartcampus");
            int dotsId = getResources().getIdentifier(
                    "id/dot" + Integer.toString(i - curRowPosition * 7), "id",
                    "com.appcenter.smartcampus");
            TextView day = (TextView) rows.get(curRowPosition).findViewById(
                    dayId);
            LinearLayout barLayout = (LinearLayout) rows.get(curRowPosition)
                    .findViewById(barsId);
            RelativeLayout dotLayout = (RelativeLayout) rows.get(curRowPosition)
                    .findViewById(dotsId);
            day.setTextColor(Color.rgb(102, 102, 102));
            if (date == null) {
                day.setTextColor(Color.rgb(102, 102, 102));
                if (i % 7 == 0)
                    day.setTextColor(Color.rgb(245, 149, 115));
                if ((i + 1) % 7 == 0)
                    day.setTextColor(Color.rgb(142, 141, 177));
                barLayout.setVisibility(View.GONE);
                dotLayout.setVisibility(View.GONE);
            } else {
                if (date[1] != month) {
                    if (i % 7 == 0)
                        day.setTextColor(Color.argb(175, 245, 149, 115));
                    else if ((i + 1) % 7 == 0)
                        day.setTextColor(Color.argb(175, 142, 141, 177));
                    else
                        day.setTextColor(Color.argb(175, 160, 160, 160));
                } else if (i % 7 == 0)
                    day.setTextColor(Color.rgb(245, 149, 115));
                else if ((i + 1) % 7 == 0)
                    day.setTextColor(Color.rgb(142, 141, 177));
                else
                    day.setTextColor(Color.rgb(102, 102, 102));
            }
            day.setText(mItems.get(i));
            day.setGravity(Gravity.CENTER);

        }
        LinearLayout bottom, item, cage;
        bottom = (LinearLayout) inflater.inflate(R.layout.haksa_month_schdeule, null);
        cage = (LinearLayout) bottom.findViewById(R.id.month_schedule);
        for (Map<String, Object> map : monthlyScheduleList) {
            String schedule = "";
            schedule += map.get("first_date").toString();
            if (!map.get("first_date").equals(map.get("last_date")))
                schedule += " ~ " + map.get("last_date").toString();
            schedule += "   " + map.get("academic_content") + "\n";
            schedule = schedule.substring(0, schedule.length() - 1);
            int color = pref.getInt(schedule,10);
            item = (LinearLayout) inflater.inflate(R.layout.haksa_month_schdule_item, null);
            TextView monthtext = (TextView) item
                    .findViewById(R.id.monthly_Haksa_text);
            ImageView dotimg = (ImageView)item.findViewById(R.id.dot);
            SharedPreferences.Editor editor = pref.edit();
            if(color == 10) {
                editor.putInt(schedule, CalendarColor.countstorage);
                editor.commit();
                color = pref.getInt(schedule,10);
                if (CalendarColor.countstorage == 9) {
                    CalendarColor.countstorage = 0;
                } else CalendarColor.countstorage = CalendarColor.countstorage + 1;
            }
            monthtext.setText(schedule);
            dotimg.setBackground(getResources().getDrawable(CalendarColor.dotstorage[color]));
            cage.addView(item);
        }
        this.addView(bottom);
    }

    private void initBardot() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        for (Map<String, Object> map : monthlyScheduleList) {
            int k = 0;
            boolean t = false;
            String schedule = "";
            schedule += map.get("first_date").toString();
            if (!map.get("first_date").equals(map.get("last_date")))
                schedule += " ~ " + map.get("last_date").toString();
            schedule += "   " + map.get("academic_content") + "\n";
            schedule = schedule.substring(0, schedule.length() - 1);
            try {
                Date firstDate = format.parse(map.get("first_date").toString());
                Date lastDate = format.parse(map.get("last_date").toString());
                for (int i = 0; i < mItems.size(); i++) {
                    int[] date = adapter.getDate(i);
                    if (date != null) {
                        String dateStr = Integer.toString(date[2]) + "-";
                        dateStr += Integer.toString(date[1] + 1) + "-"
                                + Integer.toString(date[0]);
                        Date postDate = format.parse(dateStr);
                        if (postDate.equals(firstDate)) {
                            drawdot(i, schedule, Integer.parseInt(map.get("holiday").toString()));
                            t = true;
                            k = i;
                        }
                        if (postDate.after(firstDate) && t) {
                            drawBar(k, schedule, Integer.parseInt(map.get("holiday").toString()));
                            t = false;
                        }
                        if (postDate.after(firstDate) && !t)
                            drawBar(i, schedule, Integer.parseInt(map.get("holiday").toString()));
                        if (!postDate.before(lastDate))
                            break;
                    }
                }
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void drawBar(int position, String content, int holiday) {
        int curRowPosition = (int) position / 7;
        int[] barIds = new int[2];
        TextView holi;
        int color = pref.getInt(content, 0);
        TextView[] bars = new TextView[2];
        holi = (TextView) rows.get(curRowPosition)
                .findViewById(getResources().getIdentifier(
                        "id/day"
                                + Integer.toString(position - curRowPosition
                                * 7), "id",
                        "com.appcenter.smartcampus"));
        for (int j = 0; j < 2; j++) {
            barIds[j] = getResources().getIdentifier(
                    "id/bar"
                            + j
                            + Integer.toString(position - curRowPosition
                            * 7), "id",
                    "com.appcenter.smartcampus");
            bars[j] = (TextView) rows.get(curRowPosition)
                    .findViewById(barIds[j]);
        }
        if(holiday==1){
            holi.setTextColor(Color.rgb(245, 149, 115));
        }
        if (bars[0].getTag() == null) {
            bars[0].setBackgroundColor(CalendarColor.colorstorage[color]);
            bars[0].setTag(content);
        } else if (bars[1].getTag() == null) {
            bars[1].setBackgroundColor(CalendarColor.colorstorage[color]);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void drawdot(int position, String content, int holiday) {
        ImageView[] dots = new ImageView[3];
        TextView holi;
        int curRowPosition = (int) position / 7;
        int color = pref.getInt(content, 0);
        holi = (TextView) rows.get(curRowPosition)
                .findViewById(getResources().getIdentifier(
                        "id/day"
                                + Integer.toString(position - curRowPosition
                                * 7), "id",
                        "com.appcenter.smartcampus"));
        dots[0] = (ImageView) rows.get(curRowPosition)
                .findViewById(getResources().getIdentifier(
                        "id/dot"
                                + "_center"
                                + Integer.toString(position - curRowPosition
                                * 7), "id",
                        "com.appcenter.smartcampus"));
        dots[1] = (ImageView) rows.get(curRowPosition)
                .findViewById(getResources().getIdentifier(
                        "id/dot"
                                + "_left"
                                + Integer.toString(position - curRowPosition
                                * 7), "id",
                        "com.appcenter.smartcampus"));
        dots[2] = (ImageView) rows.get(curRowPosition)
                .findViewById(getResources().getIdentifier(
                        "id/dot"
                                + "_right"
                                + Integer.toString(position - curRowPosition
                                * 7), "id",
                        "com.appcenter.smartcampus"));
        if(holiday==1){
            holi.setTextColor(Color.rgb(245, 149, 115));
        }
        if (dots[0].getTag() == null) {
            dots[0].setBackground(getResources().getDrawable(CalendarColor.dotstorage[color]));
            dots[0].setTag(content);
        } else if (dots[1].getTag() == null) {
            int d = color -1;
            if(color == 0) d = 9;
            dots[0].setBackground(null);
            dots[1].setBackground(getResources().getDrawable(CalendarColor.dotstorage[d]));
            dots[2].setBackground(getResources().getDrawable(CalendarColor.dotstorage[color]));
        }
    }
}
