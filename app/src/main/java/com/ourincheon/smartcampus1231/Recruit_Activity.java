package com.ourincheon.smartcampus1231;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Youngdo on 2016-09-01.
 */
public class Recruit_Activity extends AppCompatActivity {
    private ArrayList<Drawable> adImage = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad);
        int random = (int) (Math.random() * 3);
        Drawable drawable = getResources().getDrawable(R.drawable.recruit1);
        adImage.add(drawable);
        drawable = getResources().getDrawable(R.drawable.recruit2);
        adImage.add(drawable);
        drawable = getResources().getDrawable(R.drawable.recruit3);
        adImage.add(drawable);
        ImageView imageView = (ImageView) findViewById(R.id.recruitImage);
        imageView.setImageDrawable(adImage.get(random));
        Button close = (Button) findViewById(R.id.closeButton);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adImage.clear();
                finish();
            }
        });

    }
}
