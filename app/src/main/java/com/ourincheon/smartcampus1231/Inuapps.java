package com.ourincheon.smartcampus1231;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

/**
 * Created by kyun on 2016-03-23.
 */
public class Inuapps extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inuapps);

        LinearLayout facebook = (LinearLayout) findViewById(R.id.facebook);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/UIAppCenter/"));
                startActivity(intent);
            }
        });
        ImageButton restaurant = (ImageButton) findViewById(R.id.restaurant_img);
        restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.InuRestaurant&hl=ko"));
                startActivity(intent);
            }
        });
        ImageButton band = (ImageButton) findViewById(R.id.band);
        band.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=incheon.inuappcenter.inuband&hl=ko"));
                startActivity(intent);
            }
        });
        ImageButton telephone = (ImageButton) findViewById(R.id.inutelephone);
        telephone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.tistory.s1s1s1.inu_contact&hl=ko"));
                startActivity(intent);
            }
        });

        Button back = (Button) findViewById(R.id.inu_btn_toggle);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
