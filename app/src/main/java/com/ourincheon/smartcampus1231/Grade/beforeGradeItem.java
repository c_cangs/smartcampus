package com.ourincheon.smartcampus1231.Grade;

/**
 * Created by Youngdo on 2016-06-16.
 */
public class beforeGradeItem {
    private String name, credit, rank, bigo, div;

    public String getname() {
        return name;
    }

    public String getCredit() {
        return credit;
    }

    public String getRank() {
        return rank;
    }

    public String getBigo(){
        return bigo;
    }

    public String getDiv() {
        return div;
    }

    public beforeGradeItem(String name,String div, String credit, String rank, String bigo){
        this.name = name;
        this.credit = credit;
        this.rank = rank;
        this.bigo = bigo;
        this.div = div;
    }
}
