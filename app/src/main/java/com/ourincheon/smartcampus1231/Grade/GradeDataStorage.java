package com.ourincheon.smartcampus1231.Grade;

import java.util.ArrayList;

/**
 * Created by Youngdo on 2016-02-23.
 */
public class GradeDataStorage {
    private static String result, Total_avg, Total_credit;
    private static GradeDataStorage m_instance;
    private static ArrayList<ArrayList<GradeItem>> grade;
    private static int TotalCredit, acquire_Credit;
    private static ArrayList<String> yearAVG;
    private static ArrayList<beforeGradeItem> beforeGrade;
    public static void setYearAVG(ArrayList<String> yearAVG) {
        GradeDataStorage.yearAVG = yearAVG;
    }

    public static void setTotal_credit(String total_credit) {
        Total_credit = total_credit;
    }

    public static String getTotal_credit() {

        return Total_credit;
    }

    public static ArrayList<String> getYearAVG() {

        return yearAVG;
    }

    public static void setBeforeGrade(ArrayList<beforeGradeItem> beforeGrade) {
        GradeDataStorage.beforeGrade = beforeGrade;
    }

    public static ArrayList<beforeGradeItem> getBeforeGrade() {

        return beforeGrade;
    }

    public static void setGrade(ArrayList<ArrayList<GradeItem>> grade) {
        GradeDataStorage.grade = grade;
    }

    public static ArrayList<ArrayList<GradeItem>> getGrade() {

        return grade;
    }

    public static GradeDataStorage getInstance() {
        if (m_instance == null) {
            m_instance = new GradeDataStorage();
        }
        return m_instance;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {

        return result;
    }

    public static int getTotalCredit() {
        return TotalCredit;
    }

    public static int getAcquire_Credit() {
        return acquire_Credit;
    }

    public static void setTotalCredit(int totalCredit) {
        TotalCredit = totalCredit;
    }

    public static void setAcquire_Credit(int acquire_Credit) {
        GradeDataStorage.acquire_Credit = acquire_Credit;
    }

    public static String getTotal_avg() {
        return Total_avg;
    }

    public static void setTotal_avg(String total_avg) {
        Total_avg = total_avg;
    }
}
