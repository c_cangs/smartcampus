package com.ourincheon.smartcampus1231.Grade;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.ourincheon.smartcampus1231.DB.MySQLHelper;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.ApplicationController;
import com.ourincheon.smartcampus1231.Resource.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Youngdo on 2016-02-20.
 */
public class GradeActivity extends AppCompatActivity {
    FloatingActionButton fab;
    SQLiteDatabase db;
    MySQLHelper helper;
    private RestClient restClient = new RestClient(this);
    private ArrayList<ArrayList<GradeItem>> total = new ArrayList<ArrayList<GradeItem>>();
    private ArrayList<GradeItem> g1n1 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g1n2 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g2n1 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g2n2 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g3n1 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g3n2 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g4n1 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> g4n2 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> gn3 = new ArrayList<GradeItem>();
    private ArrayList<GradeItem> gn4 = new ArrayList<GradeItem>();
    private ArrayList<String> yearAVG = new ArrayList<String>();//new ArrayList<String> -> string?
    private ArrayList<String> indicator = new ArrayList<String>();
    private int TotalCredit = 0;
    private int acquire_Credit = 0;
    private boolean version = true;
    private ProgressDialog dialog;
    SharedPreferences setting;
    RequestParams params = new RequestParams();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grade_activity);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new GradeFragment(), "학점");
        adapter.addFragment(new GradeFragment(), "확정전 성적");
        adapter.addFragment(new GradeFragment(), "졸업요건");
        viewPager.setAdapter(adapter);
        helper = new MySQLHelper(this);//stu.db가 공유가 되는건지?
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(Color.GRAY, Color.BLACK);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimary));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("학점");
        Tracker t = ((ApplicationController) getApplication()).getTracker(ApplicationController.TrackerName.APP_TRACKER);
        t.setScreenName("GradeActivity");
        t.send(new HitBuilders.AppViewBuilder().build());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(getResources().getDrawable(R.drawable.menu_icon));
        getSupportActionBar().setElevation(0);
        setting = getSharedPreferences("setting", 0);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    dialog = ProgressDialog.show(GradeActivity.this, "",
                            "성적 갱신 중. 잠시 기다려주세요.", true);
                    Clear();
                    //version handler / db?
                    DeleteNCreate();//데이터베이스 테이블 삭제 - 제대로

                    restClient.get("stutots", null, new JsonHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                            super.onSuccess(statusCode, headers, response);
                            try {
                                JSONObject object = response.getJSONObject(0);
                                GradeDataStorage.setTotal_avg(object.getString("TOT_AVG"));
                                GradeDataStorage.setTotal_credit(object.getString("TOT_CREDIT"));
                                insert_TOT_AVG(object.getString("TOT_AVG"), object.getString("TOT_CREDIT"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });//years에서 학기별 학점
                    restClient.get("stuyears", null, new JsonHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                            super.onSuccess(statusCode, headers, response);
                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject object = response.getJSONObject(i);
                                    if (object.getString("TERM").equals("3") || object.getString("TERM").equals("4")) {

                                    } else {
                                        indicator.add(object.getString("YEAR") + object.getString("TERM"));
                                        Collections.sort(indicator, new NameAscCompare());
                                        insert_yearAVG(object.getString("AVR_SCORE"));
                                        yearAVG.add(object.getString("AVR_SCORE"));
                                    }
                                }
                                GradeDataStorage.setYearAVG(yearAVG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    restClient.get("stualls", null, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                            super.onSuccess(statusCode, headers, response);
                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject object = response.getJSONObject(i);
                                    String temp = object.getString("YEAR") + object.getString("TERM");
                                    GradeItem gradeItem = new GradeItem(object.getString("YEAR"), object.getString("TERM"), object.getString("SUBJECT_NAME"),
                                            object.getString("TYPE"), object.getString("CREDIT"), object.getString("GET_RANK"), object.getString("SCORE"), object.getString("GIVE_UP_YN"));
                                    TotalCredit += Integer.parseInt(object.getString("CREDIT"));
                                    if (object.getString("GIVE_UP_YN").equals("N") && !(object.getString("SCORE").equals("0"))) {
                                        acquire_Credit += Integer.parseInt(object.getString("CREDIT"));
                                    }
                                    try {
                                        if (object.getString("TERM").equals("3")) {
                                            gn3.add(gradeItem);
                                        } else if (object.getString("TERM").equals("4")) {
                                            gn4.add(gradeItem);
                                        } else if (temp.equals(indicator.get(0).toString())) {
                                            g1n1.add(gradeItem);
                                        } else if (temp.equals(indicator.get(1).toString())) {
                                            g1n2.add(gradeItem);
                                        } else if (temp.equals(indicator.get(2).toString())) {
                                            g2n1.add(gradeItem);
                                        } else if (temp.equals(indicator.get(3).toString())) {
                                            g2n2.add(gradeItem);
                                        } else if (temp.equals(indicator.get(4).toString())) {
                                            g3n1.add(gradeItem);
                                        } else if (temp.equals(indicator.get(5).toString())) {
                                            g3n2.add(gradeItem);
                                        } else if (temp.equals(indicator.get(6).toString())) {
                                            g4n1.add(gradeItem);
                                        } else if (temp.equals(indicator.get(7).toString())) {
                                            g4n2.add(gradeItem);
                                        }
                                    } catch (IndexOutOfBoundsException e) {

                                    }
                                }
                                total.add(g1n1);
                                insert(g1n1, "g1n1");
                                total.add(g1n2);
                                insert(g1n2, "g1n2");
                                total.add(g2n1);
                                insert(g2n1, "g2n1");
                                total.add(g2n2);
                                insert(g2n2, "g2n2");
                                total.add(g3n1);
                                insert(g3n1, "g3n1");
                                total.add(g3n2);
                                insert(g3n2, "g3n2");
                                total.add(g4n1);
                                insert(g4n1, "g4n1");
                                total.add(g4n2);
                                insert(g4n2, "g4n2");
                                total.add(gn3);
                                insert(gn3, "gn3");
                                total.add(gn4);
                                insert(gn4, "gn4");
                                GradeDataStorage.setGrade(total);
                                insert_credit(acquire_Credit, TotalCredit);
                                GradeDataStorage.setTotalCredit(TotalCredit);
                                GradeDataStorage.setAcquire_Credit(acquire_Credit);
                                dialog.dismiss();
                                finish();
                            } catch (JSONException e) {

                            } catch (SQLiteException e) {

                            }
                        }
                    });
                    //Toast.makeText(GradeActivity.this, "한번 갱신하면 60초 후 다시 갱신 할 수 있습니다 ", Toast.LENGTH_SHORT).show();
                    version = false;

/*
                    android.os.Handler handler = new android.os.Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            version = true;
                        }
                    };
                    handler.sendEmptyMessageDelayed(0, 60000);
                    */
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void insert(ArrayList<GradeItem> item, String table) {
        db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (int i = 0; i < item.size(); i++) {
            values.put("year", item.get(i).getYear());
            values.put("term", item.get(i).getTerm());
            values.put("name", item.get(i).getname());
            values.put("type", item.get(i).getType());
            values.put("credit", item.get(i).getCredit());
            values.put("rank", item.get(i).getRank());
            values.put("score", item.get(i).getScore());
            values.put("GIVE_UP_YN", item.get(i).getGIVE_UP_YN());
            db.insert(table, null, values);
        }

    }

    public void insert_credit(int acquire_Credit, int totalCredit) {
        db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("acquire_Credit", acquire_Credit);
        values.put("TotalCredit", totalCredit);
        db.insert("credit", null, values);
    }

    public void insert_TOT_AVG(String TOT_AVG, String TOT_CREDIT) {
        db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        ContentValues values_credit = new ContentValues();
        values.put("tot_AVG", TOT_AVG);
        values_credit.put("tot_CREDIT", TOT_CREDIT);
        db.insert("TOT_AVG", null, values);
        db.insert("TOT_CREDIT",null,values_credit);
    }

    public void insert_yearAVG(String yearAVG) {
        db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("year_AVG", yearAVG);
        db.insert("Year_AVG", null, values);
    }

    public void DeleteNCreate() {
        db = helper.getInstance(this).getWritableDatabase();
//        db.delete("g1n1",null,null);
//        db.delete("g1n2",null,null);
//        db.delete("g2n1",null,null);
//        db.delete("g2n2",null,null);
//        db.delete("g3n1",null,null);
//        db.delete("g3n2",null,null);
//        db.delete("g4n1",null,null);
//        db.delete("g4n2",null,null);
//        db.delete("gn3",null,null);
//        db.delete("gn4",null,null);
//        db.delete("credit",null,null);
//        db.delete("TOT_AVG",null,null);
//        db.delete("Year_AVG",null,null);


        String d_g1n1 = "drop table if exists g1n1";
        String d_g1n2 = "drop table if exists g1n2";
        String d_g2n1 = "drop table if exists g2n1";
        String d_g2n2 = "drop table if exists g2n2";
        String d_g3n1 = "drop table if exists g3n1";
        String d_g3n2 = "drop table if exists g3n2";
        String d_g4n1 = "drop table if exists g4n1";
        String d_g4n2 = "drop table if exists g4n2";
        String d_gn3 = "drop table if exists gn3";
        String d_gn4 = "drop table if exists gn4";
        String d_credit = "drop table if exists credit";
        String d_TOT_AVG = "drop table if exists TOT_AVG";
        String d_Year_AVG = "drop table if exists Year_AVG";
        String d_TOT_CREDIT = "drop table if exists TOT_CREDIT";
        db.execSQL(d_g1n1);
        db.execSQL(d_g1n2);
        db.execSQL(d_g2n1);
        db.execSQL(d_g2n2);
        db.execSQL(d_g3n1);
        db.execSQL(d_g3n2);
        db.execSQL(d_g4n1);
        db.execSQL(d_g4n2);
        db.execSQL(d_gn3);
        db.execSQL(d_gn4);
        db.execSQL(d_credit);
        db.execSQL(d_TOT_AVG);
        db.execSQL(d_Year_AVG);
        db.execSQL(d_TOT_CREDIT);

        String g1n1 = "CREATE TABLE g1n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g1n2 = "CREATE TABLE g1n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g2n1 = "CREATE TABLE g2n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g2n2 = "CREATE TABLE g2n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g3n1 = "CREATE TABLE g3n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g3n2 = "CREATE TABLE g3n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g4n1 = "CREATE TABLE g4n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g4n2 = "CREATE TABLE g4n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String gn3 = "CREATE TABLE gn3 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String gn4 = "CREATE TABLE gn4 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String credit = "CREATE TABLE credit ( acquire_Credit integer not null, TotalCredit integer not null);";
        String TOT_AVG = "CREATE TABLE TOT_AVG ( tot_AVG text not null);";
        String year_AVG = "CREATE TABLE Year_AVG ( year_AVG text not null);";
        String TOT_CREDIT = "CREATE TABLE TOT_CREDIT(tot_CREDIT text not null);";
        db.execSQL(g1n1);
        db.execSQL(g1n2);
        db.execSQL(g2n1);
        db.execSQL(g2n2);
        db.execSQL(g3n1);
        db.execSQL(g3n2);
        db.execSQL(g4n1);
        db.execSQL(g4n2);
        db.execSQL(gn3);
        db.execSQL(gn4);
        db.execSQL(credit);
        db.execSQL(TOT_AVG);
        db.execSQL(year_AVG);
        db.execSQL(TOT_CREDIT);
    }

    public static class NameAscCompare implements Comparator<String> {

        /**
         * 오름차순(ASC)
         */
        @Override
        public int compare(String arg0, String arg1) {
            // TODO Auto-generated method stub
            return arg0.toString().compareTo(arg1.toString());
        }

    }

    public void Clear() {
        TotalCredit = 0;
        acquire_Credit = 0;
        total.clear();
        g1n1.clear();
        g1n2.clear();
        g2n1.clear();
        g2n2.clear();
        g3n1.clear();
        g3n2.clear();
        g4n1.clear();
        g4n2.clear();
        gn3.clear();
        gn4.clear();
        yearAVG.clear();
        indicator.clear();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        setting = getSharedPreferences("setting", 0);
        params.put("sno", setting.getString("ID", ""));
        params.put("pw", setting.getString("PW", ""));
        restClient.post("postlogin", params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
            }
        });
    }
}
