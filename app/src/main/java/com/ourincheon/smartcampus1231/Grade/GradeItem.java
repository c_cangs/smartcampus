package com.ourincheon.smartcampus1231.Grade;

/**
 * Created by Youngdo on 2016-02-24.
 */
public class GradeItem {

    private String year, term, name, type, credit, rank, score, GIVE_UP_YN;

    public String getGIVE_UP_YN() {
        return GIVE_UP_YN;
    }

    public String getTerm() {
        return term;
    }

    public String getYear() {
        return year;
    }

    public String getname() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getCredit() {
        return credit;
    }

    public String getRank() {
        return rank;
    }

    public String getScore(){
        return score;
    }

    public GradeItem(String year, String term, String name, String type, String credit, String rank, String score, String GIVE_UP_YN){
        this.year = year;
        this.term = term;
        this.name = name;
        this.type = type;
        this.credit = credit;
        this.rank = rank;
        this.score = score;
        this.GIVE_UP_YN = GIVE_UP_YN;
    }
}
