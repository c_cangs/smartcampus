package com.ourincheon.smartcampus1231.Grade;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ourincheon.smartcampus1231.R;

import java.util.ArrayList;

/**
 * Created by Youngdo on 2016-02-22.
 */
public class GradeFragment extends Fragment {

    private static final String ARG_POSITION = "position";
    private static Bundle b;
    ArrayList<ArrayList<GradeItem>> result = new ArrayList<ArrayList<GradeItem>>();
    String resultItem = null;
    TextView tv, grade_request, grade_get, grade_avg, require_text, require_text1, require_text2;
    CustomList adapter;
    CustomList_list adapter_list;
    CustomList_list_beforeGrade adapter_list_before;
    SharedPreferences graduRequire;
    SharedPreferences.Editor editor;
    private int position;
    private GradeDataStorage gradeDataStorage = new GradeDataStorage();
    private ListView listView;
    private int mCuritem = 0;
    private int Tot_mCuritem = 0;
    private int totalCredit = 0, okCredit = 0;

    public static GradeFragment newInstance(int position) {
        GradeFragment f = new GradeFragment();
        b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);
        setHasOptionsMenu(true);
        adapter = new CustomList(getActivity());
        adapter_list = new CustomList_list(getActivity());
        adapter_list_before = new CustomList_list_beforeGrade(getActivity());
        graduRequire = getActivity().getSharedPreferences("graduRequire", 0);
        editor = graduRequire.edit();
        //resultItem = gradeDataStorage.getGrade().get(0).get(0).getname();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.grade_fragment_page, container, false);
        LinearLayout grade_board = (LinearLayout) linearLayout.findViewById(R.id.grade_board);
        RelativeLayout relativeLayout = (RelativeLayout) linearLayout.findViewById(R.id.Grade_Relative);
//        tv = (TextView) linearLayout.findViewById(R.id.grade_test);
        grade_avg = (TextView) grade_board.findViewById(R.id.grade_before_avg);
        grade_get = (TextView) grade_board.findViewById(R.id.grade_before_get);
        grade_request = (TextView) grade_board.findViewById(R.id.grade_before_request);
        listView = (ListView) relativeLayout.findViewById(R.id.grade_listview);
        require_text = (TextView) grade_board.findViewById(R.id.require_text);
        require_text1 = (TextView) grade_board.findViewById(R.id.require_text1);
        require_text2 = (TextView) grade_board.findViewById(R.id.require_text2);

//        grade_board.removeAllViews();
//        grade_board.addView(grade_avg);
//        grade_board.addView(grade_get);
//        grade_board.addView(grade_request);

        switch (position) {
            case 0:
                grade_avg.setText(gradeDataStorage.getTotal_avg());
                grade_request.setText(String.valueOf(gradeDataStorage.getTotalCredit()));
                grade_get.setText(String.valueOf(gradeDataStorage.getTotal_credit()));
                listView.setAdapter(adapter);
                break;
            case 1:
                listView.setAdapter(adapter_list_before);
                break;
            case 2:
                require_text.setText("나의 현재학점");
                require_text1.setText("필요 학점");
                require_text2.setText("졸업 학점");
                grade_avg.setText(graduRequire.getString("graduRequire", "140") + "학점");
                grade_request.setText(String.valueOf(gradeDataStorage.getTotal_credit()) + "학점");
                if (Integer.parseInt(graduRequire.getString("graduRequire", "140")) <= Integer.parseInt(gradeDataStorage.getTotal_credit())){
                    grade_get.setText("0");
                } else {
                    grade_get.setText(String.valueOf(Integer.parseInt(graduRequire.getString("graduRequire", "140")) - Integer.parseInt(gradeDataStorage.getTotal_credit())) + "학점");
                }
                TextView require_extra = (TextView) relativeLayout.findViewById(R.id.require_extra);
                Button Gdetail = (Button) relativeLayout.findViewById(R.id.graduDetail);
                Button Gedit = (Button) relativeLayout.findViewById(R.id.editGradu);
                Gdetail.setVisibility(View.VISIBLE);
                Gdetail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://rule.inu.ac.kr/sub/sub_2.jsp?idx=44"));
                        startActivity(intent);
                    }
                });
                Gedit.setVisibility(View.VISIBLE);
                Gedit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LayoutInflater inflater2 = getLayoutInflater(b);
                        final View dialogView = inflater2.inflate(R.layout.popup_info, null);
                        AlertDialog.Builder buider = new AlertDialog.Builder(getActivity());
                        buider.setView(dialogView);
                        final AlertDialog dialog = buider.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                        final ImageButton check = (ImageButton) dialog.findViewById(R.id.join_popup_check);
                        final EditText editText = (EditText) dialog.findViewById(R.id.editText);
                        check.setEnabled(false);
                        check.getBackground().setAlpha(51);
                        editText.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                if(!s.toString().isEmpty()){
                                    check.getBackground().setAlpha(200);
                                    check.setEnabled(true);
                                }else{
                                    check.getBackground().setAlpha(51);
                                    check.setEnabled(false);
                                }

                            }
                        });
                        check.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                editor.putString("graduRequire", editText.getText().toString());
                                editor.commit();
                                grade_avg.setText(graduRequire.getString("graduRequire", "140") + "학점");
                                grade_get.setText(String.valueOf(Integer.parseInt(graduRequire.getString("graduRequire", "140")) - gradeDataStorage.getAcquire_Credit()) + "학점");
                                dialog.dismiss();
                            }
                        });
                    }
                });
                require_extra.setMovementMethod(new ScrollingMovementMethod());
                require_extra.setText("① 졸업에 필요한 이수학점은 135학점 이상으로 하되 공과대학, 정보기술대학, 도시행정학과를 제외한 도시과학대학, 생명과학기술대학 생명공학부는 140학점 이상을 취득하여야 한다.\n" +
                        "② 교양과목은 30학점 이상 55학점까지 이수하여야 하고, 전공과목은 60학점 이상, 자연과학대학 수학과, 물리학과, 화학과, 해양학과 및 생명과학기술대학 생명과학부는 63학점 이상, 공과대학, 정보기술대학, 도시행정학과를 제외한 도시과학대학, 생명과학기술대학 생명공학부는 72학점 이상을 이수하여야 한다. 다만, 복수전공자의 전공과목은 주전공학과(전공)와 복수전공학과(전공)의 전공기초 및 전공필수과목을 포함하여 각각 42학점 이상을 이수하여야 한다.\n" +
                        "③ 제22조에 따라 수업연한을 단축하여 졸업하고자 하는 자는 총 성적평점평균이 4.0 이상이어야 한다.\n");
                if (graduRequire.getString("graduRequire", "0").equals("0")) {
                    LayoutInflater inflater1 = getLayoutInflater(b);
                    final View dialogView = inflater1.inflate(R.layout.popup_info, null);
                    AlertDialog.Builder buider = new AlertDialog.Builder(getActivity());
                    buider.setView(dialogView);
                    final AlertDialog dialog = buider.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                    final ImageButton check = (ImageButton) dialog.findViewById(R.id.join_popup_check);
                    final EditText editText = (EditText) dialog.findViewById(R.id.editText);
                    check.setEnabled(false);
                    check.getBackground().setAlpha(51);
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if(!s.toString().isEmpty()){
                                check.getBackground().setAlpha(200);
                                check.setEnabled(true);
                            }else{
                                check.getBackground().setAlpha(51);
                                check.setEnabled(false);
                            }

                        }
                    });
                    check.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editor.putString("graduRequire", editText.getText().toString());
                            editor.commit();
                            grade_avg.setText(graduRequire.getString("graduRequire", "140") + "학점");
                            grade_get.setText(String.valueOf(Integer.parseInt(graduRequire.getString("graduRequire", "140")) - gradeDataStorage.getAcquire_Credit()) + "학점");
                            dialog.dismiss();
                        }
                    });
                }
                break;
            default:
                return null;

        }
        linearLayout.removeAllViews();
        linearLayout.addView(grade_board);
        linearLayout.addView(relativeLayout);
        return linearLayout;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        switch (position) {
            case 0:
                getActivity().getMenuInflater().inflate(R.menu.main, menu);
                super.onCreateOptionsMenu(menu, inflater);
                break;
            default:
                getActivity().getMenuInflater().inflate(R.menu.global, menu);
                super.onCreateOptionsMenu(menu, inflater);
                break;
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (position == 0) {
            switch (id) {
                case R.id.total:
                    Tot_mCuritem = 0;
                    listView.setAdapter(adapter);
                    grade_avg.setText(gradeDataStorage.getTotal_avg());
                    grade_request.setText(String.valueOf(gradeDataStorage.getTotalCredit()));
                    grade_get.setText(String.valueOf(gradeDataStorage.getTotal_credit()));
                    return super.onOptionsItemSelected(item);
                case R.id.m1n1:
                    mCuritem = 0;
                    Tot_mCuritem = 1;
                    totalCredit = 0;
                    okCredit = 0;
                    listView.setAdapter(adapter_list);
                    for (int i = 0; i < gradeDataStorage.getGrade().get(mCuritem).size(); i++) {
                        totalCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        if (gradeDataStorage.getGrade().get(mCuritem).get(i).getGIVE_UP_YN().equals("N")  && !gradeDataStorage.getGrade().get(mCuritem).get(i).getRank().equals("F")) {
                            okCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        }
                    }
                    if (totalCredit == 0) {
                        grade_request.setText("0");
                        grade_get.setText("0");
                        grade_avg.setText("0");
                    } else {
                        if (gradeDataStorage.getYearAVG().get(mCuritem).length() > 5) {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem).substring(0, 4));
                        } else {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem));
                        }
                    }
                    return super.onOptionsItemSelected(item);
                case R.id.m1n2:
                    mCuritem = 1;
                    Tot_mCuritem = 1;
                    totalCredit = 0;
                    okCredit = 0;
                    listView.setAdapter(adapter_list);
                    for (int i = 0; i < gradeDataStorage.getGrade().get(mCuritem).size(); i++) {
                        totalCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        if (gradeDataStorage.getGrade().get(mCuritem).get(i).getGIVE_UP_YN().equals("N") && !gradeDataStorage.getGrade().get(mCuritem).get(i).getRank().equals("F")) {
                            okCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        }
                    }
                    if (totalCredit == 0) {
                        grade_request.setText("0");
                        grade_get.setText("0");
                        grade_avg.setText("0");
                    } else {
                        if (gradeDataStorage.getYearAVG().get(mCuritem).length() > 5) {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem).substring(0, 4));
                        } else {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem));
                        }
                    }
                    return super.onOptionsItemSelected(item);
                case R.id.m2n1:
                    mCuritem = 2;
                    Tot_mCuritem = 1;
                    totalCredit = 0;
                    okCredit = 0;
                    listView.setAdapter(adapter_list);
                    for (int i = 0; i < gradeDataStorage.getGrade().get(mCuritem).size(); i++) {
                        totalCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        if (gradeDataStorage.getGrade().get(mCuritem).get(i).getGIVE_UP_YN().equals("N")  && !gradeDataStorage.getGrade().get(mCuritem).get(i).getRank().equals("F")) {
                            okCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        }
                    }
                    if (totalCredit == 0) {
                        grade_request.setText("0");
                        grade_get.setText("0");
                        grade_avg.setText("0");
                    } else {
                        if (gradeDataStorage.getYearAVG().get(mCuritem).length() > 5) {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem).substring(0, 4));
                        } else {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem));
                        }
                    }
                    return super.onOptionsItemSelected(item);
                case R.id.m2n2:
                    mCuritem = 3;
                    Tot_mCuritem = 1;
                    totalCredit = 0;
                    okCredit = 0;
                    listView.setAdapter(adapter_list);
                    for (int i = 0; i < gradeDataStorage.getGrade().get(mCuritem).size(); i++) {
                        totalCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        if (gradeDataStorage.getGrade().get(mCuritem).get(i).getGIVE_UP_YN().equals("N")  && !gradeDataStorage.getGrade().get(mCuritem).get(i).getRank().equals("F")) {
                            okCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        }
                    }
                    if (totalCredit == 0) {
                        grade_request.setText("0");
                        grade_get.setText("0");
                        grade_avg.setText("0");
                    } else {
                        if (gradeDataStorage.getYearAVG().get(mCuritem).length() > 5) {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem).substring(0, 4));
                        } else {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem));
                        }
                    }
                    return super.onOptionsItemSelected(item);
                case R.id.m3n1:
                    mCuritem = 4;
                    Tot_mCuritem = 1;
                    totalCredit = 0;
                    okCredit = 0;
                    listView.setAdapter(adapter_list);
                    for (int i = 0; i < gradeDataStorage.getGrade().get(mCuritem).size(); i++) {
                        totalCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        if (gradeDataStorage.getGrade().get(mCuritem).get(i).getGIVE_UP_YN().equals("N")  && !gradeDataStorage.getGrade().get(mCuritem).get(i).getRank().equals("F")) {
                            okCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        }
                    }
                    if (totalCredit == 0) {
                        grade_request.setText("0");
                        grade_get.setText("0");
                        grade_avg.setText("0");
                    } else {
                        if (gradeDataStorage.getYearAVG().get(mCuritem).length() > 5) {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem).substring(0, 4));
                        } else {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem));
                        }
                    }
                    return super.onOptionsItemSelected(item);
                case R.id.m3n2:
                    mCuritem = 5;
                    Tot_mCuritem = 1;
                    totalCredit = 0;
                    okCredit = 0;
                    listView.setAdapter(adapter_list);
                    for (int i = 0; i < gradeDataStorage.getGrade().get(mCuritem).size(); i++) {
                        totalCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        if (gradeDataStorage.getGrade().get(mCuritem).get(i).getGIVE_UP_YN().equals("N")  && !gradeDataStorage.getGrade().get(mCuritem).get(i).getRank().equals("F")) {
                            okCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        }
                    }
                    if (totalCredit == 0) {
                        grade_request.setText("0");
                        grade_get.setText("0");
                        grade_avg.setText("0");
                    } else {
                        if (gradeDataStorage.getYearAVG().get(mCuritem).length() > 5) {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem).substring(0, 4));
                        } else {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem));
                        }
                    }
                    return super.onOptionsItemSelected(item);
                case R.id.m4n1:
                    mCuritem = 6;
                    Tot_mCuritem = 1;
                    totalCredit = 0;
                    okCredit = 0;
                    listView.setAdapter(adapter_list);
                    for (int i = 0; i < gradeDataStorage.getGrade().get(mCuritem).size(); i++) {
                        totalCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        if (gradeDataStorage.getGrade().get(mCuritem).get(i).getGIVE_UP_YN().equals("N")  && !gradeDataStorage.getGrade().get(mCuritem).get(i).getRank().equals("F")) {
                            okCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        }
                    }
                    if (totalCredit == 0) {
                        grade_request.setText("0");
                        grade_get.setText("0");
                        grade_avg.setText("0");
                    } else {
                        if (gradeDataStorage.getYearAVG().get(mCuritem).length() > 5) {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem).substring(0, 4));
                        } else {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem));
                        }
                    }
                    return super.onOptionsItemSelected(item);
                case R.id.m4n2:
                    mCuritem = 7;
                    Tot_mCuritem = 1;
                    totalCredit = 0;
                    okCredit = 0;
                    listView.setAdapter(adapter_list);
                    for (int i = 0; i < gradeDataStorage.getGrade().get(mCuritem).size(); i++) {
                        totalCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        if (gradeDataStorage.getGrade().get(mCuritem).get(i).getGIVE_UP_YN().equals("N")  && !gradeDataStorage.getGrade().get(mCuritem).get(i).getRank().equals("F")) {
                            okCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        }
                    }
                    if (totalCredit == 0) {
                        grade_request.setText("0");
                        grade_get.setText("0");
                        grade_avg.setText("0");
                    } else {
                        if (gradeDataStorage.getYearAVG().get(mCuritem).length() > 5) {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem).substring(0, 4));
                        } else {
                            grade_request.setText(String.valueOf(totalCredit));
                            grade_get.setText(String.valueOf(okCredit));
                            grade_avg.setText(gradeDataStorage.getYearAVG().get(mCuritem));
                        }
                    }
                    return super.onOptionsItemSelected(item);
                case R.id.mn3:
                    mCuritem = 8;
                    Tot_mCuritem = 1;
                    totalCredit = 0;
                    okCredit = 0;
                    listView.setAdapter(adapter_list);
                    for (int i = 0; i < gradeDataStorage.getGrade().get(mCuritem).size(); i++) {
                        totalCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        if (gradeDataStorage.getGrade().get(mCuritem).get(i).getGIVE_UP_YN().equals("N")  && !gradeDataStorage.getGrade().get(mCuritem).get(i).getRank().equals("F")) {
                            okCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        }
                    }
                    grade_request.setText(String.valueOf(totalCredit));
                    grade_get.setText(String.valueOf(okCredit));
                    grade_avg.setText(" ");
                    return super.onOptionsItemSelected(item);
                case R.id.mn4:
                    mCuritem = 9;
                    Tot_mCuritem = 1;
                    totalCredit = 0;
                    okCredit = 0;
                    listView.setAdapter(adapter_list);
                    for (int i = 0; i < gradeDataStorage.getGrade().get(mCuritem).size(); i++) {
                        totalCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        if (gradeDataStorage.getGrade().get(mCuritem).get(i).getGIVE_UP_YN().equals("N")  && !gradeDataStorage.getGrade().get(mCuritem).get(i).getRank().equals("F")) {
                            okCredit += Integer.parseInt(gradeDataStorage.getGrade().get(mCuritem).get(i).getCredit());
                        }
                    }
                    grade_request.setText(String.valueOf(totalCredit));
                    grade_get.setText(String.valueOf(okCredit));
                    grade_avg.setText(" ");
                    return super.onOptionsItemSelected(item);
                default:
                    return super.onOptionsItemSelected(item);
            }
        } else {
            return super.onOptionsItemSelected(item);
        }
//        if(id == R.id.m1n1){
//            return true;
//        }
        //noinspection SimplifiableIfStatement
//        return super.onOptionsItemSelected(item);
    }

    public class CustomList extends ArrayAdapter<String> {
        private final Activity context;

        public CustomList(Activity context) {
            super(context, R.layout.grade_item);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.grade_item, null, true);
            TextView potal_list_adapter_total_list_complete_name = (TextView) rowView.findViewById(R.id.potal_list_adapter_total_list_complete_name);
            TextView potal_list_adapter_total_list_type = (TextView) rowView.findViewById(R.id.potal_list_adapter_total_list_type);
            TextView potal_list_adapter_total_list_complete_credit = (TextView) rowView.findViewById(R.id.potal_list_adapter_total_list_complete_credit);
            TextView potal_list_adapter_total_list_avr_score = (TextView) rowView.findViewById(R.id.potal_list_adapter_total_list_avr_score);
            potal_list_adapter_total_list_complete_name.setText("전체학점");
            potal_list_adapter_total_list_type.setText("취득학점");
//            String.valueOf(gradeDataStorage.getTotalCredit())
            potal_list_adapter_total_list_complete_credit.setText(String.valueOf(gradeDataStorage.getTotal_credit()));
            potal_list_adapter_total_list_avr_score.setText(gradeDataStorage.getTotal_avg());
            return rowView;
        }

        @Override
        public String getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public int getCount() {
            return 1;
        }
    }

    public class CustomList_list extends ArrayAdapter<GradeItem> {
        private final Activity context;

        public CustomList_list(Activity context) {
            super(context, R.layout.grade_list_item);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.grade_list_item, null, true);
            TextView portal_list_adapter_subject_name = (TextView) rowView.findViewById(R.id.portal_list_adapter_subject_name);
            TextView portal_list_adapter_type = (TextView) rowView.findViewById(R.id.portal_list_adapter_type);
            TextView portal_list_adapter_credit = (TextView) rowView.findViewById(R.id.portal_list_adapter_credit);
            TextView portal_list_adapter_get_rank = (TextView) rowView.findViewById(R.id.portal_list_adapter_get_rank);
            TextView portal_list_adapter_score = (TextView) rowView.findViewById(R.id.portal_list_adapter_score);
            portal_list_adapter_subject_name.setText(gradeDataStorage.getGrade().get(mCuritem).get(position).getname());
            portal_list_adapter_type.setText(gradeDataStorage.getGrade().get(mCuritem).get(position).getType());
            portal_list_adapter_credit.setText(gradeDataStorage.getGrade().get(mCuritem).get(position).getCredit() + "학점");
            portal_list_adapter_get_rank.setText(gradeDataStorage.getGrade().get(mCuritem).get(position).getRank());
            portal_list_adapter_score.setText(gradeDataStorage.getGrade().get(mCuritem).get(position).getScore());
            return rowView;
        }

        @Override
        public GradeItem getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public int getCount() {
            return gradeDataStorage.getGrade().get(mCuritem).size();

        }
    }
    public class CustomList_list_beforeGrade extends ArrayAdapter<GradeItem> {
        private final Activity context;

        public CustomList_list_beforeGrade(Activity context) {
            super(context, R.layout.grade_list_item);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.grade_list_item, null, true);
            TextView portal_list_adapter_subject_name = (TextView) rowView.findViewById(R.id.portal_list_adapter_subject_name);
            TextView portal_list_adapter_type = (TextView) rowView.findViewById(R.id.portal_list_adapter_type);
            TextView portal_list_adapter_credit = (TextView) rowView.findViewById(R.id.portal_list_adapter_credit);
            TextView portal_list_adapter_get_rank = (TextView) rowView.findViewById(R.id.portal_list_adapter_get_rank);
            TextView portal_list_adapter_score = (TextView) rowView.findViewById(R.id.portal_list_adapter_score);
            portal_list_adapter_score.setTextSize(15);
            portal_list_adapter_subject_name.setText(gradeDataStorage.getBeforeGrade().get(position).getname());
            portal_list_adapter_type.setText(gradeDataStorage.getBeforeGrade().get(position).getDiv());
            portal_list_adapter_credit.setText(gradeDataStorage.getBeforeGrade().get(position).getCredit() + "학점");
            portal_list_adapter_get_rank.setText(gradeDataStorage.getBeforeGrade().get(position).getRank());
            portal_list_adapter_score.setText(gradeDataStorage.getBeforeGrade().get(position).getBigo());
            return rowView;
        }

        @Override
        public GradeItem getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public int getCount() {
            return gradeDataStorage.getBeforeGrade().size();

        }
    }
}
