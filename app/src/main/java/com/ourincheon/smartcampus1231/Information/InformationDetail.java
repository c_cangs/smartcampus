package com.ourincheon.smartcampus1231.Information;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;

import com.ourincheon.smartcampus1231.R;

/**
 * Created by Youngdo on 2016-01-29.
 */
public class InformationDetail extends Activity {
    InformationDataStorage dataStorage = new InformationDataStorage();
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information_detail);
        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.setWebChromeClient(new WebChromeClient());
        Intent intent = getIntent();
        String link = intent.getStringExtra("link");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        }
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.loadUrl(link);

//        Log.e("test", dataStorage.getlink().toString());
        Button back = (Button) findViewById(R.id.information_btn_toggle);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void setInformationDetail(InformationDataStorage model) {
        this.dataStorage = model;
    }
}
