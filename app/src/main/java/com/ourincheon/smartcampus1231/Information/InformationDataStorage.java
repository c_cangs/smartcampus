package com.ourincheon.smartcampus1231.Information;

/**
 * Created by Youngdo on 2016-01-28.
 */
public class InformationDataStorage {
    private static InformationDataStorage m_instance;
    private String strUrl;
    String title, name, time, link;

    public static InformationDataStorage getInstance() {
        if (m_instance == null) {
            m_instance = new InformationDataStorage();
        }
        return m_instance;
    }


    public void Setstrurl(String strUrl) {
        this.strUrl = strUrl;
    }

    public String Getstrurl(){
        return strUrl;
    }

    public void settitle(String title) {
        this.title = title;
    }

    public void setname(String name) {
        this.name = name;
    }

    public void settime(String time) {
        this.time = time;
    }

    public void setlink(String link) {
        this.link = link;
    }
    public String gettitle() {
        return title;
    }
    public String getname() {
        return name;
    }
    public String gettime() {
        return time;
    }
    public String getlink() {
        return link;
    }
}
