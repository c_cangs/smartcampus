package com.ourincheon.smartcampus1231.Information;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.ApplicationController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Youngdo on 2016-01-26.
 */
public class InformationActivity extends AppCompatActivity {
    private TabLayout tabs;
    private ViewPager pager;
    private myViewPagerAdapter adapter;
    private InformationDataStorage storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information);
        storage.getInstance().Setstrurl("http://www.incheon.ac.kr/rssList.jsp?siteId=inu&boardId=49219");
        tabs = (TabLayout) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.pager);
        adapter = new myViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ItemFragment(), "일반공지");
        adapter.addFragment(new ItemFragment(), "모집/채용");
        adapter.addFragment(new ItemFragment(), "학사공지");
        adapter.addFragment(new ItemFragment(), "교육/시험일정");
        pager.setAdapter(adapter);
//        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
//                .getDisplayMetrics());
//        pager.setPageMargin(pageMargin);
        //pager.setOffscreenPageLimit(2);
        tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimary));
        tabs.setupWithViewPager(pager);
        Button back = (Button) findViewById(R.id.information_btn_toggle);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Tracker t = ((ApplicationController) getApplication()).getTracker(ApplicationController.TrackerName.APP_TRACKER);
        t.setScreenName("InformationActivity");
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    public class myViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public myViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return ItemFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return mFragmentTitles.get(position);
        }


    }

}
