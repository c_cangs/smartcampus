package com.ourincheon.smartcampus1231.Information;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ourincheon.smartcampus1231.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Youngdo on 2016-01-26.
 */
public class ItemFragment extends Fragment {
    private static final String ARG_POSITION = "position";
    RecyclerView content;
    LinearLayout linearLayout;
    InformationDataStorage storage;
    ItemFragment.InformationGetData getData = null;
    //InformationDetail informationDetail = new InformationDetail();
    ProgressDialog dialog;
    private int position;
    private List<InformationDataStorage> list = new ArrayList<InformationDataStorage>();
    public String infoLink;

    public static ItemFragment newInstance(int position) {
        ItemFragment f = new ItemFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);
        list.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        switch (position) {
            case 0:
                storage.getInstance().Setstrurl("http://www.incheon.ac.kr/rssList.jsp?siteId=inu&boardId=49219");
                infoLink = "http://www.incheon.ac.kr/rssList.jsp?siteId=inu&boardId=49219";
                getData = new ItemFragment.InformationGetData(infoLink);
                getData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                break;
            case 1:
                storage.getInstance().Setstrurl("http://www.incheon.ac.kr/rssList.jsp?siteId=inu&boardId=49235");
                infoLink = "http://www.incheon.ac.kr/rssList.jsp?siteId=inu&boardId=49235";
                getData = new ItemFragment.InformationGetData(infoLink);
                getData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                break;
            case 2:
                storage.getInstance().Setstrurl("http://www.incheon.ac.kr/rssList.jsp?siteId=inu&boardId=49211");
                infoLink = "http://www.incheon.ac.kr/rssList.jsp?siteId=inu&boardId=49211";
                getData = new ItemFragment.InformationGetData(infoLink);
                getData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            case 3:
                storage.getInstance().Setstrurl("http://www.incheon.ac.kr/rssList.jsp?siteId=inu&boardId=307783");
                infoLink = "http://www.incheon.ac.kr/rssList.jsp?siteId=inu&boardId=307783";
                getData = new ItemFragment.InformationGetData(infoLink);
                getData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            default:
                break;
        }

        linearLayout = (LinearLayout) inflater.inflate(R.layout.information_fragment, container, false);
        content = (RecyclerView) linearLayout.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        content.setHasFixedSize(true);
        content.setLayoutManager(layoutManager);
        content.setAdapter(new RecyclerAdapter(getActivity(), list, R.layout.information_fragment));
        content.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), content, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), InformationDetail.class);
                intent.putExtra("link", list.get(position).getlink());
                startActivity(intent);

            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
        linearLayout.removeAllViews();
        linearLayout.addView(content);
        return linearLayout;
    }

    public class InformationGetData extends AsyncTask<Void, Void, Void> {
        URL url;
        String tagname = "", title = "", name = "", time = "", link = "";
        Boolean flag = null;//데이터의 내용을 모두 읽어드렸는지에 대한 정보를 저장
        InformationDataStorage storage;
        boolean isItemTag = false;
        String infoLink;

        public InformationGetData(String infoLink) {
            this.infoLink = infoLink;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);//네임스페이스 사용여부
                XmlPullParser xpp = factory.newPullParser();
                url = new URL(infoLink);
                InputStream in = url.openStream();
                xpp.setInput(in, "utf-8");//웹사이트로부터 받아온 xml문서를 읽어드리면서 데이터를 추출해주는 XmlPullParser객체로 넘겨준다
                int eventType = xpp.getEventType();//이벤트의 내용을 사용하기 위해 변수선언
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_TAG) {
                        tagname = xpp.getName();
                        if (tagname.equals("item"))
                            isItemTag = true;
                    } else if (eventType == XmlPullParser.TEXT) {
                        if (isItemTag && tagname.equals("title")) {
                            if (!(title.length() > 2))
                                title += xpp.getText();
                        }
                        if (isItemTag && tagname.equals("author")) {
                            if (!(name.length() > 2))
                                name += xpp.getText();
                        }
                        if (isItemTag && tagname.equals("link")) {
                            link += xpp.getText();
                        }
                        if (isItemTag && tagname.equals("pubDate")) {
                            if (!(time.length() > 2))
                                time += xpp.getText();
                        }
                    } else if (eventType == XmlPullParser.END_TAG) {
                        tagname = xpp.getName();
                        if (tagname.equals("item")) {
                            InformationDataStorage setModel = new InformationDataStorage();
                            setModel.settitle(title);
                            setModel.setname(name);
                            setModel.settime(time.substring(0, 16));
                            String strBefore = "http://www.incheon.ac.kr/cop/mypage/myBoardView.do?";
                            String strAfter = "http://www.incheon.ac.kr/user/boardList.do?command=view&boardId=49219&";
                            String strResult = link.toString().replace(
                                    strBefore, strAfter);//"http://www.incheon.ac.kr/cop/mypage/myBoardView.do?";를 찾아서
                            // http://www.incheon.ac.kr/user/boardList.do?command=view&boardId=49219&"; 이걸로 치환후 Seq를 더 붙인다
                            setModel.setlink(strResult);//getinstance이거 안쓰면 왜 안되는지!!!!!!
                            list.add(setModel);
                            title = "";
                            name = "";
                            time = "";
                            link = "";
                            isItemTag = false;
                        }
                    }
                    eventType = xpp.next();
                }
                flag = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try {
                storage = list.get(position);
                //informationDetail.setInformationDetail(storage);
                content.setAdapter(new RecyclerAdapter(getActivity(), list, R.layout.information_fragment));// No adapter attached; skipping layout 이거! issue!!
                //프레그먼트 생명주기
            } catch (IndexOutOfBoundsException e) {

            }

        }
    }
}

/**/