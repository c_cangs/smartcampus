package com.ourincheon.smartcampus1231.Board;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ourincheon.smartcampus1231.Information.RecyclerItemClickListener;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.ApplicationController;
import com.ourincheon.smartcampus1231.Resource.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by DBLAB on 2016-03-19.
 */
public class BoardFragment extends android.support.v4.app.Fragment {
    private static final String ARG_POSITION = "position";
    private static final int REQUEST_LOGIN = 0;
    private static final int TYPING_TIMER_LENGTH = 600;
    RequestParams params = new RequestParams();
    private int position;
    private RecyclerView recyclerView;
    private List<Board_item> items;
    private Board_Recycler_Adapter adapter;
    private Board_item[] board_items;
    private RestClient restClient;
    private ArrayList<String> id = new ArrayList<String>();
    private RecyclerView mMessagesView;
    private EditText mInputMessageView;
    private List<Message> mMessages = new ArrayList<Message>();
    private RecyclerView.Adapter mAdapter;
    private boolean mTyping = false;
    private Handler mTypingHandler = new Handler();
    private String mUsername;
    private Socket mSocket;
    private Button room1, room2, room3;
    private SharedPreferences setting;
    private SharedPreferences.Editor editor;
    private Boolean isConnected = true;
    private static Bundle b;
    String nickname;
    TextView userNum;
//닉네임 쉐어드, 변경버튼, 이벤트버스(플로팅없애기), 오류점검
    public static BoardFragment newInstance(int position) {
        BoardFragment f = new BoardFragment();
        b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mAdapter = new MessageAdapter(activity, mMessages);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);
        restClient = new RestClient(getActivity());
        setHasOptionsMenu(true);
        setting = getActivity().getSharedPreferences("setting", 0);
        editor = setting.edit();
        ApplicationController app = (ApplicationController) getActivity().getApplication();
        mSocket = app.getSocket();
        mSocket.on("login", onLogin);
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("new message", onNewMessage);
        mSocket.on("user joined", onUserJoined);
        mSocket.on("user left", onUserLeft);
        mSocket.on("current user", CurrentUser);
       /* mSocket.on("typing", onTyping);
        mSocket.on("stop typing", onStopTyping);*/
        mSocket.connect();





    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        switch (position) {

            case 0:

                LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.board_fragment_page, container, false);

                final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) linearLayout.findViewById(R.id.swipeLayout);
                recyclerView = (RecyclerView) swipeRefreshLayout.findViewById(R.id.board_recyclerview);
                items = new ArrayList<Board_item>();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(layoutManager);
                restClient.get("blist", null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Board_item board_item = new Board_item(" ", object.getString("title"), " ", object.getString("student_no"), object.getString("date").substring(0, 10), " ", 1);
                                items.add(board_item);
                                id.add(object.getString("id"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter = new Board_Recycler_Adapter(getActivity(), items, R.layout.board_fragment_page);
                        recyclerView.setAdapter(adapter);
                    }
                });
                swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        items.clear();
                        id.clear();
                        restClient.get("blist", null, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                for (int i = 0; i < response.length(); i++) {
                                    try {
                                        JSONObject object = response.getJSONObject(i);
                                        Board_item board_item = new Board_item(" ", object.getString("title"), " ", object.getString("student_no"), object.getString("date").substring(0, 10), " ", 1);
                                        items.add(board_item);
                                        id.add(object.getString("id"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                adapter = new Board_Recycler_Adapter(getActivity(), items, R.layout.board_fragment_page);
                                recyclerView.setAdapter(adapter);
                            }
                        });
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
                recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        params.put("id", id.get(position));
                        restClient.get("bread", params, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                super.onSuccess(statusCode, headers, response);
                                try {
                                    JSONObject object = response.getJSONObject(0);
                                    Intent intent = new Intent(getActivity(), Board_Detail.class);
                                    intent.putExtra("content", object.getString("content"));
                                    intent.putExtra("id", id.get(position));
                                    intent.putExtra("title", items.get(position).getTitle());
                                    intent.putExtra("name", items.get(position).getName());
                                    intent.putExtra("date", items.get(position).getDate());
                                    startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }

                    @Override
                    public void onItemLongClick(View view, int position) {

                    }
                }));
                linearLayout.removeAllViews();
                linearLayout.addView(swipeRefreshLayout);
                return linearLayout;
            case 1:
                RelativeLayout linearLayout1 = (RelativeLayout) inflater.inflate(R.layout.activity_ready, container, false);
                /*

                mMessagesView = (RecyclerView) linearLayout1.findViewById(R.id.messages);
                mMessagesView.setLayoutManager(new LinearLayoutManager(getActivity()));
                mMessagesView.setAdapter(mAdapter);
                userNum = (TextView) linearLayout1.findViewById(R.id.user);
                mInputMessageView = (EditText) linearLayout1.findViewById(R.id.message_input);
                mInputMessageView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                        if (id == R.id.send || id == EditorInfo.IME_NULL) {
                            attemptSend();
                            return true;
                        }
                        return false;
                    }
                });
                mInputMessageView.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (null == mUsername) return;
                        if (!mSocket.connected()) return;

                        if (!mTyping) {
                            mTyping = true;
                            mSocket.emit("typing");
                        }

                        mTypingHandler.removeCallbacks(onTypingTimeout);
                        mTypingHandler.postDelayed(onTypingTimeout, TYPING_TIMER_LENGTH);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });

                ImageButton sendButton = (ImageButton) linearLayout1.findViewById(R.id.send_button);
                sendButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        attemptSend();
                    }
                });
                //linearLayout1.removeAllViews();
                if(setting.getString("nickname","").isEmpty()) {
                    LayoutInflater inflater2 = getLayoutInflater(b);
                    final View dialogView = inflater2.inflate(R.layout.popup_chat, null);
                    AlertDialog.Builder buider = new AlertDialog.Builder(getActivity());
                    buider.setView(dialogView);
                    final AlertDialog dialog = buider.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                    final ImageButton check = (ImageButton) dialog.findViewById(R.id.join_popup_check);
                    final EditText editText = (EditText) dialog.findViewById(R.id.editText);
                    check.setEnabled(false);
                    check.getBackground().setAlpha(51);
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if (!s.toString().isEmpty()) {
                                check.getBackground().setAlpha(200);
                                check.setEnabled(true);
                            } else {
                                check.getBackground().setAlpha(51);
                                check.setEnabled(false);
                            }

                        }
                    });
                    check.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            nickname = editText.getText().toString();
                            editor.putString("nickname", nickname);
                            editor.commit();
                            startSignIn();
                            mSocket.emit("add user", setting.getString("nickname","").toString());
                            dialog.dismiss();
                        }
                    });
                }else{
                    startSignIn();
                    mSocket.emit("add user", setting.getString("nickname","").toString());
                }
                mSocket.emit("current user");
                */
                return linearLayout1;
            default:
                return null;
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
/*
    @Override
    public void onPause() {
        super.onPause();
        mSocket.disconnect();
        mSocket.off("login", onLogin);
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("new message", onNewMessage);
        mSocket.off("user joined", onUserJoined);
        mSocket.off("user left", onUserLeft);
        mSocket.off("typing", onTyping);
        mSocket.off("stop typing", onStopTyping);
    }

    @Override
    public void onStart() {
        super.onStart();
        mSocket.on("login", onLogin);
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("new message", onNewMessage);
        mSocket.on("user joined", onUserJoined);
        mSocket.on("user left", onUserLeft);
        mSocket.on("typing", onTyping);
        mSocket.on("stop typing", onStopTyping);
        mSocket.connect();
    }
*/
    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("login", onLogin);
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("new message", onNewMessage);
        mSocket.off("user joined", onUserJoined);
        mSocket.off("user left", onUserLeft);
        mSocket.off("current user", CurrentUser);
        /*mSocket.off("typing", onTyping);
        mSocket.off("stop typing", onStopTyping);*/

    }

    private void addLog(String message) {
        /*
        mMessages.add(new Message.Builder(Message.TYPE_LOG)
                .message(message).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();*/
    }

    private void addParticipantsLog(int numUsers) {
        //addLog(getResources().getQuantityString(R.plurals.message_participants, numUsers, numUsers));
        try{
            userNum.setText(String.valueOf(numUsers));
        }catch (NullPointerException e){
            Log.e("null","addParticipantsLog");
        }
    }

    private void addMessage(String username, String message) {
        mMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                .username(username).message(message).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void addTyping(String username) {
        mMessages.add(new Message.Builder(Message.TYPE_ACTION)
                .username(username).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void removeTyping(String username) {
        for (int i = mMessages.size() - 1; i >= 0; i--) {
            Message message = mMessages.get(i);
            if (message.getType() == Message.TYPE_ACTION && message.getUsername().equals(username)) {
                mMessages.remove(i);
                mAdapter.notifyItemRemoved(i);
            }
        }
    }

    private void attemptSend() {
        if (null == mUsername) return;
        if (!mSocket.connected()) return;

        mTyping = false;

        String message = mInputMessageView.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            mInputMessageView.requestFocus();
            return;
        }

        mInputMessageView.setText("");
        addMessage(mUsername, message);

        // perform the sending message attempt.
        //mSocket.emit("room in", "youngdo1");
        mSocket.emit("new message", message);
    }

    private void startSignIn() {
        mUsername =  setting.getString("nickname","").toString();
    }

    private void leave() {
        mUsername = null;
        mSocket.disconnect();
        mSocket.connect();
        startSignIn();
    }

    private void scrollToBottom() {
        try {
            mMessagesView.scrollToPosition(mAdapter.getItemCount() - 1);
        } catch (NullPointerException e) {
            Log.e("null","scrollToBottom");
        }

    }

    private Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];

            int numUsers;
            try {
                numUsers = data.getInt("numUsers");
            } catch (JSONException e) {
                return;
            }

        }
    };
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isConnected) {
                        if (null != mUsername)
                            mSocket.emit("add user", mUsername);
                       /* Toast.makeText(getActivity().getApplicationContext(),
                                R.string.connect, Toast.LENGTH_LONG).show();*/
                        isConnected = true;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isConnected = false;
                    /*Toast.makeText(getActivity().getApplicationContext(),
                            R.string.disconnect, Toast.LENGTH_LONG).show();*/
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /*Toast.makeText(getActivity().getApplicationContext(),
                            R.string.error_connect, Toast.LENGTH_LONG).show();*/
                }
            });
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    String message;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        message = data.getString("message");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        return;
                    }

                    removeTyping(username);
                    addMessage(username, message);
                    addParticipantsLog(numUsers);
                }
            });
        }
    };

    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        return;
                    }
                    addLog(getResources().getString(R.string.message_user_joined, username));
                    addParticipantsLog(numUsers);
                }
            });
        }
    };

    private Emitter.Listener CurrentUser = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        return;
                    }
                    addParticipantsLog(numUsers);
                }
            });
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        return;
                    }

                    addLog(getResources().getString(R.string.message_user_left, username));
                    addParticipantsLog(numUsers);
                    removeTyping(username);
                }
            });
        }
    };

    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        username = data.getString("username");
                    } catch (JSONException e) {
                        return;
                    }
                    addTyping(username);
                }
            });
        }
    };

    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        username = data.getString("username");
                    } catch (JSONException e) {
                        return;
                    }
                    removeTyping(username);
                }
            });
        }
    };

    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            if (!mTyping) return;

            mTyping = false;
            mSocket.emit("stop typing");
        }
    };
}
