package com.ourincheon.smartcampus1231.Board;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.ApplicationController;
import com.ourincheon.smartcampus1231.Resource.RestClient;

import java.sql.Date;
import java.text.SimpleDateFormat;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Youngdo on 2016-03-22.
 */
///bwrite // 게시물 작성, 파라미터 : title,content,date(ex 20140505) | category 기능 추후지원
public class Board_write_Page extends AppCompatActivity {
    EditText title, content;
    long now = System.currentTimeMillis();
    public Date d = new Date(now);
    private RequestParams params = new RequestParams();
    private RestClient restClient;
    private boolean title_ok = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.board_write);
        title = (EditText) findViewById(R.id.write_title);
        content = (EditText) findViewById(R.id.write_content);
        final Button register_Button = (Button) findViewById(R.id.board_register);
        restClient = new RestClient(this);
        Button back = (Button) findViewById(R.id.board_write_btn_toggle);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        register_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (title.getText().toString().trim().length() > 0) {
                    String date = sdf.format(d);
                    int date1 = Integer.parseInt(date);
                    params.put("title", title.getText().toString());
                    params.put("content", content.getText().toString());
                    params.put("date", String.valueOf(date1+1));
                    restClient.post("bwrite", params, new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            Toast.makeText(Board_write_Page.this, "글 작성 오류!", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                            Toast.makeText(Board_write_Page.this, "글 작성 완료!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                } else {
                    Toast.makeText(Board_write_Page.this, "제목을 입력해주세요", Toast.LENGTH_SHORT).show();
                }
            }
        });
        Tracker t = ((ApplicationController)getApplication()).getTracker(ApplicationController.TrackerName.APP_TRACKER);
        t.setScreenName("Board_write_PageActivity");
        t.send(new HitBuilders.AppViewBuilder().build());
    }
    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
}
