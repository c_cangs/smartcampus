package com.ourincheon.smartcampus1231.Board;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.ApplicationController;

/**
 * Created by DBLAB on 2016-03-19.
 */
/*
/blist
/bread 파라미터 : id
/clist 파라미터 : id

/bwrite 파라미터 : title,content,date
/cwrite 파라미터 : mother_id(댓글달게시물 id),content,date
---------------------------------------------------------
/blist // 게시물 전체 가져오기
/bread // 게시물 내용만 가져오기, 파라미터 id
/clist // 게시물 댓글들 가져오기, 파라미터 id

/bwrite // 게시물 작성, 파라미터 : title,content,date(ex 20140505) | category 기능 추후지원
/cwrite // 댓글 작성, 파라미터 : mother_id(댓글달게시물 id),content,date
* 03/19 - todo 게시물 받아와서 댓글, 데이터 쌓기, swipelayout
*/
public class Board extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        Board_ViewPagerAdapter adapter = new Board_ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BoardFragment(), "자유 게시판");
        adapter.addFragment(new BoardFragment(), "준비 중입니다");
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimary));
        tabLayout.setTabTextColors(Color.GRAY, Color.BLACK);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Board.this, Board_write_Page.class);
                startActivity(intent);
            }
        });
        Button write = (Button) findViewById(R.id.write);
        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Board.this, Board_write_Page.class);
                startActivity(intent);
            }
        });
        Button back = (Button) findViewById(R.id.board_btn_toggle);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Tracker t = ((ApplicationController) getApplication()).getTracker(ApplicationController.TrackerName.APP_TRACKER);
        t.setScreenName("BoardActivity");
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
}
