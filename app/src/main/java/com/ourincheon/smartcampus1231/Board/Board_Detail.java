package com.ourincheon.smartcampus1231.Board;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by DBLAB on 2016-03-20.
 */
/*
* TODO - 댓글리스트, 댓글작성, 게시물작성
* 글을 표출하는 item layout을 만들고 해당 scrollview가 있는 layout에서 댓글수만큼 layoutinflater로 해당 댓글 layout을 불러와서 addView하는방법도...*/
public class Board_Detail extends AppCompatActivity {
    TextView content;
    EditText reply;
    long now = System.currentTimeMillis();
    public Date d = new Date(now);
    DisplayMetrics displayMetrics;
    private RestClient restClient;
    private String content_tx = null;
    private String id = null, title, name, date;
    private RequestParams params = new RequestParams();
    private RequestParams params1 = new RequestParams();
    private LinearLayout reply_list;
    private LinearLayout reply_list_item;
    private Reply_list_Adapter reply_list_adapter;
    private ArrayList<Board_Detail_reply> items = new ArrayList<Board_Detail_reply>();
    private boolean reply_ok = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.board_detail);
        restClient = new RestClient(this);
        Intent intent = getIntent();
        final ViewHolder holder = new ViewHolder();
        content = (TextView) findViewById(R.id.board_content);
        reply_list = (LinearLayout) findViewById(R.id.reply_list);


        TextView title_Tx = (TextView) findViewById(R.id.title_board);
        TextView board_name = (TextView) findViewById(R.id.board_name);
        TextView board_date = (TextView) findViewById(R.id.board_date);

        content_tx = intent.getStringExtra("content");
        id = intent.getStringExtra("id");
        title = intent.getStringExtra("title");
        name = intent.getStringExtra("name");
        date = intent.getStringExtra("date");
        Button back = (Button) findViewById(R.id.board_btn_toggle);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title_Tx.setText(title);
        board_name.setText(name);
        board_date.setText(date);
        params.put("id", id);
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        content.setText(content_tx);
        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        reply_list_adapter = new Reply_list_Adapter(this);
        restClient.get("clist", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    items.clear();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject object = response.getJSONObject(i);
                        Board_Detail_reply board_detail_reply = new Board_Detail_reply(object.getString("student_no"), object.getString("content"), object.getString("date").substring(0, 10));
                        items.add(board_detail_reply);
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);//new view create method??
                        reply_list_item =(LinearLayout) inflater.inflate(R.layout.board_reply_item, null);
                        holder.name = (TextView) reply_list_item.findViewById(R.id.board_reply_name);
                        holder.date = (TextView) reply_list_item.findViewById(R.id.board_reply_date);
                        holder.content = (TextView) reply_list_item.findViewById(R.id.board_reply_content);
                        holder.name.setText(items.get(i).getName());
                        holder.date.setText(items.get(i).getDate());
                        holder.content.setText(items.get(i).getContent());
                        reply_list.addView(reply_list_item);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

//addview
        reply = (EditText) findViewById(R.id.board_reply_comment);
        Button register = (Button) findViewById(R.id.board_reply_button);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reply.getText().toString().trim().length() > 0) {
                    String date = sdf.format(d);
                    int date1 = Integer.parseInt(date);

                    params1.put("id", id);
                    params1.put("content", reply.getText().toString());
                    params1.put("date", String.valueOf(date1 + 1));
                    restClient.post("cwrite", params1, new TextHttpResponseHandler() {
                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                            Toast.makeText(Board_Detail.this, "댓글 등록!", Toast.LENGTH_SHORT).show();
                            reply.setText("");
                            reply.clearFocus();
                            //clearfocus, 학생회 페이지 준비중
                        }
                    });
                    reply_list.requestFocus();
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);//new view create method??
                    reply_list_item =(LinearLayout) inflater.inflate(R.layout.board_reply_item, null);
                    holder.name = (TextView) reply_list_item.findViewById(R.id.board_reply_name);
                    holder.date = (TextView) reply_list_item.findViewById(R.id.board_reply_date);
                    holder.content = (TextView) reply_list_item.findViewById(R.id.board_reply_content);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                    String date2 = sdf1.format(d);
                    holder.name.setText(name);
                    holder.date.setText(String.valueOf(date2));
                    holder.content.setText(reply.getText().toString());
                    reply_list.addView(reply_list_item);
                } else {
                    Toast.makeText(Board_Detail.this, "댓글을 입력해주세요!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private class Reply_list_Adapter extends BaseAdapter {
        private Context mContext = null;

        public Reply_list_Adapter(Context context) {
            this.mContext = context;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.board_reply_item, null);
            holder.name = (TextView) convertView.findViewById(R.id.board_reply_name);
            holder.date = (TextView) convertView.findViewById(R.id.board_reply_date);
            holder.content = (TextView) convertView.findViewById(R.id.board_reply_content);
            holder.name.setText(items.get(position).getName());
            holder.date.setText(items.get(position).getDate());
            holder.content.setText(items.get(position).getContent());
            return convertView;
        }

    }

    private class ViewHolder {
        // public ImageView mIcon;
        public TextView name;
        public TextView date;
        public TextView content;
    }


}