package com.ourincheon.smartcampus1231.Board;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ourincheon.smartcampus1231.R;

import java.util.List;

/**
 * Created by DBLAB on 2016-03-19.
 */
public class Board_Recycler_Adapter extends RecyclerView.Adapter<Board_Recycler_Adapter.ViewHolder> {
    Context context;
    List<Board_item> items;
    int item_layout;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();//?

    public Board_Recycler_Adapter(Context context, List<Board_item> items, int item_layout){
        this.context = context;
        this.items = items;
        this.item_layout = item_layout;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.board_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Board_item board_item = items.get(position);
        holder.category.setText("게시판");
        holder.title.setText(board_item.getTitle());
        holder.major.setText("");
        holder.name.setText(board_item.getName());
        holder.date.setText(board_item.getDate());
        holder.text.setText("");
        holder.reply_Num.setText("3");
//        holder.mybackground.setSelected(selectedItems.get(position, false ));
//        holder.mybackground.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {//댓글, 글쓰기
//                if (selectedItems.get(position, false)) {
//                    selectedItems.delete(position);
//                    holder.mybackground.setSelected(false);
//                    holder.test.setVisibility(View.GONE);
//                } else {
//                    selectedItems.put(position, true);
//                    holder.mybackground.setSelected(true);
//                    holder.test.setVisibility(View.VISIBLE);
//                    holder.board_content.setText(String.valueOf(position));
//                    holder.board_reply.setText(String.valueOf(position));
//                }
//            }
//        });

    }


    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView category, title, major, name, date, text,reply_Num, board_content, board_reply;
        CardView cardview;
        //private static RelativeLayout relativeLayout;
        LinearLayout mybackground;
        LinearLayout test;
        public ViewHolder(View itemView) {
            super(itemView);
            category = (TextView) itemView.findViewById(R.id.category);
            title = (TextView) itemView.findViewById(R.id.title);
            major = (TextView) itemView.findViewById(R.id.board_major);
            name = (TextView) itemView.findViewById(R.id.board_name);
            date = (TextView) itemView.findViewById(R.id.board_date);
            text = (TextView) itemView.findViewById(R.id.board_text);
            reply_Num = (TextView) itemView.findViewById(R.id.reply_num);
            cardview = (CardView) itemView.findViewById(R.id.cardView);
            //mybackground = (LinearLayout) itemView.findViewById(R.id.myback);
            //test = (LinearLayout) itemView.findViewById(R.id.test_expand);
            //board_content = (TextView) itemView.findViewById(R.id.board_content);
            //board_reply = (TextView) itemView.findViewById(R.id.board_reply);
        }
    }
}
