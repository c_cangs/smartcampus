package com.ourincheon.smartcampus1231.Board;

/**
 * Created by Youngdo on 2016-03-22.
 */
public class Board_Detail_reply {
    private String name, content, date;

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }

    public String getDate() {
        return date;
    }
    Board_Detail_reply(String name, String content, String date){
        this.name = name;
        this.content = content;
        this.date = date;
    }
}
