package com.ourincheon.smartcampus1231.Board;

/**
 * Created by DBLAB on 2016-03-19.
 */
public class Board_item {
    private String category, title, major, name, date, text, board_content, board_reply;
    private int reply_Num;

    public String getCategory() {
        return category;
    }

    public int getReply_Num() {
        return reply_Num;
    }

    public String getText() {
        return text;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getMajor() {
        return major;
    }

    public String getBoard_content() {
        return board_content;
    }

    public String getBoard_reply() {
        return board_reply;
    }

    Board_item(String category, String title, String major, String name, String date, String text, int reply_Num){
        this.category = category;
        this.title = title;
        this.major = major;
        this.name = name;
        this.date = date;
        this.text = text;
        this.reply_Num = reply_Num;
    }
}
