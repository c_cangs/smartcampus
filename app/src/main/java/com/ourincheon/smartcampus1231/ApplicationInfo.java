package com.ourincheon.smartcampus1231;

import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by DBLAB on 2016-03-14.
 */
public class ApplicationInfo extends AppCompatActivity {
    SharedPreferences setting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.application_info);
        TextView textView = (TextView)findViewById(R.id.APPinfo_ver);
        setting = getSharedPreferences("setting",0);
        //textView.setText(setting.getString("version",""));
        PackageInfo pi = null;
        try {
            pi = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (Exception e){
            e.printStackTrace();
        }
        textView.setText(pi.versionName);
        Button back = (Button) findViewById(R.id.info_btn_toggle);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
