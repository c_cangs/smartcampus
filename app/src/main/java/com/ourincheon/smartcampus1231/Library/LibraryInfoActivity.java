package com.ourincheon.smartcampus1231.Library;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.ourincheon.smartcampus1231.R;


public class LibraryInfoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_library_info);

		Button backBtn = (Button) findViewById(R.id.libray_info_btn_back);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		String strUrl = getIntent().getStringExtra("library_url");
		WebView webView = (WebView) findViewById(R.id.library_info_webview);
		webView.setWebChromeClient(new WebChromeClient());
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setSupportZoom(true);
		webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
		webView.loadUrl(strUrl);

//		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUrl));
//		startActivity(intent);
		String strTitle = getIntent().getStringExtra("library_title");
		TextView titleTextview = (TextView) findViewById(R.id.library_info_title);
		titleTextview.setText(strTitle);
	}

}
