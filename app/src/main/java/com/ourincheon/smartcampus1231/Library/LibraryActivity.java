package com.ourincheon.smartcampus1231.Library;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ourincheon.smartcampus1231.R;
import com.ourincheon.smartcampus1231.Resource.ApplicationController;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Youngdo on 2016-01-13.
 */
public class LibraryActivity extends Activity implements View.OnClickListener{

    TextView free1, free2, free3, Notebook;
    TextView free1PER, free2PER, free3PER, NotebookPER;
    List<String> list1 = new ArrayList<String>();
    List<String> list2 = new ArrayList<String>();
    List<String> list3 = new ArrayList<String>();
    List<String> list4 = new ArrayList<String>();
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.library);
        new Task().execute();
        free1 = (TextView) findViewById(R.id.textView20);
        free2 = (TextView) findViewById(R.id.textView53);
        free3 = (TextView) findViewById(R.id.textView56);
        Notebook = (TextView) findViewById(R.id.textView69);
        free1PER = (TextView) findViewById(R.id.textView21);
        free2PER = (TextView) findViewById(R.id.textView54);
        free3PER = (TextView) findViewById(R.id.textView67);
        NotebookPER = (TextView) findViewById(R.id.textView70);
        Button back = (Button) findViewById(R.id.libray_btn_toggle);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Tracker t = ((ApplicationController)getApplication()).getTracker(ApplicationController.TrackerName.APP_TRACKER);
        t.setScreenName("LibraryActivity");
        t.send(new HitBuilders.AppViewBuilder().build());
    }
    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
    public class Task extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(LibraryActivity.this, "",
                    "로딩 중입니다. 잠시 기다려주세요", true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Document doc1 = Jsoup.connect("http://117.16.225.214:8080/SeatMate.php?classInfo=1")
                        .get();

                Elements parseLine1 = doc1.select("[color]");//color로 parse한 것을 Element에 넣었다

                for(Element e1 : parseLine1){
                    list1.add(e1.text());
                }

                Document doc2 = Jsoup.connect("http://117.16.225.214:8080/SeatMate.php?classInfo=2")
                        .get();
                Elements parseLine2 = doc2.select("[color]");//color로 parse한 것을 Element에 넣었다

                for(Element e2 : parseLine2){
                    list2.add(e2.text());
                }

                Document doc3 = Jsoup.connect("http://117.16.225.214:8080/SeatMate.php?classInfo=3")
                        .get();
                Elements parseLine3 = doc3.select("[color]");//color로 parse한 것을 Element에 넣었다

                for(Element e3 : parseLine3){
                    list3.add(e3.text());
                }

                Document doc4 = Jsoup.connect("http://117.16.225.214:8080/SeatMate.php?classInfo=4")
                        .get();
                Elements parseLine4 = doc4.select("[color]");//color로 parse한 것을 Element에 넣었다

                for(Element e4 : parseLine4){
                    list4.add(e4.text());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                int free1Per = 0, free2Per = 0, free3Per = 0, NoteBook = 0;
                free1.setText(list1.get(5) + "/240");
                free1Per = (Integer.parseInt(list1.get(5)) * 100 / 240);
                if (free1Per>90){
                    free1PER.setTextColor(getResources().getColor(R.color.library_Alert));
                }
                free1PER.setText((Integer.parseInt(list1.get(5)) * 100 / 240) + "%");

                free2.setText(list2.get(5) + "/394");
                free2Per = (Integer.parseInt(list2.get(5)) * 100 / 396);
                if (free2Per>90){
                    free2PER.setTextColor(getResources().getColor(R.color.library_Alert));
                }
                free2PER.setText((Integer.parseInt(list2.get(5)) * 100 / 396) + "%");

                free3.setText(list3.get(5) + "/240");
                free3Per = (Integer.parseInt(list3.get(5)) * 100 / 240);
                if (free3Per>90){
                    free3PER.setTextColor(getResources().getColor(R.color.library_Alert));
                }
                free3PER.setText((Integer.parseInt(list3.get(5)) * 100 / 240) + "%");

                Notebook.setText(list4.get(5) + "/108");
                NoteBook = (Integer.parseInt(list4.get(5)) * 100 / 108);
                if (NoteBook>90){
                    NotebookPER.setTextColor(getResources().getColor(R.color.library_Alert));
                }
                NotebookPER.setText((Integer.parseInt(list4.get(5)) * 100 / 108) + "%");
            } catch (IndexOutOfBoundsException e){
                Toast.makeText(LibraryActivity.this,"학교 서버에 접속할 수 없습니다",Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, LibraryInfoActivity.class);
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.library_first_btn:
                intent.putExtra("library_url",
                        "http://117.16.225.214:8080/mobile/SeatMate.php?classInfo=1");
                intent.putExtra("library_title", "자유열람실1");
                break;
            case R.id.library_second_btn:
                intent.putExtra("library_url",
                        "http://117.16.225.214:8080/mobile/SeatMate.php?classInfo=2");
                intent.putExtra("library_title", "자유열람실2");
                break;
            case R.id.library_third_btn:
                intent.putExtra("library_url",
                        "http://117.16.225.214:8080/mobile/SeatMate.php?classInfo=3");
                intent.putExtra("library_title", "자유열람실3");
                break;
            case R.id.library_fourth_btn:
                intent.putExtra("library_url",
                        "http://117.16.225.214:8080/mobile/SeatMate.php?classInfo=4");
                intent.putExtra("library_title", "노트북코너");
                break;

            default:
                break;
        }
        startActivity(intent);
    }
}
