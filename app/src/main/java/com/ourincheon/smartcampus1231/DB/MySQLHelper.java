package com.ourincheon.smartcampus1231.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Youngdo on 2016-03-07.
 */
public class MySQLHelper extends SQLiteOpenHelper {
    private static Context context;
    private static MySQLHelper INSTANCE;
    private static SQLiteDatabase mDb;
    public static MySQLHelper getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = new MySQLHelper(context.getApplicationContext());

        }
        return INSTANCE;
    }
    public MySQLHelper(Context context) {
        super(context, "STU.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String g1n1 = "CREATE TABLE g1n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g1n2 = "CREATE TABLE g1n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g2n1 = "CREATE TABLE g2n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g2n2 = "CREATE TABLE g2n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g3n1 = "CREATE TABLE g3n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g3n2 = "CREATE TABLE g3n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g4n1 = "CREATE TABLE g4n1 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String g4n2 = "CREATE TABLE g4n2 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String gn3 = "CREATE TABLE gn3 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String gn4 = "CREATE TABLE gn4 (year text not null, term text not null, name text not null," +
                "type text not null, credit text not null, rank text not null, score text not null, GIVE_UP_YN text not null);";
        String credit = "CREATE TABLE credit ( acquire_Credit integer not null, TotalCredit integer not null);";
        String TOT_AVG = "CREATE TABLE TOT_AVG ( tot_AVG text not null);";
        String year_AVG = "CREATE TABLE Year_AVG ( year_AVG text not null);";
        String TOT_CREDIT = "CREATE TABLE TOT_CREDIT(tot_CREDIT text not null);";
        db.execSQL(g1n1);
        db.execSQL(g1n2);
        db.execSQL(g2n1);
        db.execSQL(g2n2);
        db.execSQL(g3n1);
        db.execSQL(g3n2);
        db.execSQL(g4n1);
        db.execSQL(g4n2);
        db.execSQL(gn3);
        db.execSQL(gn4);
        db.execSQL(credit);
        db.execSQL(TOT_AVG);
        db.execSQL(year_AVG);
        db.execSQL(TOT_CREDIT);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String d_g1n1 = "drop table if exists g1n1";
        String d_g1n2 = "drop table if exists g1n2";
        String d_g2n1 = "drop table if exists g2n1";
        String d_g2n2 = "drop table if exists g2n2";
        String d_g3n1 = "drop table if exists g3n1";
        String d_g3n2 = "drop table if exists g3n2";
        String d_g4n1 = "drop table if exists g4n1";
        String d_g4n2 = "drop table if exists g4n2";
        String d_gn3 = "drop table if exists gn3";
        String d_gn4 = "drop table if exists gn4";
        String d_credit = "drop table if exists credit";
        String d_TOT_AVG = "drop table if exists TOT_AVG";
        String d_Year_AVG = "drop table if exists Year_AVG";
        String d_TOT_CREDIT = "drop table if exists TOT_CREDIT";
        db.execSQL(d_g1n1);
        db.execSQL(d_g1n2);
        db.execSQL(d_g2n1);
        db.execSQL(d_g2n2);
        db.execSQL(d_g3n1);
        db.execSQL(d_g3n2);
        db.execSQL(d_g4n1);
        db.execSQL(d_g4n2);
        db.execSQL(d_gn3);
        db.execSQL(d_gn4);
        db.execSQL(d_credit);
        db.execSQL(d_TOT_AVG);
        db.execSQL(d_Year_AVG);
        db.execSQL(d_TOT_CREDIT);
        onCreate(db);
    }
}
