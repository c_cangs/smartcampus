package com.ourincheon.smartcampus1231.Resource;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by DBLAB on 2016-03-10.
 */
public class InfoSharedPreference extends Activity {
    private static Context context;
    public InfoSharedPreference(Context context) {
        if(this.context == null){
            this.context = context;
        }
    }
    public String getPreferencesId(){
        SharedPreferences pref = getSharedPreferences("UserInfo",MODE_PRIVATE);
        return pref.getString("id","");
    }
    public String getPreferencesPw(){
        SharedPreferences pref = getSharedPreferences("UserInfo",MODE_PRIVATE);
        return pref.getString("pw","");
    }
    public void savePreferences(String id, String pw) {
        SharedPreferences pref = getSharedPreferences("UserInfo", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("id", id);
        editor.putString("pw", pw);
        editor.commit();
    }

    public void removePreferences(){
        SharedPreferences pref = getSharedPreferences("UserInfo", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("id");
        editor.remove("pw");
        editor.commit();
    }
    public void removeAllPreferences(){
        SharedPreferences pref = getSharedPreferences("UserInfo", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }
}
